const express = require('express');
const userAuthMiddleware = require('../src/Middleware/AuthuserMiddleware');
const RetraitController = require('../src/Controllers/RetraitController');

const router = express.Router();

router.get('/retraits', userAuthMiddleware.checkisAdmin, RetraitController.index);
router.get('/retraits_country/:countryslug', userAuthMiddleware.checkisAdmin, RetraitController.retraitscountryshow);
router.get('/retraits_payementmethod/:payementmethod', userAuthMiddleware.checkisAdmin, RetraitController.retraitspmtshow);
router.get('/retraits/:retrait', userAuthMiddleware.checkAuth, RetraitController.show);
router.put('/retraits/:retrait', userAuthMiddleware.checkAuth, RetraitController.update);

module.exports = router;
