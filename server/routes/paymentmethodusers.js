const express = require('express');
const PaymentmethoduserController = require('../src/Controllers/Partial/PaymentmethoduserController');
const userAuthMiddleware = require('../src/Middleware/AuthuserMiddleware');

const router = express.Router();

router.get(
  '/mypayementmethod/:user',
  userAuthMiddleware.checkAuth,
  PaymentmethoduserController.index,
);

router.post(
  '/mypayementmethods',
  userAuthMiddleware.checkAuth,
  PaymentmethoduserController.store,
);

router.get(
  '/mypayementmethods/:payementmethoduser',
  userAuthMiddleware.checkAuth,
  PaymentmethoduserController.show,
);

router.put(
  '/mypayementmethods/:payementmethoduser',
  userAuthMiddleware.checkAuth,
  PaymentmethoduserController.update,
);

router.put(
  '/mypayementmethods/:payementmethoduser/status',
  // userAuthMiddleware.checkAuth,
  PaymentmethoduserController.changestatus,
);

module.exports = router;
