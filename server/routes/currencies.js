const express = require('express');
const CurrencyController = require('../src/Controllers/Partial/CurrencyController');
const userAuthMiddleware = require('../src/Middleware/AuthuserMiddleware');

const router = express.Router();

router.get('/ability_foradmin_permissions', CurrencyController.abilityforadminPermissions);
router.get('/ability_foruser_permissions', CurrencyController.abilityforuserPermissions);

router.get('/currencies', CurrencyController.index);
router.post('/currencies', userAuthMiddleware.checkisSuperAdmin, CurrencyController.store);
router.get('/currencies/:id', userAuthMiddleware.checkisSuperAdmin, CurrencyController.show);
router.put('/currencies/:id', userAuthMiddleware.checkisSuperAdmin, CurrencyController.update);
router.delete('/currencies/:id', userAuthMiddleware.checkisSuperAdmin, CurrencyController.destroy);

router.post('/currency/user/', userAuthMiddleware.checkAuth, CurrencyController.usercurencyStore);

module.exports = router;
