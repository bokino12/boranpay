const express = require('express');
const ContactReceptionController = require('../src/Controllers/ContactReceptionController');
const userAuthMiddleware = require('../src/Middleware/AuthuserMiddleware');

const router = express.Router();

router.post('/contactreceptions/', userAuthMiddleware.checkisAdmin, ContactReceptionController.store);
router.get('/contactreceptions/:slug', ContactReceptionController.show);
router.put('/contactreceptions/:slug', userAuthMiddleware.checkisAdmin, ContactReceptionController.update);
router.delete('/contactreceptions/:slug', userAuthMiddleware.checkisAdmin, ContactReceptionController.destroy);

module.exports = router;
