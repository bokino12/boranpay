const express = require('express');
const userAuthMiddleware = require('../src/Middleware/AuthuserMiddleware');
const TransactionDonation = require('../src/Controllers/TransactionDonation');

const router = express.Router();

router.get('/transaction_donation/user/:user', userAuthMiddleware.checkAuth, TransactionDonation.showTransactionuser);

router.get('/transaction_donation/show/:transactiondonation', userAuthMiddleware.checkAuth, TransactionDonation.show);
router.post('/transfert_donation', userAuthMiddleware.checkAuth, TransactionDonation.transfertdonation);
router.post('/donation/:donation/contribute', userAuthMiddleware.checkAuth, TransactionDonation.donationcontribute);
router.post('/donations/:user/new', userAuthMiddleware.checkAuth, TransactionDonation.donationsTouser);

module.exports = router;
