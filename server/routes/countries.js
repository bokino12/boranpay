const express = require('express');
const CountryController = require('../src/Controllers/Partial/CountryController');
const userAuthMiddleware = require('../src/Middleware/AuthuserMiddleware');

const router = express.Router();

router.get('/countries', CountryController.index);
router.get('/countries_retrait', CountryController.countriesretrait);
router.post('/countries', userAuthMiddleware.checkisSuperAdmin, CountryController.store);
router.get('/countries/:country', CountryController.show);
router.put('/countries/:country', userAuthMiddleware.checkisSuperAdmin, CountryController.update);
router.delete('/countries/:country', userAuthMiddleware.checkisSuperAdmin, CountryController.destroy);

module.exports = router;
