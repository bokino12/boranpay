const express = require('express');
const PaymentmethodController = require('../src/Controllers/Partial/PaymentmethodController');
const userAuthMiddleware = require('../src/Middleware/AuthuserMiddleware');

const router = express.Router();

router.get('/payementmethods', PaymentmethodController.index);
router.post('/payementmethods', userAuthMiddleware.checkisSuperAdmin, PaymentmethodController.store);
router.get('/payementmethods/:slugin', PaymentmethodController.show);
router.put('/payementmethods/:slugin', userAuthMiddleware.checkisSuperAdmin, PaymentmethodController.update);
router.delete('/payementmethods/:slugin', userAuthMiddleware.checkisSuperAdmin, PaymentmethodController.destroy);

router.get('/payementmethods_retrait', PaymentmethodController.payementmethodretrait);
router.get('/payementmethods_recharge', PaymentmethodController.payementmethodrecharge);

module.exports = router;
