const express = require('express');
const userAuthMiddleware = require('../src/Middleware/AuthuserMiddleware');
const StatistiqueController = require('../src/Controllers/StatistiqueController');

const router = express.Router();

router.get('/statistique/transactions', userAuthMiddleware.checkisAdmin, StatistiqueController.transactionsSite);

module.exports = router;
