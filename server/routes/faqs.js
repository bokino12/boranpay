const express = require('express');
const userAuthMiddleware = require('../src/Middleware/AuthuserMiddleware');
const FaqController = require('../src/Controllers/FaqController');

const router = express.Router();

router.get('/faqs', FaqController.index);
router.post('/faqs', userAuthMiddleware.checkisAdmin, FaqController.store);
router.get('/faqs/:slugin', FaqController.show);
router.put('/faqs/:slugin', userAuthMiddleware.checkisAdmin, FaqController.update);
router.get('/faqs/:slugin/status', userAuthMiddleware.checkisAdmin, FaqController.statusChange);
router.delete('/faqs/:slugin', userAuthMiddleware.checkisAdmin, FaqController.destroy);

module.exports = router;
