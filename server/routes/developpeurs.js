const express = require('express');
const ApplicationController = require('../src/Controllers/ApplicationController');
const userAuthMiddleware = require('../src/Middleware/AuthuserMiddleware');

const router = express.Router();

router.get('/developpeur/app', ApplicationController.index);
router.post('/developpeur/app', userAuthMiddleware.checkAuth, ApplicationController.store);
router.get('/developpeur/app/:application', userAuthMiddleware.checkAuth, ApplicationController.show);
router.put('/developpeur/app/:application', userAuthMiddleware.checkAuth, ApplicationController.update);
router.get('/developpeur/app/:application/status', userAuthMiddleware.checkAuth, ApplicationController.statusChange);
router.put('/developpeur/app/:application/status', ApplicationController.changestatus);

router.get('/developpeur/app_user/:user', userAuthMiddleware.checkAuth, ApplicationController.applicationuser);

module.exports = router;
