const express = require('express');
const AdminAuthuserController = require('../src/Controllers/Auth/AdminAuthuserController');
const userAuthMiddleware = require('../src/Middleware/AuthuserMiddleware');
const UseradminController = require('../src/Controllers/UseradminController');

const router = express.Router();

router.get('/search/users', userAuthMiddleware.checkisAdmin, UseradminController.search);
router.get('/users', userAuthMiddleware.checkisAdmin, UseradminController.allusers);
router.get('/useradmins', userAuthMiddleware.checkisSuperAdmin, UseradminController.alluseradmins);
router.post('/useradmins', AdminAuthuserController.adminstore);
router.get('/useradmins/:useradmin', userAuthMiddleware.checkisSuperAdmin, UseradminController.adminsshow);
router.put('/useradmins/:useradmin', userAuthMiddleware.checkisSuperAdmin, AdminAuthuserController.adminsupdate);
router.delete('/useradmins/:useradmin', userAuthMiddleware.checkisSuperAdmin, UseradminController.adminsdelete);
router.post('/useradmins/:useradmin/sendemail', UseradminController.adminssendemail);

router.post('/admin/login', AdminAuthuserController.adminlogin);

module.exports = router;
