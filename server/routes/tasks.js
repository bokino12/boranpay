const express = require('express');
const TaskController = require('../src/Controllers/TaskController');
const userAuthMiddleware = require('../src/Middleware/AuthuserMiddleware');

const router = express.Router();

router.get('/tasks', TaskController.index);
router.post('/tasks', userAuthMiddleware.checkAuth, TaskController.store);
router.get('/tasks/:slugin', userAuthMiddleware.checkAuth, TaskController.show);
router.put('/tasks/:slugin', userAuthMiddleware.checkAuth, TaskController.update);
router.delete('/tasks/:slugin', userAuthMiddleware.checkAuth, TaskController.destroy);

module.exports = router;
