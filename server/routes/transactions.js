const express = require('express');
const userAuthMiddleware = require('../src/Middleware/AuthuserMiddleware');
const TransactionController = require('../src/Controllers/TransactionController');

const router = express.Router();

router.get('/transactions', userAuthMiddleware.checkisAdmin, TransactionController.index);
router.get('/transaction/user/:user', userAuthMiddleware.checkAuth, TransactionController.showTransauser);
router.get('/transaction/user/:user/send', userAuthMiddleware.checkAuth, TransactionController.showTransauserSend);
router.get('/transaction/user/:usersend/send/:userrecev', userAuthMiddleware.checkAuth, TransactionController.showTransauserSenduser);
router.get('/transaction/user/:user/recev', userAuthMiddleware.checkAuth, TransactionController.showTransauserRecev);
router.get('/transaction/user/:user/cagnotes', userAuthMiddleware.checkAuth, TransactionController.showTransaCagnoteuserRecev);
router.get('/transaction/user/:user/retraits', userAuthMiddleware.checkAuth, TransactionController.showTransauserRetrait);
router.get('/transaction/show/:transaction', userAuthMiddleware.checkAuth, TransactionController.show);

module.exports = router;
