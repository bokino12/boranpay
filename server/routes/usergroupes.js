const express = require('express');
const UsergroupeController = require('../src/Controllers/UsergroupeController');
const userAuthMiddleware = require('../src/Middleware/AuthuserMiddleware');

const router = express.Router();

router.get('/usergroupes', UsergroupeController.index);
router.post('/ugs', userAuthMiddleware.checkAuth, UsergroupeController.storeForContact);
router.get('/ug/:groupe', userAuthMiddleware.checkAuth, UsergroupeController.showUsergroupeGroupeuser);
router.get('/ug_by_user/:user', userAuthMiddleware.checkAuth, UsergroupeController.showUsergroupeContactuser);
router.post('/ug/:groupe/save', userAuthMiddleware.checkAuth, UsergroupeController.storeForGroupe);
router.get('/ugs/:usergroupe', userAuthMiddleware.checkAuth, UsergroupeController.show);
router.put('/ugs/:usergroupe', userAuthMiddleware.checkAuth, UsergroupeController.update);
router.delete('/ugs/:usergroupe', userAuthMiddleware.checkAuth, UsergroupeController.destroy);

module.exports = router;
