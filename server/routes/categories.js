const express = require('express');
const CategoryController = require('../src/Controllers/CategoryController');
const userAuthMiddleware = require('../src/Middleware/AuthuserMiddleware');

const router = express.Router();

router.get('/categories', CategoryController.index);
router.post('/categories', userAuthMiddleware.checkAuth, CategoryController.store);
router.get('/categories/:slugin', CategoryController.show);
router.put('/categories/:slugin', userAuthMiddleware.checkAuth, CategoryController.update);

module.exports = router;
