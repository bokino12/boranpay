const express = require('express');
const HomeController = require('../src/Controllers/HomeController');

const router = express.Router();

router.get('/', HomeController.index);

module.exports = router;
