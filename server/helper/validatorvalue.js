const currencyValidate = ['EUR', 'GBP', 'USD', 'CAD', 'XOF', 'XAF'];
const typePayementValidate = ['retrait', 'recharge'];

module.exports = {
  currencyValidate,
  typePayementValidate,
};
