## KazouGroup 

- **[BoranPay](http://www.boranpay.com)**
- **[Kazoutech](http://www.kazoutech.cm)**
## Configuration

Cette partie conserne le server personaliser de Nodejs avec le Framework Express

Cloner le fichier puis faire un ``` yarn ``` ou ``` npm install ```

Ensuite pour demarer le server ``` yarn start ``` et ``` npm run start ```

Vous aurrez ce lien **[http://127.0.0.1:8080](http://127.0.0.1:8080)** 

## Queques commande importat ici 
Pour créer la migration et le model  ``` npx sequelize-cli model:generate --name namemodel --attributes nametable1:string,nametable2:string ``` **[ Change namtable1 and nametable2 ]** 

Configurer votre fichier ``` .env``` en duplicant celle de ``` .env.example ```

Faite ensuite  ``` php artisan key:generate ``` et faire  ``` php artisan serve ```

Vous aurrez ce lien **[http://127.0.0.1:8080](http://127.0.0.1:8080)** 

Apres la configuration de la base de données  ``` php artisan migrate:fresh --seed```

## Configuration base de données

``` mysql -uroot -p``` entrer votre mot de passe mysql ``` CREATE DATABASE ivemo;```

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [dasgivemoi@gmail.com](mailto:dasgivemoi@gmail.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).
