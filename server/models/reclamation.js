const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class reclamation extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static async associate(models) {
      // define association here

      await reclamation.belongsTo(models.user, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });
    }
  }
  reclamation.init({
    total: DataTypes.FLOAT,
    currency: DataTypes.STRING,
    title: DataTypes.STRING,
    slugin: DataTypes.STRING,
    content: DataTypes.TEXT,
    userId: DataTypes.INTEGER.UNSIGNED,
  }, {
    sequelize,
    modelName: 'reclamation',
  });
  return reclamation;
};
