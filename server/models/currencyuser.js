const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class currencyuser extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static async associate(models) {
      // define association here

      await currencyuser.belongsTo(models.user, {
        foreignKey: 'userId',
      });

      await currencyuser.belongsTo(models.currency, {
        foreignKey: 'currencyId',
      });
    }
  }
  currencyuser.init({
    userId: DataTypes.INTEGER,
    currencyId: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'currencyuser',
  });
  return currencyuser;
};
