const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class transaction extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static async associate(models) {
      // define association here

      await transaction.belongsTo(models.user, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      await transaction.belongsTo(models.user, {
        as: 'userto',
        foreignKey: 'usertoId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      await transaction.belongsTo(models.category, {
        foreignKey: 'categoryId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      await transaction.belongsTo(models.country, {
        foreignKey: 'countryId',
      });

      /** Je recupre l'amount faite par l'utilisateur */
      await transaction.hasOne(models.amount, {
        foreignKey: 'transactionId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      await transaction.hasOne(models.retrait, {
        foreignKey: 'transactionId',
        onDelete: 'CASCADE',
      });
    }
  }
  transaction.init({
    ip: DataTypes.STRING,
    statusSend: DataTypes.BOOLEAN,
    slugin: DataTypes.STRING,
    paymentId: DataTypes.STRING,
    tokenTransaction: DataTypes.STRING,
    custom: DataTypes.STRING,
    invoiceNumber: DataTypes.STRING,
    title: DataTypes.STRING,
    content: DataTypes.TEXT,
    usertoId: DataTypes.INTEGER.UNSIGNED,
    countryId: DataTypes.INTEGER.UNSIGNED,
    userId: DataTypes.INTEGER.UNSIGNED,
    categoryId: DataTypes.INTEGER.UNSIGNED,
  }, {
    sequelize,
    modelName: 'transaction',
  });
  return transaction;
};
