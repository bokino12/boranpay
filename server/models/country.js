const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class country extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static async associate(models) {
      // define association here

      await country.belongsTo(models.currency, {
        foreignKey: 'currencyId',
      });

      await country.hasMany(models.retrait, {
        foreignKey: 'countryId',
      });
    }
  }
  country.init({
    name: DataTypes.STRING,
    code: DataTypes.STRING,
    slug: DataTypes.STRING,
    region: DataTypes.STRING,
    capital: DataTypes.STRING,
    currencyId: DataTypes.INTEGER.UNSIGNED,
    statusMovesold: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: false },
    statusRechargesold: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: false },
    phoneCode: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'country',
  });
  return country;
};
