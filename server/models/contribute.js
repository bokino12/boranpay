const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class contribute extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static async associate(models) {
      // define association here

      await contribute.belongsTo(models.user, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      await contribute.belongsTo(models.transactiondonation, {
        foreignKey: 'transactiondonationId',
        constraints: false,
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      await contribute.belongsTo(models.transactioncagnote, {
        foreignKey: 'transactioncagnoteId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      await contribute.belongsTo(models.cagnote, {
        foreignKey: 'cagnoteId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      await contribute.belongsTo(models.donation, {
        foreignKey: 'donationId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });
    }
  }
  contribute.init({
    content: DataTypes.TEXT,
    slugin: DataTypes.STRING,
    ip: DataTypes.STRING,
    currency: DataTypes.STRING,
    total: DataTypes.FLOAT,
    taxeContribute: DataTypes.FLOAT,
    amountContribute: DataTypes.FLOAT,
    userId: DataTypes.INTEGER.UNSIGNED,
    cagnoteId: DataTypes.INTEGER.UNSIGNED,
    donationId: DataTypes.INTEGER.UNSIGNED,
    transactioncagnoteId: DataTypes.INTEGER.UNSIGNED,
    transactiondonationId: DataTypes.INTEGER.UNSIGNED,
    statusTotal: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: false },
    statusUser: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: false },
    invoiceNumber: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'contribute',
  });
  return contribute;
};
