const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class coupon extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static async associate(models) {
      await coupon.belongsTo(models.user, {
        foreignKey: 'userId',
        constraints: false,
      });
    }
  }
  coupon.init({
    couponAmount: DataTypes.FLOAT,
    ip: DataTypes.STRING,
    currency: DataTypes.STRING,
    couponNumber: DataTypes.STRING,
    status: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: false },
    userId: DataTypes.INTEGER.UNSIGNED,
    seriesNumber: DataTypes.INTEGER.UNSIGNED,
  }, {
    sequelize,
    modelName: 'coupon',
  });
  return coupon;
};
