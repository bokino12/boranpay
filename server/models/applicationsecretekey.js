const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class applicationsecretekey extends Model {
    /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
    static async associate(models) {
      // define association here
      await applicationsecretekey.belongsTo(models.application, {
        foreignKey: 'applicationId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });
    }
  }

  applicationsecretekey.init({
    secretKey: DataTypes.TEXT,
    ip: DataTypes.STRING,
    applicationId: DataTypes.INTEGER.UNSIGNED,
  }, {
    sequelize,
    modelName: 'applicationsecretekey',
  });
  return applicationsecretekey;
};
