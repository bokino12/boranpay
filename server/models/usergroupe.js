const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class usergroupe extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static async associate(models) {
      // define association here

      await usergroupe.belongsTo(models.user, {
        as: 'userby',
        constraints: false,
        foreignKey: 'userbyId', // ici c'est l'utilisateur qui creer
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      await usergroupe.belongsTo(models.directory, {
        foreignKey: 'directoryId',
        onUpdate: 'CASCADE',
      });

      await usergroupe.belongsTo(models.user, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      await usergroupe.belongsTo(models.groupe, {
        foreignKey: 'groupeId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });
    }
  }
  usergroupe.init({
    fullName: DataTypes.STRING,
    email: DataTypes.STRING,
    currency: DataTypes.STRING,
    slugin: DataTypes.STRING,
    district: DataTypes.STRING,
    ip: DataTypes.STRING,
    phone: DataTypes.INTEGER.UNSIGNED,
    numberCompte: DataTypes.INTEGER.UNSIGNED,
    directoryId: DataTypes.INTEGER.UNSIGNED,
    countyId: DataTypes.INTEGER.UNSIGNED,
    groupeId: DataTypes.INTEGER.UNSIGNED,
    userbyId: DataTypes.INTEGER.UNSIGNED,
    userId: DataTypes.INTEGER.UNSIGNED,
  }, {
    sequelize,
    modelName: 'usergroupe',
  });
  return usergroupe;
};
