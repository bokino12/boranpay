const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class contact extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static async associate(models) {
      // define association here

      // await contact.belongsTo(models.user, {
      //  foreignKey: 'userId',
      //  onDelete: 'CASCADE',
      //  onUpdate: 'CASCADE'
      // });

      await contact.belongsTo(models.country, {
        foreignKey: 'countryId',
      });

      await contact.belongsTo(models.user, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
      });

      await contact.belongsTo(models.user, {
        as: 'userto',
        foreignKey: 'usertoId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });
    }
  }
  contact.init({
    name: DataTypes.STRING,
    ip: DataTypes.STRING,
    slug: DataTypes.STRING,
    email: DataTypes.STRING,
    userId: DataTypes.INTEGER.UNSIGNED,
    usertoId: DataTypes.INTEGER.UNSIGNED,
    countryId: DataTypes.INTEGER.UNSIGNED,
    content: DataTypes.TEXT,
    status: DataTypes.BOOLEAN,
    subject: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'contact',
  });
  return contact;
};
