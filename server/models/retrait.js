const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class retrait extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static async associate(models) {
      // define association here

      await retrait.belongsTo(models.country, {
        foreignKey: 'countryId',
        constraints: false,
      });

      await retrait.belongsTo(models.payementmethoduser, {
        foreignKey: 'payementmethoduserId',
        constraints: false,
      });

      await retrait.belongsTo(models.payementmethod, {
        foreignKey: 'payementmethodId',
        constraints: false,
      });

      await retrait.belongsTo(models.useradmin, {
        foreignKey: 'useradminId',
      });

      await retrait.belongsTo(models.transaction, {
        foreignKey: 'transactionId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });
    }
  }
  retrait.init({
    content: DataTypes.TEXT,
    amountRetrait: DataTypes.FLOAT,
    statusMovesold: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: false },
    statusCancelretrait: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: false },
    slugin: DataTypes.STRING,
    ip: DataTypes.STRING,
    transactionId: DataTypes.INTEGER.UNSIGNED,
    useradminId: DataTypes.INTEGER.UNSIGNED,
    countryId: DataTypes.INTEGER.UNSIGNED,
    payementmethodId: DataTypes.INTEGER.UNSIGNED,
    payementmethoduserId: DataTypes.INTEGER.UNSIGNED,
  }, {
    sequelize,
    modelName: 'retrait',
  });
  return retrait;
};
