const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class amountuser extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static async associate(models) {
      // define association here

      await amountuser.belongsTo(models.user, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      await amountuser.belongsTo(models.amount, {
        foreignKey: 'amountId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });
    }
  }
  amountuser.init({
    amountUser: DataTypes.FLOAT,
    ip: DataTypes.STRING,
    amountUserNoTaxe: DataTypes.FLOAT,
    userId: DataTypes.INTEGER.UNSIGNED,
    amountId: DataTypes.INTEGER.UNSIGNED,
  }, {
    sequelize,
    modelName: 'amountuser',
  });
  return amountuser;
};
