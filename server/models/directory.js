const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class directory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static async associate(models) {
      // define association here

      await directory.hasMany(models.usergroupe, {
        foreignKey: 'directoryId',
      });
    }
  }
  directory.init({
    name: DataTypes.STRING,
    slug: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'directory',
  });
  return directory;
};
