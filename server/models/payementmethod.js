const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class payementmethod extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static async associate(models) {
      // define association here

      await payementmethod.hasMany(models.payementmethoduser, {
        foreignKey: 'payementmethodId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      await payementmethod.belongsTo(models.country, {
        foreignKey: 'countryId',
      });
    }
  }
  payementmethod.init({
    region: DataTypes.STRING,
    name: DataTypes.STRING,
    slug: DataTypes.STRING,
    slugin: DataTypes.STRING,
    type: DataTypes.STRING,
    typePayement: DataTypes.STRING,
    taxeTransaction: DataTypes.FLOAT,
    countryId: DataTypes.INTEGER.UNSIGNED,
  }, {
    sequelize,
    modelName: 'payementmethod',
  });
  return payementmethod;
};
