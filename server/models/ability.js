const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class ability extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static async associate(models) {
      // define association here

      /** Relation avec l'user du site */
      await ability.belongsTo(models.user, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      /** relation avec l'admin du site */
      await ability.belongsTo(models.useradmin, {
        foreignKey: 'useradminId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });
    }
  }
  ability.init({
    action: DataTypes.STRING,
    ip: DataTypes.STRING,
    subject: DataTypes.STRING,
    userId: DataTypes.INTEGER.UNSIGNED,
    useradminId: DataTypes.INTEGER.UNSIGNED,
  }, {
    sequelize,
    modelName: 'ability',
  });
  return ability;
};
