const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class developeruser extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static async associate(models) {
      // define association here
      /** Je recupre les transaction faite par l'utilisateur */

      await developeruser.belongsTo(models.application, {
        foreignKey: 'applicationId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });
    }
  }
  developeruser.init({
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    username: DataTypes.STRING,
    slugin: DataTypes.STRING,
    applicationId: DataTypes.INTEGER.UNSIGNED,
    statusProfile: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    email: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true,
    },
    avatar: DataTypes.STRING,
    password: DataTypes.STRING,
  }, {
    indexes: [
      // Create a unique index on email
      {
        unique: true,
        fields: ['email'],
      },
    ],
    sequelize,
    modelName: 'developeruser',
  });

  return developeruser;
};
