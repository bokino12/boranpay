const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class transactiondonation extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static async associate(models) {
      // define association here

      await transactiondonation.belongsTo(models.user, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      await transactiondonation.belongsTo(models.donation, {
        foreignKey: 'donationId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      await transactiondonation.belongsTo(models.category, {
        foreignKey: 'categoryId',
        // onDelete: 'CASCADE',
        // onUpdate: 'CASCADE',
        constraints: false,
      });

      await transactiondonation.belongsTo(models.user, {
        as: 'userto',
        foreignKey: 'usertoId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      /** Je recupre l'amount faite par l'utilisateur */
      await transactiondonation.hasOne(models.amount, {
        foreignKey: 'transactiondonationId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });
    }
  }
  transactiondonation.init({
    ip: DataTypes.STRING,
    statusSend: DataTypes.BOOLEAN,
    slugin: DataTypes.STRING,
    description: DataTypes.STRING,
    tokenTransaction: DataTypes.STRING,
    custom: DataTypes.STRING,
    invoiceNumber: DataTypes.STRING,
    title: DataTypes.STRING,
    content: DataTypes.TEXT,
    usertoId: DataTypes.INTEGER.UNSIGNED,
    categoryId: DataTypes.INTEGER.UNSIGNED,
    userId: DataTypes.INTEGER.UNSIGNED,
    donationId: DataTypes.INTEGER.UNSIGNED,
  }, {
    sequelize,
    modelName: 'transactiondonation',
  });
  return transactiondonation;
};
