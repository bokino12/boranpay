const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class amountservice extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static async associate(models) {
      // define association here

      await amountservice.belongsTo(models.user, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      await amountservice.belongsTo(models.amount, {
        foreignKey: 'amountId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      await amountservice.belongsTo(models.application, {
        foreignKey: 'applicationId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });
    }
  }
  amountservice.init({
    amountService: DataTypes.FLOAT,
    amountServiceNoTaxe: DataTypes.FLOAT,
    amountServiceTest: DataTypes.FLOAT,
    amountServiceBeyer: DataTypes.FLOAT,
    statusApplication: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: false },
    // if amountService === true application online
    userId: DataTypes.INTEGER.UNSIGNED,
    ip: DataTypes.STRING,
    serviceId: DataTypes.INTEGER.UNSIGNED,
    amountId: DataTypes.INTEGER.UNSIGNED,
    applicationId: DataTypes.INTEGER.UNSIGNED,
  }, {
    sequelize,
    modelName: 'amountservice',
  });
  return amountservice;
};
