const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class useradmin extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static async associate(models) {
      // define association here

      /** Je recupre le profile de l'utilisateur */
      await useradmin.belongsTo(models.country, {
        foreignKey: 'countryId',
        constraints: false,
      });

      await useradmin.belongsTo(models.currency, {
        foreignKey: 'currencyId',
        constraints: false,
      });

      await useradmin.belongsTo(models.organisation, {
        foreignKey: 'organisationId',
        constraints: false,
      });

      /** Je recupre les permissions de l'utilisateur */
      await useradmin.hasMany(models.ability, {
        as: 'ability',
        foreignKey: 'useradminId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });
    }
  }
  useradmin.init({
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    username: DataTypes.STRING,
    birstday: DataTypes.STRING,
    slugin: DataTypes.STRING,
    sex: DataTypes.STRING,
    role: DataTypes.STRING,
    addresse: DataTypes.STRING,
    city: DataTypes.STRING,
    viewpasswd: DataTypes.STRING,
    providerToken: DataTypes.TEXT,
    phone: DataTypes.INTEGER.UNSIGNED,
    countryId: DataTypes.INTEGER.UNSIGNED,
    currencyId: DataTypes.INTEGER.UNSIGNED,
    statusProfile: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    email: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true,
    },
    avatar: DataTypes.STRING,
    password: DataTypes.STRING,
  }, {
    indexes: [
      // Create a unique index on email
      {
        unique: true,
        fields: ['email'],
      },
    ],
    sequelize,
    modelName: 'useradmin',
  });

  return useradmin;
};
