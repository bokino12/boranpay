const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class groupe extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static async associate(models) {
      // define association here

      await groupe.belongsTo(models.user, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      await groupe.hasMany(models.usergroupe, {
        foreignKey: 'groupeId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });
    }
  }
  groupe.init({
    name: DataTypes.STRING,
    photo: DataTypes.STRING,
    ip: DataTypes.STRING,
    content: DataTypes.TEXT,
    slugin: DataTypes.STRING,
    userId: DataTypes.INTEGER.UNSIGNED,
  }, {
    sequelize,
    modelName: 'groupe',
  });
  return groupe;
};
