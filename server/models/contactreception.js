const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class contactreception extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate() {
      // define association here
    }
  }
  contactreception.init({
    title: DataTypes.STRING,
    ip: DataTypes.STRING,
    slug: DataTypes.STRING,
    content: DataTypes.TEXT,
  }, {
    sequelize,
    modelName: 'contactreception',
  });
  return contactreception;
};
