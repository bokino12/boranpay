const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class image extends Model {
    /**
       * Helper method for defining associations.
       * This method is not a part of Sequelize lifecycle.
       * The `models/index` file will call this method automatically.
       */
    static async associate(models) {
      // define association here

      await image.belongsTo(models.articleblog, {
        foreignKey: 'articleblogId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      await image.belongsTo(models.cagnote, {
        foreignKey: 'cagnoteId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      await image.belongsTo(models.donation, {
        foreignKey: 'donationId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });
    }
  }
  image.init({
    slugin: DataTypes.STRING,
    imageName: DataTypes.TEXT,
    articleblogId: DataTypes.INTEGER.UNSIGNED,
    cagnoteId: DataTypes.INTEGER.UNSIGNED,
    donationId: DataTypes.INTEGER.UNSIGNED,
  }, {
    sequelize,
    modelName: 'image',
  });
  return image;
};
