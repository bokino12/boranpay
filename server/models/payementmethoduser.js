const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class payementmethoduser extends Model {
    /**
       * Helper method for defining associations.
       * This method is not a part of Sequelize lifecycle.
       * The `models/index` file will call this method automatically.
       */
    static async associate(models) {
      // define association here

      await payementmethoduser.belongsTo(models.payementmethod, {
        foreignKey: 'payementmethodId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      await payementmethoduser.belongsTo(models.user, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });
    }
  }

  payementmethoduser.init({
    contactRecev: DataTypes.STRING,
    fullName: DataTypes.STRING,
    slugin: DataTypes.STRING,
    countryName: DataTypes.STRING,
    isDelete: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: false },
    userId: DataTypes.INTEGER.UNSIGNED,
    payementmethodId: DataTypes.INTEGER.UNSIGNED,
  }, {
    sequelize,
    modelName: 'payementmethoduser',
  });
  return payementmethoduser;
};
