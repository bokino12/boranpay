const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class abilitypermission extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static async associate() {
      // define association here

    }
  }
  abilitypermission.init({
    name: DataTypes.STRING,
    label: DataTypes.STRING,
    status: DataTypes.BOOLEAN,
    action: DataTypes.STRING,
    subject: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'abilitypermission',
  });
  return abilitypermission;
};
