const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class donation extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static async associate(models) {
      // define association here

      await donation.belongsTo(models.user, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      await donation.hasMany(models.contribute, {
        as: 'contributecount',
        foreignKey: 'donationId',
        onDelete: 'CASCADE',
      });

      await donation.hasMany(models.contribute, {
        foreignKey: 'donationId',
        onDelete: 'CASCADE',
      });

      /** Je recupre les amountservice de la donation faite par les utilisateurs */
      await donation.hasMany(models.amountdonation, {
        foreignKey: 'donationId',
        onUpdate: 'CASCADE',
      });

      /** Je recupre les amountservice de la donation faite par les utilisateurs */
      await donation.hasMany(models.transactiondonation, {
        foreignKey: 'donationId',
        onUpdate: 'CASCADE',
      });
    }
  }
  donation.init({
    title: DataTypes.STRING,
    slugin: DataTypes.STRING,
    slug: DataTypes.STRING,
    content: DataTypes.TEXT,
    userId: DataTypes.INTEGER.UNSIGNED,
    isClosing: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: false },
    isDelete: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: false },
  }, {
    sequelize,
    modelName: 'donation',
  });
  return donation;
};
