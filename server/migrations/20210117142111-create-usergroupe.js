'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('usergroupes', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fullName: {
        allowNull: true,
        type: Sequelize.STRING
      },
      email: {
        allowNull: true,
        type: Sequelize.STRING
      },
      slugin: {
        allowNull: true,
        type: Sequelize.STRING
      },
      avatar: {
        allowNull: true,
        type: Sequelize.STRING
      },
      district: {
        allowNull: true,
        type: Sequelize.STRING
      },
      ip: {
        allowNull: true,
        type: Sequelize.STRING
      },
      number_compte: {
        allowNull: true,
        type: Sequelize.INTEGER
      },
      total: {
        allowNull: true,
        type: Sequelize.FLOAT
      },
      currency: {
        allowNull: true,
        type: Sequelize.STRING
      },
      county_id: {
        allowNull: true,
        type: Sequelize.INTEGER
      },
      groupe_id: {
        allowNull: true,
        type: Sequelize.INTEGER
      },
      userby_id: {
        allowNull: true,
        type: Sequelize.INTEGER
      },
      user_id: {
        allowNull: true,
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('usergroupes');
  }
};