'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('currencyusers', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
       user_id: {
        allowNull: true,
        type: Sequelize.INTEGER.UNSIGNED,
        references: {
          models: 'users',
          key: 'id'
        }
      },
      currency_id: {
        allowNull: true,
        type: Sequelize.INTEGER.UNSIGNED,
        references: {
          models: 'currencyusers',
          key: 'id'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('currencyusers');
  }
};