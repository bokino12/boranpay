const assert = require('assert').strict;
const luhn = require('./luhn.js');

describe('luhn', async () => {
  describe('#validate()', () => {
    it('should accept valid Visa test number', async () => {
      await assert.ok(luhn.validate('4012-8888-8888-1881'));
    });
    it('should accept valid MasterCard test number', async () => {
      await assert.ok(luhn.validate('4242-4242-4242-4242'));
    });
    it('should accept valid Amex test number', async () => {
      await assert.ok(luhn.validate('3714-496353-98431'));
    });
    it('should reject invalid numbers', async () => {
      await assert.equal(luhn.validate('1234-5678-9101-2131'), false);
    });
  });
});
