const models = require('../../models');

/** Ici je Get tous les donners de la base de donner **/
const index = async (req, res) => {
  try {
    const notifications = await models.notification.findAll({

      include: [
        {
          model: models.user,
          required: false,
          attributes: ['firstName', 'lastName', 'slugin', 'avatar'],
        },
      ],
      order: [['createdAt', 'DESC']],
    });
    res.status(200).json(notifications);
  } catch (error) {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

const show = async (req, res) => {
  try {
    const notification = await models.notification.findOne({
      where: { slugin: req.params.notification },
      include: [
        {
          model: models.user,
          attributes: ['firstName', 'lastName', 'avatar'],
        },
      ],
    });
    res.status(200).json(notification);
  } catch (error) {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

const notificationuser = async (req, res) => {
  try {
    const notification = await models.notification.findOne({
      // where: { slugin: req.params.notification },
      include: [
        {
          model: models.user,
          attributes: ['firstName', 'lastName', 'avatar'],
        },
      ],
    });
    res.status(200).json(notification);
  } catch (error) {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

// const changestatus = async (req, res) => {
//   const dataUpdate = { status: false };
//   try {
//     await models.cagnote.update(dataUpdate, { where: { slugin: req.params.slugin } });
//
//     res.status(200).json({
//       message: 'Data updated successfully',
//     });
//   } catch (error) {
//     res.status(500).json({
//       message: 'Something went wrong',
//       error,
//     });
//   }
// };

module.exports = {
  index,
  show,
  notificationuser,
};
