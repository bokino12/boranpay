const models = require('../../../../models');

const getAllAffiliationService = async (req, res) => {
  const affiliations = await models.affiliation.findAll({
    attributes: ['id', 'amount', 'code', 'slugin', 'currency', 'createdAt'],
    include: [
      { model: models.user, attributes: ['id', 'firstName', 'lastName', 'email', 'slugin', 'avatar'] },
    ],
    order: [['createdAt', 'DESC']],
  });
  res.status(200).json(affiliations);
};

module.exports = { getAllAffiliationService };
