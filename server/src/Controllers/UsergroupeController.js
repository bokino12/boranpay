const Validator = require('fastest-validator');
const { makeSluginID } = require('../../helper/utils');
const models = require('../../models');
const { currencyValidate } = require('../../helper/validatorvalue');

/** Ici je Get tous les donners de la base de donner **/
const index = async (req, res) => {
  try {
    // Ici je recupere uniquement les contact non lier au Groupe!
    // c'est pourquoi j'utilise where: { directoryId: 2 }
    const usergroupes = await models.usergroupe.findAll({
      where: { directoryId: 2 },
      include: [
        {
          model: models.user,
          attributes: ['id', 'firstName', 'lastName', 'slugin', 'avatar'],
        },
      ],
      order: [['createdAt', 'DESC']],
    });
    res.status(200).json(usergroupes);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

/** Ici je Post les donners de la base de donner **/
const storeForGroupe = async (req, res) => {
  // Check if group exist
  const groupe = await models.groupe.findOne({ where: { slugin: req.params.groupe } });
  const user = await models.user.findOne({ where: { email: req.body.email } });
  if (!user) {
    res.status(400).json({ message: 'Cet utilisateur est inexistant veuillez insérer un e-mail valide' });
  }
  const usergroupeItem = await models.usergroupe.findOne({
    where: {
      userId: user.id,
      groupeId: groupe.id,
    },
  });

  const item = {
    fullName: req.body.fullName,
    phone: req.body.phone,
    currency: req.body.currency,
    district: req.body.district,
    email: req.body.email,
    groupeId: groupe.id,
    ip: req.ip,
    directoryId: 2, // Ici c'est pour sauvegarder les contact dans le groupe
    userbyId: req.AuthUser.userId,
    userId: user.id,
    slugin: makeSluginID(30),
  };

  const schema = {
    fullName: { type: 'string', min: 3, max: 200 },
    email: { type: 'email', min: 4, max: 6000 },
    currency: { type: 'enum', values: currencyValidate },
    phone: { type: 'number', positive: true, integer: true },
    userId: { type: 'number', positive: true, integer: true },
    userbyId: { type: 'number', positive: true, integer: true },
  };

  const v = new Validator();
  const validationResponse = v.validate(item, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    if (validationResponse === true) {
      if (!usergroupeItem) {
        const usergroupe = await models.usergroupe.create(
          item, { where: { slugin: groupe.slugin } },
        );
        res.status(200).json({
          message: 'Données sauvegardées avec succès',
          usergroupe,
        });
      } else {
        res.status(400).json({ message: 'Cet utilisateur est déjà membre du groupe veuillez ajouter un autre' });
      }
    } else {
      res.status(400).json({
        errors: validationResponse,
      });
    }
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

const storeForContact = async (req, res) => {
  // Check if group exist
  const user = await models.user.findOne({ where: { email: req.body.email } });
  if (!user) {
    return res.status(400).json({ message: 'Cet utilisateur est inexistant veuillez insérer un e-mail valide' });
  }

  const item = {
    fullName: req.body.fullName,
    phone: req.body.phone,
    currency: req.body.currency,
    district: req.body.district,
    email: req.body.email,
    ip: req.ip,
    directoryId: 1, // Ici c'est pour sauvegarder les contact
    userbyId: req.AuthUser.userId,
    userId: user.id,
    slugin: makeSluginID(30),
  };

  const schema = {
    fullName: { type: 'string', min: 3, max: 200 },
    email: { type: 'email', min: 4, max: 6000 },
    currency: { type: 'enum', values: currencyValidate },
    phone: { type: 'number', positive: true, integer: true },
    userId: { type: 'number', positive: true, integer: true },
    userbyId: { type: 'number', positive: true, integer: true },
  };

  const v = new Validator();
  const validationResponse = v.validate(item, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    if (validationResponse === true) {
      const usergroupe = await models.usergroupe.create(item);
      return res.status(200).json({
        message: 'Data save successfully',
        usergroupe,
      });
    }
    return res.status(400).json({
      errors: validationResponse,
    });
  } catch (error) {
    return res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

/** Ici je Show une donner les donners de la base de donner **/
const show = async (req, res) => {
  try {
    const usergroupe = await models.usergroupe.findOne({
      where: { slugin: req.params.usergroupe },
      include: [
        {
          model: models.user,
          attributes: ['id', 'firstName', 'lastName', 'slugin', 'avatar'],
        },
      ],
    });
    res.status(200).json(usergroupe);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

/** Ici je recupere tous les utilisateur qui sont dans le groupe */
const showUsergroupeGroupeuser = async (req, res) => {
  const groupeSlugin = req.params.groupe;
  const userItem = req.AuthUser.userId;

  try {
    // Ici je recupere uniquement les contact non lier au Groupe!
    // c'est pourquoi j'utilise where: { directoryId: 2 }
    const groupe = await models.usergroupe.findAll({
      where: { userbyId: userItem, directoryId: 2 },
      include: [
        {
          model: models.user,
          attributes: ['firstName', 'lastName', 'email', 'username', 'slugin', 'sex', 'avatar'],
        },
        {
          model: models.groupe,
          where: { slugin: groupeSlugin, userId: userItem },
          attributes: ['name', 'slugin', 'userId'],
        },
      ],
      order: [['updatedAt', 'DESC']],

    });
    res.status(200).json(groupe);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

/** Ici je recupere tous les utilisateurs qui sont dans le repertoire */
const showUsergroupeContactuser = async (req, res) => {
  try {
    // Ici je recupere uniquement les contact non lier au Groupe!
    // c'est pourquoi j'utilise where: { directoryId: 1 }
    const users = await models.usergroupe.findAll({
      where: { directoryId: 1 },
      include: [
        {
          model: models.user,
          as: 'userby',
          where: { slugin: req.params.user },
          attributes: ['firstName', 'lastName', 'email', 'username', 'slugin', 'sex', 'avatar'],
        },
        {
          model: models.user,
          attributes: ['firstName', 'lastName', 'email', 'username', 'slugin', 'sex', 'avatar'],
        },
      ],
      order: [['fullName', 'DESC']],

    });
    res.status(200).json(users);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

/** Ici je Update la donne */
const update = async (req, res) => {
  const dataUpdate = {
    fullName: req.body.fullName,
    phone: req.body.phone,
    currency: req.body.currency,
    district: req.body.district,
    ip: req.ip,
  };

  const schema = {
    fullName: { type: 'string', min: 3, max: 200 },
  };

  const v = new Validator();
  const validationResponse = v.validate(dataUpdate, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    if (validationResponse === true) {
      await models.usergroupe.update(dataUpdate, { where: { slugin: req.params.usergroupe } });

      res.status(200).json({
        message: 'Data updated successfully',
      });
    } else {
      res.status(400).json({
        errors: validationResponse,
      });
    }
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

/** Ici je Delete  la donne */
const destroy = async (req, res) => {
  try {
    await models.usergroupe.destroy({ where: { slugin: req.params.usergroupe } });

    res.status(200).json({
      message: 'Data delete successfully',
    });
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

module.exports = {
  index,
  storeForGroupe,
  show,
  storeForContact,
  showUsergroupeGroupeuser,
  showUsergroupeContactuser,
  update,
  destroy,
};
