const Validator = require('fastest-validator');
const { makeSluginID } = require('../../../helper/utils');
const models = require('../../../models');

/** Ici je Get tous les donne de la base de donner **/
const index = async (req, res) => {
  try {
    const countries = await models.organisation.findAll({
      attributes: ['id', 'name'],
      order: [['name', 'asc']],
    });
    res.status(200).json(countries);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

const store = async (req, res) => {
  const organisation = await models.organisation.findOne({ where: { name: req.body.name } });
  if (organisation) {
    return res.status(400).json({ message: 'This organisation exist change' });
  }

  const item = {
    name: req.body.name,
    slugin: makeSluginID(30),
  };

  const schema = {
    name: { type: 'string', min: 3, max: 200 },
  };

  const v = new Validator();
  const validationResponse = v.validate(item, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    const response = await models.organisation.create(item);
    return res.status(200).json({ response });
  } catch (error) {
    return res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

const update = async (req, res) => {
  const dataUpdate = {
    name: req.body.name,
    slugin: makeSluginID(30),
  };

  const schema = {
    name: { type: 'string', min: 3, max: 200 },
  };

  const v = new Validator();
  const validationResponse = v.validate(dataUpdate, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    if (validationResponse === true) {
      await models.organisation.update(dataUpdate, { where: { slugin: req.params.organisation } });

      res.status(200).json({
        message: 'Data updated successfully',
      });
    } else {
      res.status(400).json({
        errors: validationResponse,
      });
    }
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

const show = async (req, res) => {
  try {
    const organisation = await models.organisation.findOne({
      where: { slug: req.params.organisation },
      attributes: ['id', 'name'],
    });
    res.status(200).json(organisation);
  } catch (error) {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

const destroy = async (req, res) => {
  try {
    await models.organisation.destroy({ where: { slugin: req.params.organisation } });

    res.status(200).json({
      message: 'Data delete successfully',
    });
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

module.exports = {
  index,
  store,
  show,
  update,
  destroy,
};
