const Validator = require('fastest-validator');

const models = require('../../../models');
const { makeSluginNumber } = require('../../../helper/utils');
const { currencyValidate } = require('../../../helper/validatorvalue');

/** Ici je Get tous les donne de la base de donner **/
const index = async (req, res) => {
  const userInfo = await models.useradmin.findOne({ where: { slugin: req.AuthUser.slugin } });
  if (!userInfo) {
    return res.status(400).json({ message: 'User not defined please check or contact administrator' });
  }

  try {
    const coupons = await models.coupon.findAll({
      attributes: ['couponAmount', 'status', 'currency', 'couponNumber', 'updatedAt'],
      include: [
        {
          model: models.user,
          attributes: ['firstName', 'lastName', 'slugin', 'avatar', 'email'],
        },
      ],
      order: [['createdAt', 'DESC']],
    });
    return res.status(200).json(coupons);
  } catch (error) {
    return res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

/** Ici je Post les donners de la base de donner **/
const store = async (req, res) => {
  const userInfo = await models.useradmin.findOne({ where: { slugin: req.AuthUser.slugin } });
  if (!userInfo) {
    return res.status(400).json({ message: 'User not defined please check or contact administrator' });
  }

  const item = {
    couponAmount: req.body.couponAmount,
    currency: req.body.currency,
    // userId: req.AuthUser.userId,
    couponNumber: makeSluginNumber(16),
  };

  const schema = {
    couponAmount: { type: 'number', positive: true, integer: true },
    currency: { type: 'enum', values: currencyValidate },
  };

  const v = new Validator();
  const validationResponse = v.validate(item, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    if (validationResponse === true) {
      const coupon = await models.coupon.create(item);
      if (!coupon) {
        return res.status(400).json({ message: 'Données incorrect' });
      }
      return res.status(200).json({ coupon });
    }
    res.status(400).json({
      errors: validationResponse,
    });
  } catch (error) {
    return res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

module.exports = {
  index,
  store,
};
