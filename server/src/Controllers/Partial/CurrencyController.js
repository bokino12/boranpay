const Validator = require('fastest-validator');
const models = require('../../../models');

const abilityforadminPermissions = async (req, res) => {
  try {
    const abilitypermissions = await models.abilitypermission.findAll({
      where: { status: true },
      attributes: ['label', 'name', 'action', 'subject'],
      order: [['createdAt', 'DESC']],
    });

    res.status(200).json(abilitypermissions);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

const abilityforuserPermissions = async (req, res) => {
  try {
    const abilitypermissions = await models.abilitypermission.findAll({
      where: { status: false },
      attributes: ['label', 'name'],
      order: [['createdAt', 'DESC']],
    });

    res.status(200).json(abilitypermissions);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};
/** Ici je Get tous les donne de la base de donner **/
const index = async (req, res) => {
  try {
    const currencies = await models.currency.findAll({
      attributes: ['id', 'code', 'name', 'symbol', 'currencyNumber'],
      order: [['code', 'DESC']],
    });
    res.status(200).json(currencies);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

/** Ici je Post les donners de la base de donner **/
const store = async (req, res) => {
  const userInfo = await models.useradmin.findOne({ where: { slugin: req.AuthUser.slugin } });
  if (!userInfo) {
    return res.status(400).json({ message: 'User not defined please check or contact administrator' });
  }

  const item = {
    code: req.body.code,
    name: req.body.name,
    symbol: req.body.symbol,
    currencyNumber: req.body.currencyNumber,
  };

  const schema = {
    code: 'string|required|max:200',
    name: 'string|required|max:200',
    symbol: 'string|required|max:200',
  };

  const v = new Validator();
  const validationResponse = v.validate(item, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    const currency = await models.currency.create(item);
    return res.status(200).json({
      message: 'Données sauvegardées avec succès',
      currency,
    });
  } catch (error) {
    return res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

/** Ici je Update la donne */
const update = async (req, res) => {
  const userInfo = await models.useradmin.findOne({ where: { slugin: req.AuthUser.slugin } });
  if (!userInfo) {
    return res.status(400).json({ message: 'User not defined please check or contact administrator' });
  }

  const dataUpdate = {
    code: req.body.code,
    name: req.body.name,
    symbol: req.body.symbol,
    currencyNumber: req.body.currencyNumber,
  };

  const schema = {
    code: 'string|required|max:200',
    name: 'string|required|max:200',
    symbol: 'string|required|max:200',
  };

  const v = new Validator();
  const validationResponse = v.validate(dataUpdate, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    await models.currency.update(dataUpdate, { where: { id: req.params.id } });
    return res.status(200).json({
      message: 'Données mis à jour avec succès',
    });
  } catch (error) {
    return res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

/** Ici je Show une donner les donners de la base de donner **/
const show = async (req, res) => {
  try {
    const currency = await models.currency.findByPk(req.params.id);
    res.status(200).json(currency);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

const usercurencyStore = async (req, res) => {
  const item = {
    currencyId: req.body.currency_id,
    userId: req.AuthUser.userId,
  };

  const schema = {
    currencyId: { type: 'number', positive: true, integer: true },
    userId: { type: 'number', positive: true, integer: true },
  };

  const v = new Validator();
  const validationResponse = v.validate(item, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    const currencyuser = await models.currencyuser.create(item);
    return res.status(200).json({
      message: 'Devise sauvegardées avec succès',
      currencyuser,
    });
  } catch (error) {
    return res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};
/** Ici je Delete  la donne */
const destroy = async (req, res) => {
  const userInfo = await models.useradmin.findOne({ where: { slugin: req.AuthUser.slugin } });
  if (!userInfo) {
    return res.status(400).json({ message: 'User not defined please check or contact administrator' });
  }

  try {
    await models.currency.destroy({ where: { id: req.params.id } });
    return res.status(200).json({
      message: 'Devise supprimé avec succès',
    });
  } catch (error) {
    return res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

module.exports = {
  index,
  store,
  show,
  update,
  destroy,
  abilityforadminPermissions,
  abilityforuserPermissions,
  usercurencyStore,
};
