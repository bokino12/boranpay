const Validator = require('fastest-validator');
const { makeSluginID } = require('../../helper/utils');
const models = require('../../models');
const { currencyValidate } = require('../../helper/validatorvalue');

/** Ici je Get tous les donners de la base de donner **/
const index = async (req, res) => {
  try {
    const reclamations = await models.reclamation.findAll({
      include: [
        {
          model: models.user,
          attributes: ['firstName', 'lastName', 'slugin', 'avatar'],
        }],
      order: [['createdAt', 'DESC']],
    });
    res.status(200).json(reclamations);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

/** Ici je Post les donners de la base de donner **/
const store = async (req, res) => {
  const item = {
    title: req.body.title,
    total: req.body.total,
    currency: req.body.currency,
    content: req.body.content,
    userId: req.AuthUser.userId,
    slugin: makeSluginID(30),
  };

  const schema = {
    title: { type: 'string', min: 3, max: 200 },
    total: { type: 'number', positive: true, integer: true },
    userId: { type: 'number', positive: true, integer: true },
    currency: { type: 'enum', values: currencyValidate },
  };

  const v = new Validator();
  const validationResponse = v.validate(item, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    if (validationResponse === true) {
      const reclamation = await models.reclamation.create(item);
      res.status(200).json({
        message: 'Data save successfully',
        reclamation,
      });
    } else {
      res.status(400).json({
        errors: validationResponse,
      });
    }
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

/** Ici je Show une donner les donners de la base de donner **/
const show = async (req, res) => {
  try {
    const reclamation = await models.reclamation.findOne({
      where: { slugin: req.params.reclamation },
      include: [
        {
          model: models.user,
          attributes: ['firstName', 'lastName', 'slugin', 'avatar'],
        }],
    });
    res.status(200).json(reclamation);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

const showanduser = async (req, res) => {
  try {
    const reclamation = await models.reclamation.findOne({
      where: { slugin: req.params.reclamation },
      include: [
        {
          model: models.user,
          where: { slugin: req.params.userslugin },
          attributes: ['firstName', 'slugin', 'avatar'],
        }],
    });
    res.status(200).json(reclamation);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

const showDatauser = async (req, res) => {
  try {
    const reclamations = await models.reclamation.findAll({
      include: [
        {
          model: models.user,
          where: { slugin: req.params.user },
          attributes: ['firstName', 'lastName', 'slugin'],
        },
      ],
      order: [['updatedAt', 'DESC']],
    });

    res.status(200).json(reclamations);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

/** Ici je Update la donne */
const update = async (req, res) => {
  const dataUpdate = {
    title: req.body.title,
    total: req.body.total,
    currency: req.body.currency,
    content: req.body.content,
  };

  const schema = {
    title: { type: 'string', min: 3, max: 200 },
    total: { type: 'number', positive: true, integer: true },
    currency: { type: 'enum', values: currencyValidate },
  };

  const v = new Validator();
  const validationResponse = v.validate(dataUpdate, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    if (validationResponse === true) {
      await models.reclamation.update(dataUpdate, { where: { slugin: req.params.reclamation } });

      res.status(200).json({
        message: 'Data updated successfully',
      });
    } else {
      res.status(400).json({
        errors: validationResponse,
      });
    }
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

/** Ici je Delete  la donne */
const destroy = async (req, res) => {
  try {
    await models.reclamation.destroy({ where: { slugin: req.params.reclamation } });

    res.status(200).json({
      message: 'Data delete successfully',
    });
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

module.exports = {
  index,
  store,
  show,
  showanduser,
  showDatauser,
  update,
  destroy,
};
