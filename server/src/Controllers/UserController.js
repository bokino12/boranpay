const { Sequelize } = require('sequelize');
const jwt = require('jsonwebtoken');
const models = require('../../models');

/** Ici je Get l'utilisateur avec ses donner liés la relation */
const show = async (req, res) => {
  await models.user.findOne({
    where: { slugin: req.params.user },
    attributes: ['id', 'firstName', 'email', 'lastName', 'username', 'slugin', 'sex', 'avatar'],

    include: [
      {
        model: models.profile,
        attributes: ['slugin'],
        required: true,
        include: [
          {
            model: models.currency,
            required: true,
            attributes: ['code', 'symbol', 'currencyNumber'],
          },
        ],
      },
      {
        model: models.amountuser,
        attributes: [
          'userId',
          [Sequelize.fn('sum', Sequelize.col('amountUser')), 'totalAmountuser'],
        ],
        group: ['userId'],
        limit: 999999999999999,
      },
      {
        model: models.amountservice,
        attributes: [
          'userId',
          [Sequelize.fn('sum', Sequelize.col('amountService')), 'totalAmountservice'],
        ],
        group: ['userId'],
        limit: 999999999999999,

      },
      {
        model: models.amountdonation,
        attributes: [
          'userId',
          [Sequelize.fn('sum', Sequelize.col('amountDonation')), 'totalAmountdonation'],
        ],
        group: ['userId'],
        // Bon ici c'est pour corriger le bug de sequelize pour calculer bien
        limit: 999999999999999,
      },
      {
        model: models.amountcagnote,
        attributes: [
          'userId',
          [Sequelize.fn('sum', Sequelize.col('amountCagnote')), 'totalAmountcagnote'],
        ],
        group: ['userId'],
        limit: 999999999999999,

      },
      {
        model: models.amountservice,
        attributes: [
          'userId',
          [Sequelize.fn('sum', Sequelize.col('amountService')), 'totalAmountservice'],
        ],
        group: ['userId'],
        limit: 999999999999999,
      },
      {
        limit: 8,
        model: models.transactioncagnote,
        as: 'transactioncagnoterecevs',
        order: [['createdAt', 'DESC']],
        include: [
          { model: models.user, attributes: ['id', 'firstName', 'email', 'avatar'] },
          { model: models.amount, attributes: ['total', 'totalNoTaxe', 'currency'] },
        ],
      },
      {
        limit: 8,
        model: models.transaction,
        order: [['createdAt', 'DESC']],
        include: [
          { model: models.user, as: 'userto', attributes: ['id', 'firstName', 'lastName', 'email', 'slugin', 'avatar'] },
          { model: models.user, attributes: ['id', 'firstName', 'lastName', 'email', 'slugin', 'avatar'] },
          { model: models.amount, attributes: ['total', 'totalNoTaxe', 'currency'] },
          { model: models.category, attributes: ['name', 'slug'] },
        ],
      },
      {
        limit: 8,
        model: models.transaction,
        as: 'transactionsends',
        where: { statusSend: false },
        order: [['createdAt', 'DESC']],
        include: [
          { model: models.user, as: 'userto', attributes: ['id', 'firstName', 'lastName', 'email', 'slugin', 'avatar'] },
          { model: models.user, attributes: ['id', 'firstName', 'lastName', 'email', 'slugin', 'avatar'] },
          { model: models.amount, attributes: ['total', 'totalNoTaxe', 'currency'] },
          { model: models.category, attributes: ['name', 'slug'] },
        ],
      },
      {
        limit: 8,
        model: models.transaction,
        as: 'transactionrecevs',
        where: { statusSend: true },
        order: [['createdAt', 'DESC']],
        include: [
          { model: models.user, as: 'userto', attributes: ['id', 'firstName', 'lastName', 'email', 'slugin', 'avatar'] },
          { model: models.user, attributes: ['id', 'firstName', 'lastName', 'slugin', 'avatar'] },
          { model: models.amount, attributes: ['total', 'totalNoTaxe', 'currency'] },
          { model: models.category, attributes: ['name', 'slug'] },
        ],
      },
      {
        limit: 8,
        model: models.currencyuser,
        attributes: ['currencyId', 'userId'],
        order: [['createdAt', 'DESC']],
        include: [
          { model: models.currency, attributes: ['code', 'symbol', 'currencyNumber'] },
        ],
      },
    ],
  }).then((result) => {
    res.status(200).json(result);
  }).catch((error) => {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  });
};

const showuserSlugin = async (req, res) => {
  try {
    const user = await models.user.findOne({
      where: { slugin: req.params.user },
      attributes: ['firstName', 'lastName', 'email', 'phone', 'username', 'slugin', 'sex', 'avatar'],
      include: [
        {
          model: models.profile,
          required: true,
          include: [
            {
              model: models.currency,
              required: true,
              attributes: ['code', 'symbol', 'currencyNumber'],
            },
          ],
        },
      ],
    });

    res.status(200).json(user);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

const showpublic = async (req, res) => {
  const usernameuser = req.params.username;
  const iduser = req.params.id;

  try {
    const user = await models.user.findOne({
      where: { id: iduser, username: usernameuser },
      attributes: ['id', 'firstName', 'email', 'lastName', 'username', 'slugin', 'sex', 'avatar'],
    });

    res.status(200).json(user);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

const profile = async (req, res) => {
  const user = req.AuthUser;
  return res.status(200).json({ user });
};
/** Update my profile */
const update = async (req, res) => {
  const {
    firstName, lastName, username, sex, email, phone,
  } = req.body;
  const user = await models.user.findOne({
    where: { slugin: req.params.user },
    include: [{
      model: models.ability, as: 'ability', attributes: ['action', 'subject'], required: true,
    }],
  });
  if (!user) {
    return res.status(400).json({ message: "Aucun utilisateur n'a été trouvé avec cette adresse mail" });
  }

  try {
    const dataUpdate = {
      firstName,
      lastName,
      username,
      sex,
      email,
      phone,
    };

    // const user = req.AuthUser;
    await user.update(dataUpdate);

    const itemUser = {
      firstName,
      lastName,
      username,
      sex,
      email,
      statusProfile: user.statusProfile,
      avatar: user.avatar,
      slugin: user.slugin,
      emailVerifiedStatus: user.emailVerifiedStatus,
      userId: user.id,
      role: user.role,
      ability: user.ability,
    };

    const token = jwt.sign(itemUser, process.env.JWT_KEY, {
      expiresIn: process.env.JWT_EXPIRE,
    });

    return res.status(200).json({
      firstName,
      lastName,
      username,
      sex,
      email,
      statusProfile: user.statusProfile,
      avatar: user.avatar,
      slugin: user.slugin,
      emailVerifiedStatus: user.emailVerifiedStatus,
      userId: user.id,
      role: user.role,
      ability: user.ability,
      accessToken: token,
    });
  } catch (error) {
    return res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

const updateProfile = async (req, res) => {
  const {
    addresse, countryId, siteInternet, currencyId,
  } = req.body;
  try {
    const dataUpdate = {
      addresse,
      countryId,
      siteInternet,
      currencyId,
    };

    await models.profile.update(dataUpdate, { where: { slugin: req.params.user } });

    res.status(200).json({
      message: 'Data updated successfully',
    });
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

module.exports = {
  show,
  showuserSlugin,
  showpublic,
  profile,
  updateProfile,
  update,
};
