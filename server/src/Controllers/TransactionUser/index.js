const Validator = require('fastest-validator');
const { makeSluginID } = require('../../../helper/utils');
const models = require('../../../models');
const transactionuserService = require('./services/TransactionuserService');
const { currencyValidate } = require('../../../helper/validatorvalue');

const transfertreclamation = async (req, res) => {
  const reclamation = await models.reclamation.findOne({
    where: { slugin: req.params.reclamation },
  });
  if (!reclamation) {
    res.status(400).json({ message: "Cette reclamation n'existe pas ou n'est plus valide" });
  }

  const Title = `${reclamation.title}`;
  const userIdSend = req.AuthUser.userId;
  const userIdRecev = reclamation.userId;
  const Total = reclamation.total;
  const userSend = req.AuthUser;
  const userRecev = reclamation.userId;
  const userCurrency = reclamation.currency;
  const categoryTransation = 3; // Id tranfert

  const itemSend = {
    title: Title,
    userId: userIdSend,
    content: req.body.content,
    currency: userCurrency,
    statusSend: false,
    slugin: makeSluginID(30),
  };

  const schema = {
    userId: { type: 'string', positive: true, integer: true },
    statusSend: 'boolean', // short-hand def
  };

  const v = new Validator();
  const validationResponse = v.validate(itemSend, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    if (validationResponse === true) {
      if (reclamation.userId === req.AuthUser.userId) {
        res.status(400).json({ message: 'Vous ne pouvez pas vous faire de transfert copier le lien et partager à vos proches.' });
      } else {
        transactionuserService.anountTraitementUser(
          req,
          res,
          categoryTransation,
          Title,
          Total,
          userCurrency,
          userIdSend,
          userIdRecev,
          userSend,
          userRecev,
        );
      }
    } else {
      res.status(400).json({
        errors: validationResponse,
      });
    }
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

const usergroupe = async (req, res) => {
  const usergroupeData = await models.usergroupe.findOne({
    where: { slugin: req.params.usergroupe },
  });
  if (!usergroupeData) {
    res.status(400).json({ message: "Cet utilisateur n'existe pas dans votre repertoire veuilez svp insérer un e-mail valide" });
  }

  const Title = `Transfert - ${usergroupeData.fullName || ''}`;
  const userIdSend = req.AuthUser.userId;
  const userIdRecev = usergroupeData.userId;
  const Total = req.body.total;
  const userSend = req.AuthUser;
  const userRecev = usergroupeData.userId;
  const userCurrency = usergroupeData.currency;
  const categoryTransation = 3; // Id tranfert

  const itemSend = {
    title: Title,
    userId: userIdSend,
    content: req.body.content,
    currency: userCurrency,
    statusSend: false,
    slugin: makeSluginID(30),
  };

  const schema = {
    userId: { type: 'string', positive: true, integer: true },
    statusSend: 'boolean', // short-hand def
  };

  const v = new Validator();
  const validationResponse = v.validate(itemSend, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    if (validationResponse === true) {
      transactionuserService.anountTraitementUser(
        req,
        res,
        categoryTransation,
        Title,
        Total,
        userCurrency,
        userIdSend,
        userIdRecev,
        userSend,
        userRecev,
      );
    } else {
      res.status(400).json({
        errors: validationResponse,
      });
    }
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

/** Ici je Post les donners de la base de donner **/
const transfertsend = async (req, res) => {
  const {
    email, total, currency, title,
  } = req.body;
  const user = await models.user.findOne({ where: { email } });
  if (!user) {
    res.status(400).json({ message: 'Cet utilisateur est inexistant veuilez insérer un e-mail valide' });
  }

  const Title = `Transfert - ${title}`;
  const userIdSend = req.AuthUser.userId;
  const userIdRecev = user.id;
  const Total = total;
  const userSend = req.AuthUser;
  const userRecev = user;
  const userCurrency = currency;
  const categoryTransation = 3; // Id tranfert

  const itemSend = {
    title: Title,
    userId: userIdSend,
    content: req.body.content,
    currency: userCurrency,
    statusSend: false,
    slugin: makeSluginID(30),
  };

  const schema = {
    userId: { type: 'string', positive: true, integer: true },
    currency: { type: 'enum', values: currencyValidate },
    title: { type: 'string', min: 3, max: 200 },
    statusSend: 'boolean', // short-hand def
  };

  const v = new Validator();
  const validationResponse = v.validate(itemSend, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  console.log('req.AuthUser ====>', req.AuthUser);
  console.log('userIdRecev ====>', user);

  try {
    if (validationResponse === true) {
      if (user.email === req.AuthUser.email) {
        res.status(400).json({ message: 'Vous ne pouviez pas vous faire de transfert! Partager votre contact avec vos proche pour recevoir les tranferts' });
      } else {
        transactionuserService.anountTraitementUser(
          req,
          res,
          categoryTransation,
          Title,
          Total,
          userCurrency,
          userIdSend,
          userIdRecev,
          userSend,
          userRecev,
        );
      }
    } else {
      res.status(400).json({
        errors: validationResponse,
      });
    }
  } catch (error) {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

const transfertusersend = async (req, res) => {
  const user = await models.user.findOne({ where: { slugin: req.params.user } });
  if (!user) {
    res.status(400).json({ message: 'Cet utilisateur est inexistant veuilez insérer un e-mail valide' });
  }

  const Title = `Transfert - ${req.body.title}`;
  const userIdSend = req.AuthUser.userId;
  const userIdRecev = user.id;
  const Total = req.body.total;
  const userSend = req.AuthUser;
  const userRecev = user;
  const userCurrency = req.body.currency;
  const categoryTransation = 3; // Id tranfert

  const itemSend = {
    title: Title,
    userId: userIdSend,
    content: req.body.content,
    currency: userCurrency,
    statusSend: false,
    slugin: makeSluginID(30),
  };

  const schema = {
    userId: { type: 'string', positive: true, integer: true },
    currency: { type: 'enum', values: currencyValidate },
    statusSend: 'boolean', // short-hand def
  };

  const v = new Validator();
  const validationResponse = v.validate(itemSend, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    if (validationResponse === true) {
      if (user.email === req.AuthUser.email) {
        res.status(400).json({ message: 'Vous ne pouviez pas vous faire de transfert! Partager votre contact avec vos proche pour recevoir les tranferts' });
      } else {
        transactionuserService.anountTraitementUser(
          req,
          res,
          categoryTransation,
          Title,
          Total,
          userCurrency,
          userIdSend,
          userIdRecev,
          userSend,
          userRecev,
        );
      }
    } else {
      res.status(400).json({
        errors: validationResponse,
      });
    }
  } catch (error) {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

module.exports = {
  transfertsend,
  usergroupe,
  transfertusersend,
  transfertreclamation,
};
