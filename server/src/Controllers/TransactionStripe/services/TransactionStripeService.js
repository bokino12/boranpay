const models = require('../../../../models');
const rechargeService = require('./TransactionStripeServicesTraitement');

const anountTraitementdonationUser = async (
  req,
  res,
  categoryTransation,
  Title,
  TotalAmount,
  userCurrency,
  userIdRecev,
  userIdSend,
  donation,
) => {
  const newAmountSave = TotalAmount; // Bon savoir! utiliser ligne pour gerre les taxe d'envoie
  /** Ici je veux sauvegarder toutes les transaction en EUR (Euro Union euro) */
  const Inputcurrency = userCurrency;
  const currencies = await models.currency.findAll();
  const getCurrenciesTocheck = async (currencyData) => {
    const newAmountConverted = (newAmountSave / currencyData.currencyNumber);
    const newAmountConvertedTaxe = (newAmountConverted * 3) / 100; // Ici je calcule les taxes
    const newAmountConvertedTaxeTotal = (newAmountConverted - newAmountConvertedTaxe);
    const newAmountRecevConverted = newAmountConvertedTaxeTotal;
    switch (Inputcurrency) {
      case 'EUR':
        if (Inputcurrency === currencyData.code) {
          rechargeService.stripedonationcharge(
            req,
            res,
            newAmountRecevConverted,
            currencyData,
            categoryTransation,
            newAmountConverted,
            newAmountConvertedTaxe,
            Title,
            userCurrency,
            userIdSend,
            userIdRecev,
            donation,
          );
        }
        break;
      default:
      { // accolade ajoutée
        console.log('Aucune action reçue.');
        break;
      } // accolade ajoutée
    }
  };

  for (let i = 0; i < currencies.length; i++) { getCurrenciesTocheck(currencies[i]); }
};

module.exports = {
  anountTraitementdonationUser,
};
