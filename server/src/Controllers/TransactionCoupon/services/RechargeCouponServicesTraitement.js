const models = require('../../../../models');
const TransactionMail = require('../../../Mail/TransactionMail');
const { makeSluginNumber, makeSluginID, makeParseIp } = require('../../../../helper/utils');

const rechargeForUser = async (
  req,
  res,
  newAmountSave,
  newAmountSaveConverted,
  Title,
  userCurrency,
  userIdRecev,
  userRecev,
  categoryTransation,
  userId,
  couponNumber,
  coupon,
) => {
  const itemRecev = {
    title: Title,
    userId: userIdRecev,
    usertoId: userId,
    content: req.body.content,
    categoryId: categoryTransation,
    currency: userCurrency,
    statusSend: true,
    ip: makeParseIp(req),
    slugin: makeSluginID(30),
    tokenTransaction: makeSluginID(60),
    invoiceNumber: makeSluginNumber(12),
  };

  const transaction = await models.transaction.create(itemRecev);
  if (!transaction) {
    return res.status(400).json({ message: 'Invalid data' });
  }

  const itemAmountRecev = {
    total: newAmountSave,
    currency: userCurrency,
    transactionId: transaction.id,
  };

  /** Ici je verify si la validation du montant est vrai puis je sauvegarde */
  const amount = await models.amount.create(itemAmountRecev);

  // Le + va alle ajouter le solde dans le conte de la personne qui envoi
  const newAmountuserRecev = +newAmountSaveConverted;
  const itemAmountUserRecev = {
    amountUser: newAmountuserRecev,
    amountId: amount.id,
    userId: userIdRecev,
    transactionId: amount.transactionId,
  }; // Cette ligne c'est pour sauvegarder la reception

  const amountuser = await models.amountuser.create(itemAmountUserRecev);
  if (!amountuser) {
    return res.status(400).json({ message: 'Invalid data' });
  }
  // Ici c'est pour verifier si c'est un coupon ou pas !
  if (couponNumber) {
    await coupon.update({ status: true, userId: userIdRecev });
  }

  TransactionMail.transactionCouponMail(userRecev, transaction, itemAmountRecev);
  return res.status(200).json({
    message: 'Data save successfully',
    transaction,
  });
};

const rechargedonationForUser = async (
  req,
  res,
  couponNumber,
  coupon,
  newAmountRecevConverted,
  currencyData,
  categoryTransation,
  newAmountConvertedTaxe,
  Title,
  userCurrency,
  userIdRecev,
  userIdSend,
  donation,
) => {
  /** ces variable ci dessous c'est pour declarer les donner a sauvergarder */
  const myTaxTransaction = newAmountConvertedTaxe * currencyData.currencyNumber; // get taxe
  const itemRecev = {
    title: Title,
    userId: userIdRecev,
    usertoId: userIdSend,
    content: req.body.content,
    donationId: donation.id,
    categoryId: categoryTransation,
    currency: userCurrency,
    statusSend: true,
    ip: makeParseIp(req),
    slugin: makeSluginID(30),
    tokenTransaction: makeSluginID(60),
    invoiceNumber: makeSluginNumber(12),
  };
  const transactionRecev = await models.transactiondonation.create(itemRecev);

  const itemAmountRecev = {
    total: (newAmountRecevConverted * currencyData.currencyNumber),
    taxeTransaction: myTaxTransaction,
    currency: userCurrency,
    transactiondonationId: transactionRecev.id,
  };

  const amount = await models.amount.create(itemAmountRecev);
  if (!amount) { return res.status(400).json({ message: 'Invalid data' }); }
  // Le + va alle ajouter le solde dans le conte de la personne qui envoi
  const newAmountuserRecev = +newAmountRecevConverted;
  const itemAmountUserRecev = {
    amountDonation: newAmountuserRecev,
    amountDonationNoTaxe: myTaxTransaction,
    amountId: amount.id,
    userId: userIdRecev,
    donationId: donation.id,
  }; // Cette ligne c'est pour sauvegarder la reception

  const responseRecev = await models.amountdonation.create(itemAmountUserRecev);
  if (!responseRecev) { return res.status(400).json({ message: 'Invalid data' }); }

  if (categoryTransation === 4) {
    const itemContribute = {
      userId: userIdSend,
      content: req.body.content,
      statusUser: req.body.statusUser,
      statusTotal: req.body.statusTotal,
      taxeContribute: myTaxTransaction,
      amountContribute: responseRecev.amountDonation,
      donationId: donation.id,
      currency: userCurrency,
      transactiondonationId: transactionRecev.id,
      total: (((responseRecev.amountDonation) * currencyData.currencyNumber) + myTaxTransaction),
      slugin: makeSluginID(30),
      ip: makeParseIp(req),
    };
    const contribute = await models.contribute.create(itemContribute);
    if (!contribute) { return res.status(400).json({ message: 'Invalid data' }); }
    // Ici c'est pour verifier si c'est un coupon ou pas !
    if (couponNumber) { await coupon.update({ status: true, userId: userIdRecev }); }
  }

  return res.status(200).json({
    message: 'Data save successfully',
    myTaxTransaction,
  });
};

const rechargecagnoteForUser = async (
  req,
  res,
  couponNumber,
  coupon,
  newAmountRecevConverted,
  currencyData,
  categoryTransation,
  newAmountConvertedTaxe,
  Title,
  userCurrency,
  userIdRecev,
  userIdSend,
  cagnote,
) => {
  /** ces variable ci dessous c'est pour declarer les donner a sauvergarder */
  const myTaxTransaction = newAmountConvertedTaxe * currencyData.currencyNumber; // get taxe
  const itemRecev = {
    title: Title,
    userId: userIdRecev,
    usertoId: userIdSend,
    content: req.body.content,
    cagnoteId: cagnote.id,
    categoryId: categoryTransation,
    currency: userCurrency,
    statusSend: true,
    ip: makeParseIp(req),
    slugin: makeSluginID(30),
    tokenTransaction: makeSluginID(60),
    invoiceNumber: makeSluginNumber(12),
  };
  const transactionRecev = await models.transactiondonation.create(itemRecev);

  const itemAmountRecev = {
    total: (newAmountRecevConverted * currencyData.currencyNumber),
    taxeTransaction: myTaxTransaction,
    currency: userCurrency,
    transactiondonationId: transactionRecev.id,
  };

  const amount = await models.amount.create(itemAmountRecev);
  if (!amount) { return res.status(400).json({ message: 'Invalid data' }); }
  // Le + va alle ajouter le solde dans le conte de la personne qui envoi
  const newAmountuserRecev = +newAmountRecevConverted;
  const itemAmountUserRecev = {
    amountCagnote: newAmountuserRecev,
    amountCagnoteNoTaxe: myTaxTransaction,
    amountId: amount.id,
    userId: userIdRecev,
    cagnoteId: cagnote.id,
  }; // Cette ligne c'est pour sauvegarder la reception

  const responseRecev = await models.amountcagnote.create(itemAmountUserRecev);
  if (!responseRecev) { return res.status(400).json({ message: 'Invalid data' }); }

  if (categoryTransation === 4) {
    const itemContribute = {
      userId: userIdSend,
      content: req.body.content,
      statusUser: req.body.statusUser,
      statusTotal: req.body.statusTotal,
      taxeContribute: myTaxTransaction,
      amountContribute: responseRecev.amountCagnote,
      cagnoteId: cagnote.id,
      currency: userCurrency,
      transactiondonationId: transactionRecev.id,
      total: (((responseRecev.amountCagnote) * currencyData.currencyNumber) + myTaxTransaction),
      slugin: makeSluginID(30),
      ip: makeParseIp(req),
    };
    const contribute = await models.contribute.create(itemContribute);
    if (!contribute) { return res.status(400).json({ message: 'Invalid data' }); }
    // Ici c'est pour verifier si c'est un coupon ou pas !
    if (couponNumber) { await coupon.update({ status: true, userId: userIdRecev }); }
  }

  return res.status(200).json({
    message: 'Data save successfully',
    myTaxTransaction,
  });
};

module.exports = {
  rechargedonationForUser,
  rechargecagnoteForUser,
  rechargeForUser,
};
