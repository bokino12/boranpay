const Validator = require('fastest-validator');
const models = require('../../../models');
const { makeSluginID } = require('../../../helper/utils');
const rechargeService = require('./services/RechargeCouponServicesTraitement');
const { currencyValidate } = require('../../../helper/validatorvalue');

const couponcharge = async (req, res) => {
  const coupon = await models.coupon.findOne({ where: { couponNumber: req.body.total } });
  if (!coupon) {
    res.status(400).json({ message: "Ce coupon n'existe pas veuillez changer ou ajouter un autre" });
  }

  const Title = `Recharge ${process.env.NODE_NAME}`;
  const userRecev = req.AuthUser;
  const userIdRecev = req.AuthUser.userId;
  const { userId } = req.AuthUser;
  const categoryTransation = 1; // Id recharge
  const Total = coupon.couponAmount;
  const couponNumber = req.body.total;
  const userCurrency = coupon.currency;
  const itemRecev = {
    title: Title,
    userId: userIdRecev,
    usertoId: userIdRecev,
    content: req.body.content,
    categoryId: categoryTransation,
    currency: coupon.currency,
    statusSend: true,
    slugin: makeSluginID(30),
  };

  const schema = {
    userId: { type: 'string', positive: true, integer: true },
    currency: { type: 'enum', values: currencyValidate },
    statusSend: 'boolean', // short-hand def
  };

  const v = new Validator();
  const validationResponse = v.validate(itemRecev, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    if (coupon.status !== true) {
      if (validationResponse === true) {
        const newAmountSave = Total; // Bon savoir! utiliser ligne pour gerre les taxe d'envoie
        /** Ici je veux sauvegarder toutes les transaction en EUR (Euro Union euro) */
        const Inputcurrency = userCurrency;
        const currencies = await models.currency.findAll();
        const getCurrenciesTocheck = async (currencyData) => {
          // Cette ligne verify la conversion de la somme en €
          const newAmountSaveConverted = (newAmountSave / currencyData.currencyNumber);
          switch (Inputcurrency) {
            case 'CAD':
              if (Inputcurrency === currencyData.code) {
                rechargeService.rechargeForUser(
                  req,
                  res,
                  newAmountSave,
                  newAmountSaveConverted,
                  Title,
                  userCurrency,
                  userIdRecev,
                  userRecev,
                  categoryTransation,
                  userId,
                  couponNumber,
                  coupon,
                );
              }
              break;
            case 'EUR':
              if (Inputcurrency === currencyData.code) {
                rechargeService.rechargeForUser(
                  req,
                  res,
                  newAmountSave,
                  newAmountSaveConverted,
                  Title,
                  userCurrency,
                  userIdRecev,
                  userRecev,
                  categoryTransation,
                  userId,
                  couponNumber,
                  coupon,
                );
              }
              break;
            case 'GBP':
              if (Inputcurrency === currencyData.code) {
                rechargeService.rechargeForUser(
                  req,
                  res,
                  newAmountSave,
                  newAmountSaveConverted,
                  Title,
                  userCurrency,
                  userIdRecev,
                  userRecev,
                  categoryTransation,
                  userId,
                  couponNumber,
                  coupon,
                );
              }
              break;
            case 'USD':
              if (Inputcurrency === currencyData.code) {
                rechargeService.rechargeForUser(
                  req,
                  res,
                  newAmountSave,
                  newAmountSaveConverted,
                  Title,
                  userCurrency,
                  userIdRecev,
                  userRecev,
                  categoryTransation,
                  userId,
                  couponNumber,
                  coupon,
                );
              }
              break;
            case 'XAF':
              if (Inputcurrency === currencyData.code) {
                rechargeService.rechargeForUser(
                  req,
                  res,
                  newAmountSave,
                  newAmountSaveConverted,
                  Title,
                  userCurrency,
                  userIdRecev,
                  userRecev,
                  categoryTransation,
                  userId,
                  couponNumber,
                  coupon,
                );
              }
              break;
            case 'XOF':
              if (Inputcurrency === currencyData.code) {
                rechargeService.rechargeForUser(
                  req,
                  res,
                  newAmountSave,
                  newAmountSaveConverted,
                  Title,
                  userCurrency,
                  userIdRecev,
                  userRecev,
                  categoryTransation,
                  userId,
                  couponNumber,
                  coupon,
                );
              }
              break;
            default:
            { // accolade ajoutée
              console.log('Aucune action reçue.');
              break;
            }
          }
        };
        for (let i = 0; i < currencies.length; i++) { getCurrenciesTocheck(currencies[i]); }
      } else {
        res.status(400).json({
          errors: validationResponse,
        });
      }
    } else {
      res.status(400).json({ message: 'Ce coupon est dejà utilisé' });
    }
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

const couponchargedonation = async (req, res) => {
  const coupon = await models.coupon.findOne({ where: { couponNumber: req.body.total } });
  if (!coupon) {
    res.status(400).json({ message: "Ce coupon n'existe pas veuillez changer ou ajouter un autre" });
  }
  const donation = await models.donation.findOne({ where: { slugin: req.params.donation } });
  if (!donation) {
    res.status(400).json({ message: "Cette campagne n'est plus disponible" });
  }

  const Title = `Recharge ${process.env.NODE_NAME}`;
  const userIdRecev = donation.userId;
  const userIdSend = req.AuthUser.userId;
  const categoryTransation = 4; // Id transaction
  const Total = coupon.couponAmount;
  const couponNumber = req.body.total;
  const userCurrency = coupon.currency;
  const itemRecev = {
    title: Title,
    userId: userIdRecev,
    usertoId: userIdRecev,
    content: req.body.content,
    categoryId: categoryTransation,
    currency: coupon.currency,
    statusSend: true,
    slugin: makeSluginID(30),
  };

  const schema = {
    userId: { type: 'string', positive: true, integer: true },
    currency: { type: 'enum', values: currencyValidate },
    statusSend: 'boolean', // short-hand def
  };

  const v = new Validator();
  const validationResponse = v.validate(itemRecev, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    if (coupon.status !== true) {
      if (validationResponse === true) {
        const newAmountSave = Total; // Bon savoir! utiliser ligne pour gerre les taxe d'envoie
        /** Ici je veux sauvegarder toutes les transaction en EUR (Euro Union euro) */
        const Inputcurrency = userCurrency;
        const currencies = await models.currency.findAll();
        const getCurrenciesTocheck = async (currencyData) => {
          const newAmountConverted = (newAmountSave / currencyData.currencyNumber);
          const newAmountConvertedTaxe = (newAmountConverted * 2) / 100; // Ici je calcule les taxes
          const newAmountConvertedTaxeTotal = (newAmountConverted - newAmountConvertedTaxe);
          const newAmountRecevConverted = newAmountConvertedTaxeTotal;

          switch (Inputcurrency) {
            case 'CAD':
              if (Inputcurrency === currencyData.code) {
                rechargeService.rechargedonationForUser(
                  req,
                  res,
                  couponNumber,
                  coupon,
                  newAmountRecevConverted,
                  currencyData,
                  categoryTransation,
                  newAmountConvertedTaxe,
                  Title,
                  userCurrency,
                  userIdRecev,
                  userIdSend,
                  donation,
                );
              }
              break;
            case 'EUR':
              if (Inputcurrency === currencyData.code) {
                rechargeService.rechargedonationForUser(
                  req,
                  res,
                  couponNumber,
                  coupon,
                  newAmountRecevConverted,
                  currencyData,
                  categoryTransation,
                  newAmountConvertedTaxe,
                  Title,
                  userCurrency,
                  userIdRecev,
                  userIdSend,
                  donation,
                );
              }
              break;
            case 'GBP':
              if (Inputcurrency === currencyData.code) {
                rechargeService.rechargedonationForUser(
                  req,
                  res,
                  couponNumber,
                  coupon,
                  newAmountRecevConverted,
                  currencyData,
                  categoryTransation,
                  newAmountConvertedTaxe,
                  Title,
                  userCurrency,
                  userIdRecev,
                  userIdSend,
                  donation,
                );
              }
              break;
            case 'USD':
              if (Inputcurrency === currencyData.code) {
                rechargeService.rechargedonationForUser(
                  req,
                  res,
                  couponNumber,
                  coupon,
                  newAmountRecevConverted,
                  currencyData,
                  categoryTransation,
                  newAmountConvertedTaxe,
                  Title,
                  userCurrency,
                  userIdRecev,
                  userIdSend,
                  donation,
                );
              }
              break;
            case 'XAF':
              if (Inputcurrency === currencyData.code) {
                rechargeService.rechargedonationForUser(
                  req,
                  res,
                  couponNumber,
                  coupon,
                  newAmountRecevConverted,
                  currencyData,
                  categoryTransation,
                  newAmountConvertedTaxe,
                  Title,
                  userCurrency,
                  userIdRecev,
                  userIdSend,
                  donation,
                );
              }
              break;
            case 'XOF':
              if (Inputcurrency === currencyData.code) {
                rechargeService.rechargedonationForUser(
                  req,
                  res,
                  couponNumber,
                  coupon,
                  newAmountRecevConverted,
                  currencyData,
                  categoryTransation,
                  newAmountConvertedTaxe,
                  Title,
                  userCurrency,
                  userIdRecev,
                  userIdSend,
                  donation,
                );
              }
              break;
            default:
            { // accolade ajoutée
              console.log('Aucune action reçue.');
              break;
            }
          }
        };
        for (let i = 0; i < currencies.length; i++) { getCurrenciesTocheck(currencies[i]); }
      } else {
        res.status(400).json({
          errors: validationResponse,
        });
      }
    } else {
      res.status(400).json({ message: 'Ce coupon est dejà utilisé' });
    }
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

const couponchargecagnote = async (req, res) => {
  const coupon = await models.coupon.findOne({ where: { couponNumber: req.body.total } });
  if (!coupon) {
    res.status(400).json({ message: "Ce coupon n'existe pas veuillez changer ou ajouter un autre" });
  }
  const cagnote = await models.cagnote.findOne({ where: { slugin: req.params.cagnote } });
  if (!cagnote) {
    res.status(400).json({ message: "Cette campagne n'est plus disponible" });
  }

  const Title = `Recharge ${process.env.NODE_NAME}`;
  const userIdRecev = cagnote.userId;
  const userIdSend = req.AuthUser.userId;
  const categoryTransation = 4; // Id transaction
  const Total = coupon.couponAmount;
  const couponNumber = req.body.total;
  const userCurrency = coupon.currency;
  const itemRecev = {
    title: Title,
    userId: userIdRecev,
    usertoId: userIdRecev,
    content: req.body.content,
    categoryId: categoryTransation,
    currency: coupon.currency,
    statusSend: true,
    slugin: makeSluginID(30),
  };

  const schema = {
    userId: { type: 'string', positive: true, integer: true },
    currency: { type: 'enum', values: currencyValidate },
    statusSend: 'boolean', // short-hand def
  };

  const v = new Validator();
  const validationResponse = v.validate(itemRecev, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    if (coupon.status !== true) {
      if (validationResponse === true) {
        const newAmountSave = Total; // Bon savoir! utiliser ligne pour gerre les taxe d'envoie
        /** Ici je veux sauvegarder toutes les transaction en EUR (Euro Union euro) */
        const Inputcurrency = userCurrency;
        const currencies = await models.currency.findAll();
        const getCurrenciesTocheck = async (currencyData) => {
          const newAmountConverted = (newAmountSave / currencyData.currencyNumber);
          const newAmountConvertedTaxe = (newAmountConverted * 2) / 100; // Ici je calcule les taxes
          const newAmountConvertedTaxeTotal = (newAmountConverted - newAmountConvertedTaxe);
          const newAmountRecevConverted = newAmountConvertedTaxeTotal;

          switch (Inputcurrency) {
            case 'CAD':
              if (Inputcurrency === currencyData.code) {
                rechargeService.rechargecagnoteForUser(
                  req,
                  res,
                  couponNumber,
                  coupon,
                  newAmountRecevConverted,
                  currencyData,
                  categoryTransation,
                  newAmountConvertedTaxe,
                  Title,
                  userCurrency,
                  userIdRecev,
                  userIdSend,
                  cagnote,
                );
              }
              break;
            case 'EUR':
              if (Inputcurrency === currencyData.code) {
                rechargeService.rechargecagnoteForUser(
                  req,
                  res,
                  couponNumber,
                  coupon,
                  newAmountRecevConverted,
                  currencyData,
                  categoryTransation,
                  newAmountConvertedTaxe,
                  Title,
                  userCurrency,
                  userIdRecev,
                  userIdSend,
                  cagnote,
                );
              }
              break;
            case 'GBP':
              if (Inputcurrency === currencyData.code) {
                rechargeService.rechargecagnoteForUser(
                  req,
                  res,
                  couponNumber,
                  coupon,
                  newAmountRecevConverted,
                  currencyData,
                  categoryTransation,
                  newAmountConvertedTaxe,
                  Title,
                  userCurrency,
                  userIdRecev,
                  userIdSend,
                  cagnote,
                );
              }
              break;
            case 'USD':
              if (Inputcurrency === currencyData.code) {
                rechargeService.rechargecagnoteForUser(
                  req,
                  res,
                  couponNumber,
                  coupon,
                  newAmountRecevConverted,
                  currencyData,
                  categoryTransation,
                  newAmountConvertedTaxe,
                  Title,
                  userCurrency,
                  userIdRecev,
                  userIdSend,
                  cagnote,
                );
              }
              break;
            case 'XAF':
              if (Inputcurrency === currencyData.code) {
                rechargeService.rechargecagnoteForUser(
                  req,
                  res,
                  couponNumber,
                  coupon,
                  newAmountRecevConverted,
                  currencyData,
                  categoryTransation,
                  newAmountConvertedTaxe,
                  Title,
                  userCurrency,
                  userIdRecev,
                  userIdSend,
                  cagnote,
                );
              }
              break;
            case 'XOF':
              if (Inputcurrency === currencyData.code) {
                rechargeService.rechargecagnoteForUser(
                  req,
                  res,
                  couponNumber,
                  coupon,
                  newAmountRecevConverted,
                  currencyData,
                  categoryTransation,
                  newAmountConvertedTaxe,
                  Title,
                  userCurrency,
                  userIdRecev,
                  userIdSend,
                  cagnote,
                );
              }
              break;
            default:
            { // accolade ajoutée
              console.log('Aucune action reçue.');
              break;
            }
          }
        };
        for (let i = 0; i < currencies.length; i++) { getCurrenciesTocheck(currencies[i]); }
      } else {
        res.status(400).json({
          errors: validationResponse,
        });
      }
    } else {
      res.status(400).json({ message: 'Ce coupon est dejà utilisé' });
    }
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

module.exports = {
  couponcharge,
  couponchargedonation,
  couponchargecagnote,
};
