const Validator = require('fastest-validator');
const { makeSluginID } = require('../../../helper/utils');
const models = require('../../../models');
const transactionService = require('./services/TransactionRetraitServices');
const { currencyValidate } = require('../../../helper/validatorvalue');

const retraits = async (req, res) => {
  const payementmethoduserItem = await models.payementmethoduser.findOne({
    where: { id: req.body.payementmethodId },
    include: [{ model: models.payementmethod, attributes: ['id', 'name'] }],
  });
  if (!payementmethoduserItem) {
    res.status(400).json({ message: "Ce payement n'existe pas" });
  }
  const payementmethod = await models.payementmethod.findOne({
    where: { id: payementmethoduserItem.payementmethodId },
    include: [
      {
        model: models.country,
        attributes: ['id', 'name'],
        include: [{ model: models.currency, attributes: ['id', 'code'] }],
      },
    ],
  });
  if (!payementmethod) {
    res.status(400).json({ message: "Ce payement n'existe pas" });
  }

  const Title = `Retrait - ${payementmethod?.name} ${payementmethod?.country?.name}`;
  const userIdSend = req.AuthUser.userId;
  const userSend = req.AuthUser;
  const userCurrency = payementmethod?.country?.currency?.code;
  const countryRetrait = payementmethod?.country?.id;
  const categoryTransation = 2; // Id tranfert

  const itemSend = {
    title: Title,
    userId: userIdSend,
    content: req.body.content,
    currency: userCurrency,
    statusSend: false,
    slugin: makeSluginID(30),
  };

  const schema = {
    userId: { type: 'string', positive: true, integer: true },
    currency: { type: 'enum', values: currencyValidate },
    statusSend: 'boolean', // short-hand def
  };

  const v = new Validator();
  const validationResponse = v.validate(itemSend, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    if (validationResponse === true) {
      transactionService.anountTraitementretraitUser(
        req,
        res,
        payementmethod,
        payementmethoduserItem,
        categoryTransation,
        countryRetrait,
        Title,
        userCurrency,
        userSend,
        userIdSend,
      );
    } else {
      res.status(400).json({
        errors: validationResponse,
      });
    }
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

module.exports = {
  retraits,
};
