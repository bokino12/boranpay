const models = require('../../../../models');
const { makeSluginNumber, makeSluginID, makeParseIp } = require('../../../../helper/utils');

const transactionretraitUser = async (
  req,
  res,
  payementmethoduserItem,
  userSend,
  userIdSend,
  newAmountConverted,
  newAmountSaveConverted,
  categoryTransation,
  taxeTransaction,
  countryRetrait,
  Title,
  userCurrency,
  currencyData,
) => {
  const itemSend = {
    title: Title,
    userId: userIdSend,
    usertoId: userIdSend,
    content: req.body.content,
    categoryId: categoryTransation,
    currency: userCurrency,
    statusSend: false,
    slugin: makeSluginID(30),
    ip: makeParseIp(req),
    tokenTransaction: makeSluginID(60),
    invoiceNumber: makeSluginNumber(12),
  };

  const transaction = await models.transaction.create(itemSend);
  if (!transaction) {
    return res.status(400).json({ message: 'Invalid data' });
  }

  const itemAmount = {
    total: (newAmountSaveConverted * currencyData.currencyNumber),
    taxeTransaction: (taxeTransaction * currencyData.currencyNumber),
    currency: userCurrency,
    transactionId: transaction.id,
  };
  const amount = await models.amount.create(itemAmount);
  // Le - va alle soustraire le solde dans le conte de la personne qui envoi
  const newAmountuserSend = -newAmountSaveConverted;
  const itemAmountRecev = {
    amountUser: newAmountuserSend,
    amountId: amount.id,
    userId: userIdSend,
    transactionId: amount.transactionId,
  }; // Cette ligne c'est pour sauvegarder l'envoie

  await models.amountuser.create(itemAmountRecev);

  if (categoryTransation === 2) {
    const itemContribute = {
      userId: userIdSend,
      amountRetrait: (newAmountConverted * currencyData.currencyNumber),
      countryId: countryRetrait,
      payementmethoduserId: payementmethoduserItem.id,
      payementmethodId: payementmethoduserItem.payementmethod.id,
      transactionId: transaction.id,
      slugin: makeSluginID(30),
      ip: makeParseIp(req),
    };

    await models.retrait.create(itemContribute);
  }

  return res.status(200).json({
    message: 'Data save successfully',
    transaction,
  });
};

module.exports = {
  transactionretraitUser,
};
