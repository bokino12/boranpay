const models = require('../../../../models');
const { makeSluginNumber, makeSluginID, makeParseIp } = require('../../../../helper/utils');

/** Cette fuction gerre le virrement de cagnote vers le compte principal */
const transactionviremcaUser = async (
  req,
  res,
  newAmountSendConverted,
  newAmountRecevConverted,
  categoryTransation,
  Title,
  userCurrency,
  currencyData,
  userIdSend,
  userIdRecev,
  // userSend,
  // userRecev,
) => {
  const itemSend = {
    title: Title,
    userId: userIdSend,
    usertoId: userIdRecev,
    content: req.body.content,
    categoryId: categoryTransation,
    currency: userCurrency,
    statusSend: false,
    slugin: makeSluginID(30),
    ip: makeParseIp(req),
    tokenTransaction: makeSluginID(60),
    invoiceNumber: makeSluginNumber(12),
  };

  const transactioncagnote = await models.transactioncagnote.create(itemSend);
  if (!transactioncagnote) {
    return res.status(400).json({ message: 'Invalid data' });
  }
  const itemRecev = {
    title: Title,
    userId: userIdRecev,
    usertoId: userIdSend,
    content: req.body.content,
    categoryId: categoryTransation,
    currency: userCurrency,
    statusSend: true,
    slugin: makeSluginID(30),
    ip: makeParseIp(req),
    tokenTransaction: makeSluginID(60),
    invoiceNumber: transactioncagnote.invoiceNumber,
  };
  const transactionRecev = await models.transaction.create(itemRecev);

  const itemAmount = {
    total: (newAmountSendConverted * currencyData.currencyNumber),
    currency: userCurrency,
    transactioncagnoteId: transactioncagnote.id,
  };
  const itemAmountRecev = {
    total: (newAmountRecevConverted * currencyData.currencyNumber),
    currency: userCurrency,
    transactionId: transactionRecev.id,
  };

  const amount = await models.amount.create(itemAmount);
  await models.amount.create(itemAmountRecev);

  // Le - va alle soustraire le solde dans le conte de la personne qui envoi
  const newAmountuserSend = -newAmountSendConverted;
  // Le + va alle ajouter le solde dans le conte de la personne qui envoi
  const newAmountuserRecev = +newAmountRecevConverted;
  const itemAmountUserSend = {
    amountCagnote: newAmountuserSend,
    amountId: amount.id,
    userId: userIdSend,
  }; // Cette ligne c'est pour sauvegarder l'envoie
  const itemAmountUserRecev = {
    amountUser: newAmountuserRecev,
    amountId: amount.id,
    userId: userIdRecev,
  }; // Cette ligne c'est pour sauvegarder la reception

  const responseSend = await models.amountcagnote.create(itemAmountUserSend);
  if (!responseSend) { return res.status(400).json({ message: 'Invalid data' }); }
  await models.amountuser.create(itemAmountUserRecev);

  return res.status(200).json({
    message: 'Data save successfully',
    transactioncagnote,
  });
};

/** cete partie s'occupe de la transaction sur les
 * cagnotes quie est tres different des transaction simple
 * */
const transactioncagnoteUser = async (
  req,
  res,
  newAmountConverted,
  newAmountConvertedTaxe,
  newAmountSendConverted,
  newAmountRecevConverted,
  categoryTransation,
  Title,
  currencyData,
  userCurrency,
  userIdRecev,
  userIdSend,
  cagnote,
) => {
  const itemSend = {
    title: Title,
    userId: userIdSend,
    usertoId: userIdRecev,
    content: req.body.content,
    categoryId: categoryTransation,
    currency: userCurrency,
    statusSend: false,
    slugin: makeSluginID(30),
    ip: makeParseIp(req),
    tokenTransaction: makeSluginID(60),
    invoiceNumber: makeSluginNumber(12),
  };

  const transaction = await models.transaction.create(itemSend);
  if (!transaction) { return res.status(400).json({ message: 'Invalid data' }); }
  const myTaxTransaction = newAmountConvertedTaxe * currencyData.currencyNumber; // get taxe
  const itemRecev = {
    title: Title,
    userId: userIdRecev,
    usertoId: userIdSend,
    content: req.body.content,
    cagnoteId: cagnote.id,
    categoryId: categoryTransation,
    currency: userCurrency,
    statusSend: true,
    slugin: makeSluginID(30),
    ip: makeParseIp(req),
    tokenTransaction: makeSluginID(60),
    invoiceNumber: transaction.invoiceNumber,
  };
  const transactionRecev = await models.transactioncagnote.create(itemRecev);

  const itemAmount = {
    total: (newAmountSendConverted * currencyData.currencyNumber),
    currency: userCurrency,
    transactionId: transaction.id,
  };

  const itemAmountRecev = {
    total: (newAmountRecevConverted * currencyData.currencyNumber),
    currency: userCurrency,
    transactioncagnoteId: transactionRecev.id,
    ip: makeParseIp(req),
  };

  const amount = await models.amount.create(itemAmount);
  if (!amount) { return res.status(400).json({ message: 'Invalid data' }); }
  await models.amount.create(itemAmountRecev);
  // Le - va alle soustraire le solde dans le conte de la personne qui envoi
  const newAmountuserSend = -newAmountSendConverted;
  // Le + va alle ajouter le solde dans le conte de la personne qui envoi
  const newAmountuserRecev = +newAmountRecevConverted;
  // Cette ligne c'est pour sauvegarder l'envoie
  const itemAmountUserSend = {
    amountUser: newAmountuserSend,
    amountId: amount.id,
    userId: userIdSend,
    ip: makeParseIp(req),
  };
  const itemAmountUserRecev = {
    amountCagnote: newAmountuserRecev,
    amountId: amount.id,
    userId: userIdRecev,
    cagnoteId: cagnote.id,
    ip: makeParseIp(req),
  }; // Cette ligne c'est pour sauvegarder la reception

  const responseRecev = await models.amountcagnote.create(itemAmountUserRecev);
  if (!responseRecev) { return res.status(400).json({ message: 'Invalid data' }); }
  await models.amountuser.create(itemAmountUserSend);

  if (categoryTransation === 5) {
    const itemContribute = {
      userId: userIdSend,
      content: req.body.content,
      statusUser: req.body.statusUser,
      statusTotal: req.body.statusTotal,
      taxeContribute: myTaxTransaction,
      amountContribute: responseRecev.amountCagnote, // Ici je n'ai pas besoin de convertion
      cagnoteId: cagnote.id,
      currency: userCurrency,
      transactioncagnoteId: transactionRecev.id,
      total: (newAmountConverted * currencyData.currencyNumber),
      slugin: makeSluginID(30),
      ip: makeParseIp(req),
    };
    await models.contribute.create(itemContribute);
  }

  return res.status(200).json({
    message: 'Data save successfully',
    transaction,
  });
};

module.exports = {
  transactionviremcaUser,
  transactioncagnoteUser,
};
