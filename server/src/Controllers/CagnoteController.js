const Validator = require('fastest-validator');
const { Sequelize } = require('sequelize');
const mySlug = require('slug');
const models = require('../../models');
const { makeSluginID, makeSluginNumber, makeParseIp } = require('../../helper/utils');

/** Ici je Get tous les donners de la base de donner **/
const index = async (req, res) => {
  try {
    const cagontes = await models.cagnote.findAll({
      where: { isDelete: false },
      attributes: ['id', 'title', 'slug', 'slugin', 'userId', 'total', 'createdAt', 'updatedAt'],
      include: [
        {
          model: models.user,
          required: false,
          attributes: ['firstName', 'lastName', 'email', 'slugin', 'avatar'],
        },
        {
          model: models.currency,
          required: false,
          attributes: ['code', 'symbol', 'currencyNumber', 'name'],
        },
        {
          model: models.contribute,
          attributes: [
            'cagnoteId',
            [Sequelize.fn('COUNT', Sequelize.col('cagnoteId')), 'contributes_count']],
          group: ['cagnoteId'],
          limit: 999999999999999,
        },
        {
          model: models.amountcagnote,
          attributes: [
            'cagnoteId',
            [Sequelize.fn('sum', Sequelize.col('amountCagnote')), 'totalAmountcagnote'],
            [Sequelize.fn('sum', Sequelize.col('amountCagnoteNoTaxe')), 'totalAmountcagnoteNoTaxe'],
          ],
          group: ['cagnoteId'],
          limit: 999999999999999,
        },
      ],
      order: [['createdAt', 'DESC']],
    });
    res.status(200).json(cagontes);
  } catch (error) {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

const showCagnotuser = async (req, res) => {
  try {
    const cagnotes = await models.cagnote.findAll({
      where: { isDelete: false },
      attributes: ['title', 'slug', 'slugin', 'total', 'isClosing', 'isDelete', 'currencyId', 'createdAt'],
      include: [
        {
          model: models.user,
          where: { slugin: req.params.userslugin },
          attributes: ['firstName', 'lastName', 'email', 'slugin', 'avatar'],
        },
        {
          model: models.currency,
          attributes: ['code', 'symbol', 'currencyNumber', 'name'],
        },
        {
          model: models.contribute,
          attributes: [
            'cagnoteId',
            [Sequelize.fn('COUNT', Sequelize.col('cagnoteId')), 'contributes_count']],
          group: ['cagnoteId'],
          limit: 999999999999999,
        },
        {
          model: models.amountcagnote,
          attributes: [
            'cagnoteId',
            [Sequelize.fn('sum', Sequelize.col('amountCagnote')), 'totalAmountcagnote'],
            [Sequelize.fn('sum', Sequelize.col('amountCagnoteNoTaxe')), 'totalAmountcagnoteNoTaxe'],
          ],
          group: ['cagnoteId'],
          limit: 999999999999999,
        },

      ],
      order: [['createdAt', 'DESC']],
    });
    res.status(200).json(cagnotes);
  } catch (error) {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

/** Ici je Post les donners de la base de donner **/
const store = async (req, res) => {
  const { userId } = req.AuthUser;
  const {
    title, content, total, currencyId,
  } = req.body;
  const item = {
    title,
    content,
    total,
    currencyId,
    userId,
    slug: `${mySlug(title)}-${makeSluginNumber(4)}`,
    ip: makeParseIp(req),
    slugin: makeSluginID(20),
  };

  const schema = {
    title: { type: 'string', min: 3, max: 200 },
    content: { type: 'string', min: 3, max: 65000 },
    total: { type: 'string', positive: true, integer: true },
  };

  const v = new Validator();
  const validationResponse = v.validate(item, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    const cagnote = await models.cagnote.create(item);
    if (cagnote) {
      await models.contribute.create({
        userId,
        cagnoteId: cagnote.id,
      });
      await models.amountcagnote.create({
        userId,
        cagnoteId: cagnote.id,
      });
    }
    res.status(200).json(cagnote);
  } catch (error) {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

const showPublic = async (req, res) => {
  try {
    const cagnote = await models.cagnote.findOne({
      where: { slug: req.params.cagnote },
      include: [
        {
          model: models.user,
          attributes: ['firstName', 'email', 'slugin', 'lastName', 'avatar'],
        },
        {
          model: models.currency,
          attributes: ['code', 'symbol', 'currencyNumber', 'name'],
        },
        {
          model: models.contribute,
          attributes: [
            'cagnoteId',
            [Sequelize.fn('COUNT', Sequelize.col('cagnoteId')), 'contributes_count'],
            [Sequelize.fn('sum', Sequelize.col('amountContribute')), 'totalAmountcontribute']],
          group: ['cagnoteId'],
          limit: 999999999999999,
        },
      ],
    });
    res.status(200).json(cagnote);
  } catch (error) {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

const show = async (req, res) => {
  try {
    const cagnote = await models.cagnote.findOne({
      where: { slugin: req.params.cagnote },
      include: [
        {
          model: models.user,
          where: { slugin: req.params.userslugin },
          attributes: ['firstName', 'email', 'slugin', 'lastName', 'avatar'],
          include: [
            {
              model: models.profile,
              attributes: ['slugin'],
              include: [{ model: models.currency, required: true, attributes: ['code', 'symbol', 'currencyNumber'] }],
            },
          ],
        },

      ],
    });
    res.status(200).json(cagnote);
  } catch (error) {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

const showcontributes = async (req, res) => {
  try {
    const contributes = await models.contribute.findAll({
      include: [
        {
          model: models.user,
          attributes: ['id', 'firstName', 'lastName', 'email', 'slugin', 'avatar'],
        },
        {
          model: models.cagnote,
          where: { slugin: req.params.cagnote },
          attributes: ['id', 'slugin'],
        },
      ],
      order: [['createdAt', 'DESC']],
    });
    res.status(200).json(contributes);
  } catch (error) {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

/** Ici je Update la donne */
const update = async (req, res) => {
  const {
    title, content, total, currencyId,
  } = req.body;
  const item = req.params.cagnote;

  const dataUpdate = {
    title,
    content,
    total,
    currencyId,
    ip: makeParseIp(req),
  };

  const schema = {
    title: { type: 'string', min: 3, max: 200 },
    content: { type: 'string', min: 3, max: 65000 },
    total: { type: 'string', positive: true, integer: true },
  };

  const v = new Validator();
  const validationResponse = v.validate(dataUpdate, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    await models.cagnote.update(dataUpdate, { where: { slugin: item } });
    res.status(200).json({
      message: 'Data update successfully',
    });
  } catch (error) {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

const changestatus = async (req, res) => {
  const dataUpdate = { isDelete: true };
  try {
    await models.cagnote.update(dataUpdate, { where: { slugin: req.params.cagnote } });

    res.status(200).json({
      message: 'Data updated successfully',
    });
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

const closingstatus = async (req, res) => {
  const dataUpdate = { isClosing: true };
  try {
    await models.cagnote.update(dataUpdate, { where: { slugin: req.params.cagnote } });

    res.status(200).json({
      message: 'Data updated successfully',
    });
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

module.exports = {
  index,
  store,
  show,
  update,
  showPublic,
  showCagnotuser,
  showcontributes,
  changestatus,
  closingstatus,
};
