const models = require('../../../../models');
const { makeSluginNumber, makeSluginID, makeParseIp } = require('../../../../helper/utils');

/** Cette funtion gerre le virement de la donnation vers la cagnote */
const transactionviremdoUser = async (
  req,
  res,
  newAmountSendConverted,
  newAmountRecevConverted,
  categoryTransation,
  Title,
  userCurrency,
  currencyData,
  userIdSend,
  userIdRecev,
  // userSend,
  // userRecev,
) => {
  const itemSend = {
    title: Title,
    userId: userIdSend,
    usertoId: userIdRecev,
    content: req.body.content,
    categoryId: categoryTransation,
    currency: userCurrency,
    statusSend: false,
    slugin: makeSluginID(20),
    ip: makeParseIp(req),
    tokenTransaction: makeSluginID(60),
    invoiceNumber: makeSluginNumber(12),
  };

  const transactiondonation = await models.transactiondonation.create(itemSend);
  if (!transactiondonation) {
    return res.status(400).json({ message: 'Invalid data' });
  }
  const itemRecev = {
    title: Title,
    userId: userIdRecev,
    usertoId: userIdSend,
    content: req.body.content,
    categoryId: categoryTransation,
    currency: userCurrency,
    statusSend: true,
    slugin: makeSluginID(20),
    ip: makeParseIp(req),
    tokenTransaction: makeSluginID(60),
    invoiceNumber: transactiondonation.invoiceNumber,
  };
  const transactionRecev = await models.transaction.create(itemRecev);

  const itemAmount = {
    total: (newAmountSendConverted * currencyData.currencyNumber),
    currency: userCurrency,
    transactiondonationId: transactiondonation.id,
  };
  const itemAmountRecev = {
    total: (newAmountRecevConverted * currencyData.currencyNumber),
    currency: userCurrency,
    transactionId: transactionRecev.id,
  };

  const amount = await models.amount.create(itemAmount);
  await models.amount.create(itemAmountRecev);
  // Le - va alle soustraire le solde dans le conte de la personne qui envoi
  const newAmountuserSend = -newAmountSendConverted;
  // Le + va alle ajouter le solde dans le conte de la personne qui envoi
  const newAmountuserRecev = +newAmountRecevConverted;
  const itemAmountUserSend = {
    amountDonation: newAmountuserSend,
    amountId: amount.id,
    userId: userIdSend,
  }; // Cette ligne c'est pour sauvegarder l'envoie
  const itemAmountUserRecev = {
    amountUser: newAmountuserRecev,
    amountId: amount.id,
    userId: userIdRecev,
  }; // Cette ligne c'est pour sauvegarder la reception

  const responseSend = await models.amountdonation.create(itemAmountUserSend);
  if (!responseSend) { return res.status(400).json({ message: 'Invalid data' }); }
  await models.amountuser.create(itemAmountUserRecev);

  return res.status(200).json({
    message: 'Data save successfully',
    transactiondonation,
  });
};

/** cete partie s'occupe de la transaction sur les
 *  donations quie est tres different des transaction simple
*/
const transactiondonationUser = async (
  req,
  res,
  newAmountSendConverted,
  newAmountRecevConverted,
  currencyData,
  categoryTransation,
  newAmountConvertedTaxe,
  Title,
  userCurrency,
  userIdSend,
  userIdRecev,
  donation,
) => {
  /** ces variable ci dessous c'est pour declarer les donner a sauvergarder */

  const itemSend = {
    title: Title,
    userId: userIdSend,
    usertoId: userIdRecev,
    content: req.body.content,
    categoryId: categoryTransation,
    currency: userCurrency,
    statusSend: false,
    slugin: makeSluginID(30),
    ip: makeParseIp(req),
    tokenTransaction: makeSluginID(60),
    invoiceNumber: makeSluginNumber(12),
  };

  const transaction = await models.transaction.create(itemSend);
  const myTaxTransaction = newAmountConvertedTaxe * currencyData.currencyNumber; // get taxe
  if (!transaction) { return res.status(400).json({ message: 'Invalid data' }); }
  const itemRecev = {
    title: Title,
    userId: userIdRecev,
    usertoId: userIdSend,
    content: req.body.content,
    donationId: donation.id,
    categoryId: categoryTransation,
    currency: userCurrency,
    statusSend: true,
    slugin: makeSluginID(30),
    ip: makeParseIp(req),
    tokenTransaction: makeSluginID(60),
    invoiceNumber: transaction.invoiceNumber,
  };
  const transactionRecev = await models.transactiondonation.create(itemRecev);

  const itemAmount = {
    total: (newAmountSendConverted * currencyData.currencyNumber),
    currency: userCurrency,
    transactionId: transaction.id,
  };
  const itemAmountRecev = {
    total: (newAmountRecevConverted * currencyData.currencyNumber),
    taxeTransaction: myTaxTransaction,
    currency: userCurrency,
    transactiondonationId: transactionRecev.id,
  };

  const amount = await models.amount.create(itemAmount);
  if (!amount) { return res.status(400).json({ message: 'Invalid data' }); }
  await models.amount.create(itemAmountRecev);
  // Le - va alle soustraire le solde dans le conte de la personne qui envoi
  const newAmountuserSend = -newAmountSendConverted;
  // Le + va alle ajouter le solde dans le conte de la personne qui envoi
  const newAmountuserRecev = +newAmountRecevConverted;
  const itemAmountUserSend = {
    amountUser: newAmountuserSend,
    amountId: amount.id,
    userId: userIdSend,
  }; // Cette ligne c'est pour sauvegarder l'envoie
  const itemAmountUserRecev = {
    amountDonation: newAmountuserRecev,
    amountDonationNoTaxe: myTaxTransaction,
    amountId: amount.id,
    userId: userIdRecev,
    donationId: donation.id,
  }; // Cette ligne c'est pour sauvegarder la reception

  const responseRecev = await models.amountdonation.create(itemAmountUserRecev);
  if (!responseRecev) { return res.status(400).json({ message: 'Invalid data' }); }
  await models.amountuser.create(itemAmountUserSend);

  if (categoryTransation === 4) {
    const itemContribute = {
      userId: userIdSend,
      content: req.body.content,
      statusUser: req.body.statusUser,
      statusTotal: req.body.statusTotal,
      taxeContribute: myTaxTransaction,
      amountContribute: responseRecev.amountDonation,
      donationId: donation.id,
      currency: userCurrency,
      transactiondonationId: transactionRecev.id,
      total: ((responseRecev.amountDonation) * currencyData.currencyNumber),
      slugin: makeSluginID(30),
      ip: makeParseIp(req),
    };
    await models.contribute.create(itemContribute);
  }

  return res.status(200).json({
    message: 'Data save successfully',
    transaction,
  });
};

module.exports = {
  transactionviremdoUser,
  transactiondonationUser,
};
