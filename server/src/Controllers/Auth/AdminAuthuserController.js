const Validator = require('fastest-validator');
const mySlug = require('slug');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const models = require('../../../models');
const NewAdminUserMail = require('../../Mail/Auth/NewAdminUserMail');
const { makeSluginID } = require('../../../helper/utils');

/** Create new admin site */
const adminstore = async (req, res) => {
  const {
    // eslint-disable-next-line max-len
    firstName, lastName, phone, email, sex, organisationId, role, currencyId, countryId, addresse, city,
  } = req.body;

  try {
    const user = await models.useradmin.findOne({ where: { email } });
    const abilitypermission = await models.abilitypermission.findOne({ where: { name: role } });
    if (user) {
      return res.status(400).json({ message: 'E-mail déjà existant' });
    }

    const Mypassword = makeSluginID(15);
    const item = {
      firstName,
      sex,
      organisationId,
      email,
      addresse,
      city,
      role: abilitypermission.name,
      password: Mypassword,
    };

    const schema = {
      firstName: { type: 'string', min: 3, max: 255 },
      email: { type: 'email', min: 3, max: 255 },
      role: { type: 'enum', values: ['admin', 'moderator'] },
      sex: { type: 'enum', values: ['male', 'female'] },
    };

    const v = new Validator();
    const validationResponse = v.validate(item, schema);

    if (validationResponse !== true) {
      return res.status(400).json({ errors: validationResponse });
    }

    if (validationResponse === true) {
      const hashedPsw = await bcrypt.hash(Mypassword, 12);

      const userRecev = await models.useradmin.create({
        firstName,
        lastName,
        sex,
        organisationId,
        email,
        phone,
        addresse,
        city,
        currencyId,
        countryId,
        role: abilitypermission.name,
        viewpasswd: Mypassword,
        password: hashedPsw,
        username: mySlug(firstName),
        providerToken: makeSluginID(100),
        slugin: makeSluginID(30),
      });
      if (!userRecev) {
        return res.status(400).json({ message: 'Données incorrect' });
      }
      /** Default user */
      await models.ability.create({
        useradminId: userRecev.id,
        action: abilitypermission.action,
        subject: abilitypermission.subject,
      });

      NewAdminUserMail.newadminuserMail(userRecev);
      return res.status(200).json({
        message: 'Inscription faite avec succès',
      });
    }
    return res.status(400).json({
      errors: validationResponse,
    });
  } catch (error) {
    return res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

const adminsupdate = async (req, res) => {
  const {
    // eslint-disable-next-line max-len
    firstName, lastName, phone, email, organisationId, sex, role, currencyId, countryId, addresse, city,
  } = req.body;

  try {
    const user = await models.useradmin.findOne({ where: { slugin: req.params.useradmin } });
    const abilitypermission = await models.abilitypermission.findOne({ where: { name: role } });

    const item = {
      firstName,
      email,
      organisationId,
      sex,
      role: abilitypermission.name,
    };

    const schema = {
      firstName: { type: 'string', min: 3, max: 255 },
      email: { type: 'email', min: 3, max: 255 },
      role: { type: 'enum', values: ['admin', 'moderator'] },
      sex: { type: 'enum', values: ['male', 'female'] },
    };

    const v = new Validator();
    const validationResponse = v.validate(item, schema);

    if (validationResponse !== true) {
      return res.status(400).json({ errors: validationResponse });
    }

    if (validationResponse === true) {
      const userRecev = await user.update({
        firstName,
        sex,
        organisationId,
        email,
        addresse,
        city,
        currencyId,
        countryId,
        lastName,
        phone,
        role: abilitypermission.name,
      });
      if (!userRecev) {
        return res.status(400).json({ message: 'Données incorrect' });
      }
      const myAbility = await models.ability.findOne({ where: { useradminId: user.id } });
      /** Default user */
      if (myAbility) {
        await myAbility.update({
          action: abilitypermission.action,
          subject: abilitypermission.subject,
        });
      }

      return res.status(200).json({
        message: 'Inscription faite avec succès',
      });
    }
    return res.status(400).json({
      errors: validationResponse,
    });
  } catch (error) {
    return res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

/** Login function */
const adminlogin = async (req, res) => {
  const { email, password } = req.body;

  try {
    const user = await models.useradmin.findOne({
      where: { email },
      include: [
        {
          model: models.ability, as: 'ability', attributes: ['action', 'subject'], required: true,
        },
        { model: models.country, attributes: ['code', 'name', 'region', 'slug'], required: true },
        { model: models.currency, attributes: ['code', 'name', 'symbol'], required: true },
      ],
    });

    if (!user) {
      return res.status(400).json({ message: 'E-mail ou mot de passe incorrect' });
    }
    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch) {
      return res.status(400).json({ message: 'E-mail ou mot de passe incorrect' });
    }

    const itemUser = {
      firstName: user.firstName,
      lastName: user.lastName,
      username: user.username,
      sex: user.sex,
      email: user.email,
      avatar: user.avatar,
      slugin: user.slugin,
      userId: user.id,
      role: user.role,
      country: user.country,
      currency: user.currency,
      ability: user.ability,
    };

    const token = jwt.sign(itemUser, process.env.JWT_KEY, {
      expiresIn: process.env.JWT_ADMIN_EXPIRE,
    });

    return res.status(200).json({
      firstName: user.firstName,
      lastName: user.lastName,
      username: user.username,
      sex: user.sex,
      email: user.email,
      avatar: user.avatar,
      slugin: user.slugin,
      userId: user.id,
      role: user.role,
      country: user.country,
      currency: user.currency,
      ability: user.ability,
      accessToken: token,
    });
  } catch (error) {
    return res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

// const passwordreset = async (req, res) => {
//   const { email } = req.body;
//
//   try {
//     const user = await models.user.findOne({ where: { email } });
//     if (!user) {
//      return res.status(400).json({
//  message: "Aucun utilisateur n'a été trouvé avec cette adresse mail" });
//     }
//
//     const itemUser = { email: user.firstName, userId: user.id };
//     const token = jwt.sign(itemUser, process.env.JWT_KEY, { expiresIn: '15m' });
//
//     const userRecev = await models.password_reset.create({ email, token });
//     if (!userRecev) {
//       return res.status(400).json({ message: 'Une erreur est survenus' });
//     }
//     ResetPasswordMail.resetpasswordMail(userRecev);
//     res.status(200).json({
//       message: 'Email send successfully',
//     });
//   } catch (error) {
//     res.status(500).json({
//       message: 'Identifiant incorrect',
//       error,
//     });
//   }
// };

module.exports = {
  adminlogin,
  adminsupdate,
  adminstore,
};
