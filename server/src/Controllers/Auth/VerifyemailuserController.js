const jwt = require('jsonwebtoken');
const models = require('../../../models');
const VerifyEmailUserMail = require('../../Mail/Auth/VerifyEmailUserMail');

/** Cette fucntion ci bas me donne la possibiliter de status du changement d'email */
const verifyemail = async (req, res) => {
  try {
    const user = await models.user.findOne({
      where: { providerToken: req.params.slugin },
      include: [{
        model: models.ability, as: 'ability', attributes: ['action', 'subject'], required: true,
      }],
    });
    if (!user) {
      return res.status(400).json({ message: "Aucun utilisateur n'a été trouvé avec cette adresse mail" });
    }

    const dataUpdate = {
      emailVerifiedStatus: true,
    };

    const newuser = await user.update(dataUpdate);

    const itemUser = {
      firstName: user.firstName,
      lastName: user.lastName,
      username: user.username,
      sex: user.sex,
      email: user.email,
      avatar: user.avatar,
      slugin: user.slugin,
      emailVerifiedStatus: newuser.emailVerifiedStatus,
      userId: user.id,
      role: user.role,
      ability: user.ability,
    };

    const token = jwt.sign(itemUser, process.env.JWT_KEY, {
      expiresIn: process.env.JWT_EXPIRE,
    });

    return res.status(200).json({
      firstName: user.firstName,
      lastName: user.lastName,
      username: user.username,
      sex: user.sex,
      email: user.email,
      avatar: user.avatar,
      slugin: user.slugin,
      emailVerifiedStatus: newuser.emailVerifiedStatus,
      userId: user.id,
      role: user.role,
      ability: user.ability,
      accessToken: token,
    });
  } catch (error) {
    return res.status(500).json({
      message: 'Identifiant incorrect',
      error,
    });
  }
};

const resendemail = async (req, res) => {
  const { slugin } = req.params;
  try {
    const userRecev = await models.user.findOne({ where: { slugin } });

    if (!userRecev) {
      res.status(400).json({ message: 'No user was found with this email address' });
    }
    VerifyEmailUserMail.verifyemailuserMail(userRecev);
    res.status(200).json({
      message: 'Email send successfully',
    });
  } catch (error) {
    res.status(500).json({
      message: 'Identifiant incorrect',
      error,
    });
  }
};
module.exports = {
  verifyemail,
  resendemail,
};
