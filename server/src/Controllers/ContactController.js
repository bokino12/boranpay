const Validator = require('fastest-validator');
// const Queue = require('bull');
const { makeSluginID, makeParseIp } = require('../../helper/utils');
const models = require('../../models');
const ContactUserMail = require('../Mail/ContactUserMail');

// const ContactMessage = new Queue('Message send',
// { redis: { port: 6379, host: '127.0.0.1' } }); // Specify Redis connection using object

/** Ici je Get tous les donners de la base de donner **/
const index = async (req, res) => {
  try {
    const contacts = await models.contact.findAll({
      include: [
        { model: models.user, attributes: ['id', 'firstName', 'lastName', 'slugin', 'email'] },
        { model: models.user, as: 'userto', attributes: ['id', 'firstName', 'lastName', 'slugin', 'email'] },
      ],
      order: [['createdAt', 'DESC']],
    });
    res.status(200).json(contacts);
  } catch (error) {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

/** Ici je Post les donners de la base de donner **/
const store = async (req, res) => {
  const item = {
    name: req.body.name,
    email: req.body.email,
    ip: makeParseIp(req),
    content: req.body.content,
    countryId: req.body.countryId,
    userId: req?.AuthUser?.userId,
    subject: req.body.subject,
    slug: makeSluginID(30),
    status: false,
  };

  const schema = {
    name: { type: 'string', min: 3, max: 200 },
    email: { type: 'email', min: 3, max: 200 },
    subject: { type: 'string', min: 3, max: 200 },
    content: { type: 'string', min: 3, max: 65000 },
    status: 'boolean',
  };

  const v = new Validator();
  const validationResponse = v.validate(item, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    const contact = await models.contact.create(item);
    res.status(200).json({
      message: 'Data save successfully',
      contact,
    });
  } catch (error) {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

const storeuser = async (req, res) => {
  const { content, countryId } = req.body;
  const userRecev = await models.user.findOne({ where: { slugin: req.params.user } });
  const userSend = req.AuthUser;
  const item = {
    name: `${userRecev.firstName || ''} ${userRecev.lastName || ''}`,
    email: userRecev.email,
    content,
    countryId,
    ip: makeParseIp(req),
    userId: userSend.userId,
    usertoId: userRecev.id,
    subject: `Message ${userSend.firstName || ''} ${userSend.lastName || ''}`,
    slug: makeSluginID(30),
    status: false,
  };

  const schema = {
    subject: { type: 'string', min: 3, max: 200 },
    content: { type: 'string', min: 3, max: 65000 },
    status: 'boolean',
  };

  const v = new Validator();
  const validationResponse = v.validate(item, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    const contact = await models.contact.create(item);

    await ContactUserMail.contactuserMail(userRecev, contact);
    res.status(200).json({
      message: 'Data save successfully',
      contact,
    });
  } catch (error) {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

/** Ici je Show une donner les donners de la base de donner **/
const show = async (req, res) => {
  try {
    const contact = await models.contact.findOne({
      where: { slug: req.params.contact },
      include: [{ model: models.user, attributes: ['firstName', 'lastName', 'slugin', 'email'] }],
    });
    res.status(200).json(contact);
  } catch (error) {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

/** Ici je Delete data la donne */
const destroy = async (req, res) => {
  try {
    await models.contact.destroy({
      where: { slug: req.params.contact },
    });
    res.status(200).json({
      message: 'Data delete successfully',
    });
  } catch (error) {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

const mycontacts = async (req, res) => {
  try {
    const contact = await models.contact.findAll({
      include: [
        {
          model: models.user,
          required: true,
          where: { slugin: req.params.user },
          attributes: ['firstName', 'lastName', 'slugin', 'email'],
        },
        {
          model: models.user,
          as: 'userto',
          required: true,
          attributes: ['firstName', 'lastName', 'slugin', 'email'],
        },
      ],
      order: [['createdAt', 'DESC']],
    });
    res.status(200).json(contact);
  } catch (error) {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

const mycontactsuser = async (req, res) => {
  try {
    const contact = await models.contact.findAll({
      include: [
        {
          model: models.user,
          required: true,
          where: { slugin: req.params.user },
          attributes: ['firstName', 'lastName', 'slugin', 'email'],
        },
        {
          model: models.user,
          as: 'userto',
          required: true,
          where: { slugin: req.params.userto },
          attributes: ['firstName', 'lastName', 'slugin', 'email'],
        },
      ],
      order: [['createdAt', 'DESC']],
    });
    res.status(200).json(contact);
  } catch (error) {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

module.exports = {
  index,
  store,
  show,
  mycontacts,
  mycontactsuser,
  storeuser,
  destroy,
};
