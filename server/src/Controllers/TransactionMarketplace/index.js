const Validator = require('fastest-validator');
const models = require('../../../models');
const { makeSluginID } = require('../../../helper/utils');
const transactionmarketService = require('./services/TransactionmarketService');
const { currencyValidate } = require('../../../helper/validatorvalue');

const applicationCheck = async (req, res) => {
  try {
    const applicationVendeur = await models.application.findOne({
      where: { clientId: req.params.clientId },
      include: [
        { model: models.user, attributes: ['id', 'firstName', 'email', 'slugin', 'avatar'] },
        { model: models.applicationsecretekey, where: { secretKey: req.params.clientSecret }, attributes: ['applicationId', 'secretKey'] },
      ],
    });
    if (!applicationVendeur) {
      res.status(400).json({ message: 'This application don\'t exist!' });
    }
    res.status(200).json(applicationVendeur);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

/** Ici je Get tous les transctions faites ou recus par l'utilisateur */
const showTransactionuser = async (req, res) => {
  try {
    const transactionservices = await models.transactionservice.findAll({
      where: { statusApplication: true },
      include: [{
        model: models.user,
        where: { slugin: req.params.user },
        attributes: ['firstName', 'email', 'slugin', 'avatar'],
      },
      { model: models.user, as: 'userto', attributes: ['firstName', 'email', 'slugin', 'avatar'] },
      { model: models.amount, attributes: ['total', 'totalNoTaxe', 'taxeTransaction', 'currency'] },
      ],
      order: [['createdAt', 'DESC']],
    });
    res.status(200).json(transactionservices);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

const tranfertCheckout = async (req, res) => {
  // process.env.BORANPAY_ACCESS_ID
  // process.env.BORANPAY_SECRET_KEY
  const applicationVendeur = await models.application.findOne({
    where: {
      clientId: req.params.clientId,
      statusExistApplication: true,
    },
    include: [{ model: models.user, attributes: ['id', 'firstName', 'email', 'slugin', 'avatar'] }],
  });
  if (!applicationVendeur) {
    res.status(400).json({ message: 'Cet application est inexistant contactez le prepretaire pour plus d\'information' });
  }
  const userAcheteur = await models.user.findOne({ where: { slugin: req.AuthUser.slugin } });
  if (!userAcheteur) {
    res.status(400).json({ message: 'Cet utilisateur est inexistant veuilez insérer un e-mail valide' });
  }

  // console.log('Application =>', userAcheteur)

  const Title = `${req.body.title} ${applicationVendeur.name}`;
  const Total = req.body.total;
  const userSend = userAcheteur;
  const userRecev = applicationVendeur.user;
  const userCurrency = req.body.currency;
  const categoryTransation = 6; // Id Achat & Commerce

  const itemSend = {
    title: Title,
    userId: userSend.id,
    content: req.body.content,
    currency: userCurrency,
    statusSend: false,
    slugin: makeSluginID(30),
  };

  const schema = {
    userId: { type: 'string', positive: true, integer: true },
    currency: { type: 'enum', values: currencyValidate },
    statusSend: 'boolean', // short-hand def
  };

  const v = new Validator();
  const validationResponse = v.validate(itemSend, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    if (validationResponse === true) {
      transactionmarketService.anountTraitementserviceUser(
        req,
        res,
        categoryTransation,
        applicationVendeur,
        Title,
        Total,
        userCurrency,
        userSend,
        userRecev,
      );
    } else {
      res.status(400).json({
        errors: validationResponse,
      });
    }
  } catch (error) {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

const transfertservice = async (req, res) => {
  const user = await models.user.findOne({ where: { email: req.body.email } });
  if (!user) {
    res.status(400).json({ message: 'Cet utilisateur est inexistant veuilez insérer un e-mail valide' });
  }

  const Title = `${req.body.title}`;
  const userIdSend = req.AuthUser.userId;
  const userIdRecev = user.id;
  const userSend = req.AuthUser;
  const userRecev = user;
  const userCurrency = req.body.currency;
  const categoryTransation = 3; // Id tranfert

  const itemSend = {
    title: Title,
    userId: userIdSend,
    content: req.body.content,
    currency: userCurrency,
    statusSend: false,
    slugin: makeSluginID(30),
  };

  const schema = {
    currency: { type: 'enum', values: currencyValidate },
    statusSend: 'boolean', // short-hand def
  };

  const v = new Validator();
  const validationResponse = v.validate(itemSend, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    if (validationResponse === true) {
      transactionmarketService.anountTransfertserviceUser(
        req,
        res,
        categoryTransation,
        Title,
        userCurrency,
        userIdSend,
        userIdRecev,
        userSend,
        userRecev,
      );
    } else {
      res.status(400).json({
        errors: validationResponse,
      });
    }
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

/** Ici je Show une donner les donners de la base de donner **/
const show = async (req, res) => {
  try {
    const transactionservice = await models.transactionservice.findOne({
      where: { slugin: req.params.transactionservice },
      include: [
        {
          model: models.user,
          as: 'userto',
          required: true,
          attributes: ['id', 'firstName', 'email', 'slugin', 'avatar', 'lastName', 'phone'],
          include: [
            {
              model: models.profile,
              required: true,
              include: [
                { model: models.currency, attributes: ['code', 'symbol', 'currencyNumber'] },
                { model: models.country, attributes: ['code', 'name'] },
              ],
            },
          ],
        },
        {
          model: models.user,
          required: true,
          attributes: ['id', 'firstName', 'email', 'slugin', 'avatar', 'lastName', 'phone'],
          include: [
            {
              model: models.profile,
              required: true,
              include: [
                { model: models.currency, attributes: ['code', 'symbol', 'currencyNumber'] },
                { model: models.country, attributes: ['code', 'name'] },
              ],
            },
          ],
        },
        {
          model: models.amount,
          required: true,
          attributes: ['id', 'total', 'totalNoTaxe', 'taxeTransaction', 'currency'],
          // eslint-disable-next-line max-len
          // include: [{ model: models.amountservice, attributes: ['amountId', 'amountService', 'statusApplication'] }],
        },
      ],
    });

    res.status(200).json(transactionservice);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

module.exports = {
  applicationCheck,
  showTransactionuser,
  transfertservice,
  show,
  tranfertCheckout,
};
