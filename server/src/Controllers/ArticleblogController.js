const Validator = require('fastest-validator');
const mySlug = require('slug');
const { Op } = require('sequelize');
const { makeSluginID } = require('../../helper/utils');
const models = require('../../models');

const getPagination = (page, size) => {
  const limit = size ? +size : 3;
  const offset = page ? page * limit : 0;

  return { limit, offset };
};

const getPagingData = (data, page, limit) => {
  const { count: totalItems, rows: articles } = data;
  const currentPage = page ? +page : 0;
  const totalPages = Math.ceil(totalItems / limit);

  return {
    totalItems, articles, totalPages, currentPage,
  };
};

/** Ici je Get tous les donne de la base de donner **/
const search = async (req, res) => {
  const { page, size, fileQuery } = req.query;
  const condition = {
    [Op.or]: [
      { title: { [Op.like]: `%${fileQuery}%` } },
      { '$user.email$': { [Op.like]: `%${fileQuery}%` } },
    ],
  };

  const { limit, offset } = getPagination(page, size);

  models.articleblog.findAndCountAll({
    where: condition,
    include: [
      {
        model: models.user,
        attributes: ['firstName', 'lastName', 'email', 'slugin', 'avatar'],
      },
    ],
    order: [['createdAt', 'DESC']],
    limit,
    offset,
  }).then((data) => {
    const response = getPagingData(data, page, limit);
    res.send(response);
  }).catch((err) => {
    res.status(500).send({
      message:
          err.message || 'Some error occurred while retrieving tutorials.',
    });
  });
};

const index = async (req, res) => {
  try {
    const articleblogs = await models.articleblog.findAll({
      include: [
        {
          model: models.user,
          attributes: ['firstName', 'lastName', 'email', 'slugin', 'avatar'],
        },
      ],
      order: [['createdAt', 'DESC']],
    });
    res.status(200).json(articleblogs);
  } catch (error) {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

/** Ici je Post les donners de la base de donner **/
const store = async (req, res) => {
  const { title, content } = req.body;
  const { userId } = req.AuthUser;
  const item = {
    title,
    userId,
    content,
    slugin: makeSluginID(30),
    slug: `${mySlug(title)}-${makeSluginID(8)}`,
  };

  const schema = {
    title: { type: 'string', min: 3, max: 200 },
    content: { type: 'string', min: 3, max: 65000 },
  };

  const v = new Validator();
  const validationResponse = v.validate(item, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    const response = await models.articleblog.create(item);
    res.status(200).json(response);
  } catch (error) {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

/** Ici je Show une donner les donners de la base de donner **/
const show = async (req, res) => {
  await models.articleblog.findOne({ where: { slugin: req.params.articleblog } }).then((result) => {
    res.status(200).json(result);
  }).catch((error) => {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  });
};

/** Ici je Update la donne */
const update = async (req, res) => {
  const { title, content } = req.body;

  const dataUpdate = {
    title,
    content,
  };

  const schema = {
    name: { type: 'string', min: 3, max: 200 },
    content: { type: 'string', min: 3, max: 65000 },
  };

  const v = new Validator();
  const validationResponse = v.validate(dataUpdate, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  await models.articleblog.update(dataUpdate,
    { where: { slugin: req.params.articleblog } }).then(() => {
    res.status(200).json({
      message: 'Data update successfully',
    });
  }).catch((error) => {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  });
};

module.exports = {
  search,
  index,
  store,
  show,
  update,
};
