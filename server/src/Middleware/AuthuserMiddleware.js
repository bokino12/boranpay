const jwt = require('jsonwebtoken');

const checkAuth = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(' ')[1];
    const accessToken = jwt.verify(token, process.env.JWT_KEY);
    const { userId } = accessToken;
    req.AuthUser = accessToken;
    if (
      req.AuthUser
            && req.AuthUser.userId === userId
    ) {
      next();
    } else {
      res.status(401).send({ message: 'Token is not valid for this user' });
    }
  } catch (e) {
    res.status(401).json({
      message: 'Invalid or expired token provider!',
      error: e,
    });
  }
};

const checkisAdmin = async (req, res, next) => {
  try {
    const token = req.headers.authorization.split(' ')[1];
    const accessToken = jwt.verify(token, process.env.JWT_KEY);
    const { userId } = accessToken;
    req.AuthUser = accessToken;
    if (
      req.AuthUser
            && (req.AuthUser.role === 'moderator'
            || req.AuthUser.role === 'admin')
            && req.AuthUser.userId === userId
    ) {
      next();
    } else {
      res.status(401).send({ message: 'Token is not valid for admin user' });
    }
  } catch (e) {
    res.status(401).json({
      message: 'Invalid or expired token provider!',
      error: e,
    });
  }
};

const checkisSuperAdmin = async (req, res, next) => {
  try {
    const token = req.headers.authorization.split(' ')[1];
    const accessToken = jwt.verify(token, process.env.JWT_KEY);
    const { userId } = accessToken;
    req.AuthUser = accessToken;
    if (
      req.AuthUser
            && req.AuthUser.role === 'admin'
            && req.AuthUser.userId === userId
    ) {
      next();
    } else {
      res.status(401).send({ message: 'Token is not valid for admin user' });
    }
  } catch (e) {
    res.status(401).json({
      message: 'Invalid or expired token provider!',
      error: e,
    });
  }
};

module.exports = {
  checkAuth,
  checkisAdmin,
  checkisSuperAdmin,
};
