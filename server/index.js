const express = require('express');
const cors = require('cors');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const path = require('path');
const helmet = require('helmet');
// const session = require('express-session')
// const serveStatic = require('serve-static');
const app = express();
const logger = require('morgan');

// app.set('views', './resource/views')
// app.set('view engine', 'pug');

/** Ici c'est pour donner la permmision a express du dossier public */
// app.use('/assets', express.static(__dirname + '/public'))

/** Ici je recuprese la route dans le controller */
// const homeRoute = require('./routes/home');
const authRoute = require('./routes/auth');
const usersRoute = require('./routes/users');
const affiliationRoute = require('./routes/affiliations');
const notificationsRoute = require('./routes/notifications');
const categoriesRoute = require('./routes/categories');
const faqsRoute = require('./routes/faqs');
const cagnotesRoute = require('./routes/cagnotes');
const contactreceptionRoute = require('./routes/contactreception');
const reclamationsRoute = require('./routes/reclamations');
const taskRoute = require('./routes/tasks');
const donationRoute = require('./routes/donations');
const retraitRoute = require('./routes/retraits');
const countryRoute = require('./routes/countries');
const payementmethodRoute = require('./routes/paymentmethods');
const paymentmethoduserRoute = require('./routes/paymentmethodusers');
const couponRoute = require('./routes/coupons');
const groupeRoute = require('./routes/groupes');
const usergroupeRoute = require('./routes/usergroupes');
const transactionRoute = require('./routes/transactions');
const statistiquesRoute = require('./routes/statistiques');
const transactionserviceRoute = require('./routes/transactionservice');
const transactiondonationRoute = require('./routes/transactiondonation');
const transactioncagnoteRoute = require('./routes/transactioncagnote');
const contactsRoute = require('./routes/contacts');
const articleblogsRoute = require('./routes/articleblogs');
const organisationRoute = require('./routes/organisations');
const currencyRoute = require('./routes/currencies');
const rechargesRoute = require('./routes/recharges');
const transfertsRoute = require('./routes/transactionuser');
const litigerequestsRoute = require('./routes/litigerequests');
const developpeursRoute = require('./routes/developpeurs');
const useradminRoute = require('./routes/useradmins');

/** Moteur de template */
if (process.env.NODE_ENV === 'production') {
  app.use(express.static(path.join(__dirname, '../client/build')));
  // app.get(/^\/(?!api).*/, (req, res) => {
  // res.sendFile(path.join(__dirname, '../client/build/index.html'))
  // });
  app.get(/^((?!\/api\/v1).)*$/, (req, res) => {
    res.sendFile(path.join(__dirname, '../client/build', 'index.html'));
  });
}

// Express boilerplate middleware
// =============================================
app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(helmet());

app.use(async (req, res, next) => {
  if (req.headers.origin) {
    const { origin } = req.headers;
    res.header('Access-Control-Allow-Origin', origin);
  }
  res.header('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
  next();
});

app.use(cors());

/** app.use('/', [homeRoute]); */
app.use(`/api/${process.env.NODE_API_VERSION}`, [
  authRoute,
  usersRoute,
  notificationsRoute,
  transfertsRoute,
  countryRoute,
  payementmethodRoute,
  paymentmethoduserRoute,
  contactreceptionRoute,
  couponRoute,
  retraitRoute,
  transactionserviceRoute,
  transactiondonationRoute,
  transactioncagnoteRoute,
  donationRoute,
  rechargesRoute,
  groupeRoute,
  affiliationRoute,
  usergroupeRoute,
  organisationRoute,
  currencyRoute,
  transactionRoute,
  statistiquesRoute,
  categoriesRoute,
  cagnotesRoute,
  articleblogsRoute,
  reclamationsRoute,
  faqsRoute,
  taskRoute,
  contactsRoute,
  litigerequestsRoute,
  developpeursRoute,
  useradminRoute,
]);
app.all('*', (req, res) => res.status(404).json('404 Not Found'));

module.exports = app;
