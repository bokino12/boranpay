import dyaxios from './index'
import { authHeader } from '@components/service'

class userServices {

  userCheckValidate(userSite, data) {
    return dyaxios.post(`/user_check/${userSite.slugin}`, data, { headers: authHeader() })
  }

}

export default new userServices()