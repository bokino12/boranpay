import dyaxios from './index'
import { authHeader } from '@components/service'

class donationApiServices {

  getShowEditDonation(slugin, userSite) {
    return dyaxios.get(`/donation/${slugin}/${userSite.slugin}`, { headers: authHeader() })
  }

  /** Save new cagnote */
  postDonation(data) {
    return dyaxios.post(`/donation`, data, { headers: authHeader() })
  }

  /** Update cagnote */
  updateDonation(slugin, data) {
    return dyaxios.put(`/donation/${slugin}`, data, { headers: authHeader() })
  }

  /** Update status cagnote */
  updateStatusDonation(slugin, data) {
    return dyaxios.put(`/donation/${slugin}/status`, data, { headers: authHeader() })
  }

}

export default new donationApiServices()