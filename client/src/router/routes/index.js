// ** Routes Imports
import DashboardRoutes from './Dashboards'
import TransactionRoutes from './Transactions'
import CagnoteRoutes from './Cagnotes'
import DonationRoutes from './Donations'
import AuthRoutes from './Auth'
import PageRoutes from './Pages'
import DeveloppeurRoutes from './Developpeur'
import SupportRoutes from './Support'
import CheckoutRoutes from './Checkout'
// ** Document title
const TemplateTitle = `${process.env.REACT_APP_NAME}`

// ** Default Route
const DefaultRoute = '/dashboard/'

// ** Merge Routes
const Routes = [
  ...DashboardRoutes,
  ...TransactionRoutes,
  ...CagnoteRoutes,
  ...DonationRoutes,
  ...AuthRoutes,
  ...PageRoutes,
  ...CheckoutRoutes,
  ...DeveloppeurRoutes,
  ...SupportRoutes
]

export { DefaultRoute, TemplateTitle, Routes }
