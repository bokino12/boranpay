import { lazy } from 'react'

const PageRoutes = [
  {
    path: `/${process.env.REACT_APP_COUNTRY}/`,
    component: lazy(() => import('../../views/pages/LandingpageUser')),
    exact: true,
    layout: 'BlankLayout',
    meta: { authRoute: true }
  },
  {
    path: '/directories/',
    component: lazy(() => import('../../views/directory/DirectoryUser')),
    exact: true,
    meta: { action: 'read', resource: 'ACL' }
  },
  {
    path: '/user/profile/',
    component: lazy(() => import('../../views/profile/ProfileIndex')),
    exact: true,
    meta: { action: 'read', resource: 'ACL' }
  },
  {
    path: '/ugs/new/',
    component: lazy(() => import('../../views/directory/UserGroupeUserCreateAndEdit')),
    exact: true,
    meta: { action: 'read', resource: 'ACL' }
  },
  {
    path: '/ug/:groupe/',
    component: lazy(() => import('../../views/groupe/UserGroupeUser')),
    meta: { navLink: '/ug' },
    exact: true,
    meta: { action: 'read', resource: 'ACL' }
  },
  {
    path: '/ug/:groupe/new/',
    component: lazy(() => import('../../views/groupe/UserGroupeUserCreate')),
    meta: { navLink: '/ug' },
    exact: true,
    meta: { action: 'read', resource: 'ACL' }
  },
  {
    path: '/ugs/:usergroupe/edit/',
    component: lazy(() => import('../../views/directory/UserGroupeUserCreateAndEdit')),
    exact: true,
    meta: { action: 'read', resource: 'ACL' }
  },
  {
    path: '/ug_transfert/:usergroupe/transfert/',
    component: lazy(() => import('../../views/directory/UserGroupeTransfertUserCreate')),
    exact: true,
    meta: { action: 'read', resource: 'ACL' }
  },
  {
    path: '/groupes/',
    component: lazy(() => import('../../views/groupe/GroupeUser')),
    exact: true,
    meta: { action: 'read', resource: 'ACL' }
  },
  {
    path: '/groupes/new/',
    component: lazy(() => import('../../views/groupe/GroupeUserCreateAndEdit')),
    meta: { navLink: '/groupes' },
    exact: true,
    meta: { navLink: '/groupes', action: 'read', resource: 'ACL' }
  },
  {
    path: '/groupes/:slugin/edit/',
    component: lazy(() => import('../../views/groupe/GroupeUserCreateAndEdit')),
    meta: { navLink: '/groupes' },
    exact: true,
    meta: { navLink: '/groupes', action: 'read', resource: 'ACL' }
  },
  {
    path: '/mypayementmethods/new/',
    component: lazy(() => import('../../views/paymentmethod/PayementmethodCreateAndEdit')),
    meta: { navLink: '/mypayementmethods' },
    exact: true,
    meta: { navLink: '/mypayementmethods', action: 'read', resource: 'ACL' }
  },
  {
    path: '/mypayementmethods/',
    component: lazy(() => import('../../views/paymentmethod/PayementmethodUser')),
    meta: { navLink: '/mypayementmethods' },
    exact: true,
    meta: { navLink: '/mypayementmethods', action: 'read', resource: 'ACL' }
  },  
  {
    path: '/mypayementmethods/:payementmethod/edit/',
    component: lazy(() => import('../../views/paymentmethod/PayementmethodCreateAndEdit')),
    meta: { navLink: '/mypayementmethods' },
    exact: true,
    meta: { navLink: '/mypayementmethods', action: 'read', resource: 'ACL' }
  }
]

export default PageRoutes
