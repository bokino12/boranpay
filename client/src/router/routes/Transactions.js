import { lazy } from 'react'

const TransactionRoutes = [
  {
    path: '/transactions/',
    component: lazy(() => import('../../views/transaction/TransactionUser')),
    exact: true,
    meta: { action: 'read', resource: 'ACL' }
  },
  {
    path: '/marketplaces/',
    component: lazy(() => import('../../views/transaction/TransactionServiceUser')),
    exact: true,
    meta: { action: 'read', resource: 'ACL' }
  },
  {
    path: '/transaction/donations/',
    component: lazy(() => import('../../views/transaction/TransactionDonationsUser')),
    exact: true,
    meta: { action: 'read', resource: 'ACL' }
  },
  {
    path: '/transaction/cagnotes/',
    component: lazy(() => import('../../views/transaction/TransactionCagnoteUser')),
    exact: true,
    meta: { action: 'read', resource: 'ACL' }
  },
  {
    path: '/transactions/:transaction/',
    component: lazy(() => import('../../views/transaction/invoices/InvoiceTransactionUser')),
    exact: true,
    meta: { action: 'read', resource: 'ACL' }
  },
  {
    path: '/transactions_service/:transactionservice/',
    component: lazy(() => import('../../views/transaction/TransactionserviceUserShow')),
    exact: true,
    meta: { action: 'read', resource: 'ACL' }
  },
  {
    path: '/transaction/send/:user/',
    component: lazy(() => import('../../views/transaction/TransactionUserSend')),
    exact: true,
    meta: { action: 'read', resource: 'ACL' }
  },
  {
    path: '/transaction/recev/:user?/',
    component: lazy(() => import('../../views/transaction/TransactionUserRecev')),
    exact: true,
    meta: { action: 'read', resource: 'ACL' }
  },
  {
    path: '/transfert/new/',
    component: lazy(() => import('../../views/transaction/TransfertUserCreate')),
    exact: true,
    meta: { action: 'read', resource: 'ACL' }
  },
  {
    path: '/transfert/user/:user?/new/',
    component: lazy(() => import('../../views/transaction/TransfertToUserCreate')),
    exact: true,
    meta: { action: 'read', resource: 'ACL' }
  },
  {
    path: '/virement_dons/new/',
    component: lazy(() => import('../../views/virements/VirementDonsUserCreate')),
    exact: true,
    meta: { action: 'read', resource: 'ACL' }
  },
  {
    path: '/virement_services/new/',
    component: lazy(() => import('../../views/virements/VirementServiceUserCreate')),
    exact: true,
    meta: { action: 'read', resource: 'ACL' }
  },
  {
    path: '/virement_cagnotes/new/',
    component: lazy(() => import('../../views/virements/VirementCagnoteUserCreate')),
    exact: true,
    meta: { action: 'read', resource: 'ACL' }
  },
  {
    path: '/transaction/send/:usersend?/u/:userrecev?/',
    component: lazy(() => import('../../views/transaction/TransactionUserSendUser')),
    exact: true,
    meta: { action: 'read', resource: 'ACL' }
  },
  {
    path: '/recharge/',
    component: lazy(() => import('../../views/recharge/RechargeUser')),
    exact: true,
    meta: { action: 'read', resource: 'ACL' }
  },
  {
    path: '/recharge/paypal/',
    component: lazy(() => import('../../views/recharge/RechargePaypalUserCreate')),
    exact: true,
    meta: { action: 'read', resource: 'ACL' }
  },
  {
    path: '/requests/',
    component: lazy(() => import('../../views/request/RequestUser')),
    exact: true,
    meta: { action: 'read', resource: 'ACL' }
  },
  {
    path: '/requests/new/',
    component: lazy(() => import('../../views/request/ReclamationUserCreateAndEdit')),
    exact: true,
    meta: { action: 'read', resource: 'ACL' }
  },
  {
    path: '/requests/:reclamation/:userslugin/edit/',
    component: lazy(() => import('../../views/request/ReclamationUserCreateAndEdit')),
    exact: true,
    meta: { action: 'read', resource: 'ACL' }
  },
  {
    path: '/requests/:reclamation/',
    component: lazy(() => import('../../views/request/ReclamationShow')),
    exact: true,
    meta: { action: 'read', resource: 'ACL' }
  },
  {
    path: '/retraits/',
    component: lazy(() => import('../../views/retrait/RetraitUser')),
    exact: true,
    meta: { action: 'read', resource: 'ACL' }
  },
  {
    path: '/retraits/new/',
    component: lazy(() => import('../../views/retrait/RetraitUserCreate')),
    exact: true,
    meta: { action: 'read', resource: 'ACL' }
  },
  {
    path: '/retraits/:retrait/edit/',
    component: lazy(() => import('../../views/retrait/RetraitUserEdit')),
    exact: true,
    meta: { action: 'read', resource: 'ACL' }
  }
]

export default TransactionRoutes
