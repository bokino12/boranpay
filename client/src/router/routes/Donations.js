import { lazy } from 'react'

const DonationRoutes = [
  {
    path: '/donations/',
    component: lazy(() => import('../../views/donation/DonationUser')),
    meta: { navLink: '/donations' },
    exact: true,
    meta: { navLink: '/donations', action: 'read', resource: 'ACL' }
  },
  {
    path: '/donations/:user/new/',
    component: lazy(() => import('../../views/donation/DonationUserCreate')),
    meta: { navLink: '/donations' },
    exact: true,
    meta: { navLink: '/donations', action: 'read', resource: 'ACL' }
  },
  {
    path: '/donation/new/',
    component: lazy(() => import('../../views/donation/DonationCreateAndEdit')),
    exact: true,
    meta: { navLink: '/donation', action: 'read', resource: 'ACL' }
  },
  {
    path: '/donation/:slugin/edit/',
    component: lazy(() => import('../../views/donation/DonationCreateAndEdit')),
    exact: true,
    meta: { navLink: '/donation', action: 'read', resource: 'ACL' }
  },
  {
    path: '/donation/:slugin/statistique/',
    component: lazy(() => import('../../views/donation/DonationStatistiqueUser')),
    exact: true,
    meta: { navLink: '/donation', action: 'read', resource: 'ACL' }
  },
  {
    path: '/donation/:slugin/contribution/',
    component: lazy(() => import('../../views/donation/DonationSendUserCreate')),
    exact: true,
    meta: { navLink: '/donation', action: 'read', resource: 'ACL' }
  }
]

export default DonationRoutes
