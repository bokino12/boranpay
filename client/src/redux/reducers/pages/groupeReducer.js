import produce from "immer"

const initialState = {
    groupe: { user: [] },
    usergroupes: [],
    usergroupe: [],
    groupes: []
}


export default produce((draft, action = {}) => {
    switch (action.type) {
        case 'GET_GROUPES_USER':
            draft.groupes = action.payload
            return
        case 'GET_USERGROUPES_USER':
            draft.usergroupes = action.payload
            return
        case 'GET_SHOW_USERGROUPESDIRECTORY_USER':
            draft.usergroupes = action.payload
            return
        case 'GET_SHOW_GROUPE':
            draft.groupe = action.payload
            return
        case 'GET_SHOW_USERGROUPE':
            draft.usergroupe = action.payload
            return
        case 'DELETE_GROUPE_USER':
            const datadelete = draft.groupes.findIndex(i => i.slugin === action.payload)
            if (datadelete !== -1) draft.groupes.splice(datadelete, 1)
            return draft
        case 'DELETE_USERGROUPE_USER':
            const dataugdelete = draft.usergroupes.findIndex(i => i.slugin === action.payload)
            if (dataugdelete !== -1) draft.usergroupes.splice(dataugdelete, 1)
            return draft
        default:
    }
},
    initialState
)
