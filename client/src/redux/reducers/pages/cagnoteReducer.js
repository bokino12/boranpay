import produce from "immer"

const initialState = {
    cagnote: { user: [] },
    cagnotes: [],
    contributes: []
}


export default produce((draft, action = {}) => {
    switch (action.type) {
        case 'GET_CAGNOTES_USER':
            draft.cagnotes = action.payload
            return
        case 'GET_SHOW_CAGNOTE':
            draft.cagnote = action.payload
            return
        case 'GET_SHOW_CAGNOTE_CONTRIBUTES':
            draft.contributes = action.payload
            return
        case 'CLOSE_CAGNOTE_USER':
            const dataclose = draft.cagnotes.findIndex(i => i.slugin === action.payload)
            if (dataclose !== -1) draft.cagnotes[dataclose].isClosing = action.payload
            return draft
        case 'DELETE_CAGNOTE_USER':
            const datadelete = draft.cagnotes.findIndex(i => i.slugin === action.payload)
            if (datadelete !== -1) draft.cagnotes.splice(datadelete, 1)
            return draft
        default:
    }
},
    initialState
)
