import produce from "immer"

const initialState = {
    faqs: []
}


export default produce((draft, action = {}) => {
    switch (action.type) {
        case 'GET_ALL_FAQS':
            draft.faqs = action.payload
        default:
    }
},
    initialState
)