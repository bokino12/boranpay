import produce from "immer"

const initialState = {
    reclamation: { user: [] },
    reclamations: []
}


export default produce((draft, action = {}) => {
    switch (action.type) {
        case 'GET_RECLAMATIONS_USER':
            draft.reclamations = action.payload
            return
        case 'GET_SHOW_RECLAMATION':
            draft.reclamation = action.payload
            return
        case 'DELETE_RECLAMATION_USER':
            const datadelete = draft.reclamations.findIndex(i => i.slugin === action.payload)
            if (datadelete !== -1) draft.reclamations.splice(datadelete, 1)
            return draft
        default:
    }
},
    initialState
)
