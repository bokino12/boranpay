import { combineReducers } from "redux"
import auth from './auth'
import navbar from './navbar'
import layout from './layout'
import faqReducer from "./pages/faqReducer"
import profileReducer from "./auth/profileReducer"
import transactionReducer from "./pages/transactionReducer"
import currencyReducer from "./pages/currencyReducer"
import countryReducer from "./pages/countryReducer"
import cagnoteReducer from "./pages/cagnoteReducer"
import groupeReducer from "./pages/groupeReducer"
import reclamationReducer from "./pages/reclamationReducer"
import donationReducer from "./pages/donationReducer"
import aplicationdeveloppeurReducer from './pages/aplicationdeveloppeurReducer'


const rootReducer = combineReducers({
  auth,
  navbar,
  layout,
  faqs: faqReducer,
  profile: profileReducer,
  transactions: transactionReducer,
  currencies: currencyReducer,
  countries: countryReducer,
  cagnotes: cagnoteReducer,
  groupes: groupeReducer,
  reclamations: reclamationReducer,
  donations: donationReducer,
  developpeurs: aplicationdeveloppeurReducer
})

export default rootReducer
