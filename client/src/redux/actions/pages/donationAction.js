import {
    GET_SHOW_DONATION,
    GET_DONATIONS_USER,
    DELETE_DONATION_USER,
    GET_SHOW_DONATION_USER
} from '../index'
import axios from "axios"
import { authHeader } from '@components/service'
import Swal from 'sweetalert2'
import { SuccessToast } from '@components/toastalert'
import { toast } from 'react-toastify'

export const loadShowDonationUser = (slugin) => async (dispatch) => {

    await axios.get(`${process.env.REACT_APP_SERVER_NODE_URL}/donation/${slugin}`, { headers: authHeader() })
        .then((response) => (
            dispatch({
                type: GET_SHOW_DONATION,
                payload: response.data
            }))

        ).catch(error => console.error(error))
}


export const ShowDonationUser = (slugin, userslugin) => async (dispatch) => {

    await axios.get(`${process.env.REACT_APP_SERVER_NODE_URL}/donation/${slugin}/${userslugin}`, { headers: authHeader() })
        .then((response) => (
            dispatch({
                type: GET_SHOW_DONATION_USER,
                payload: response.data
            }))

        ).catch(error => console.error(error))
}

export const loadDonationUser = (slugin) => async (dispatch) => {

    await axios.get(`${process.env.REACT_APP_SERVER_NODE_URL}/mydonations/${slugin}/`, { headers: authHeader() })
        .then((response) => (
            dispatch({
                type: GET_DONATIONS_USER,
                payload: response.data
            }))

        ).catch(error => console.error(error))
}

export const deleteItem = (props) => async (dispatch) => {

    Swal.fire({
        title: 'Suppression?',
        text: "Etes-vous sûr de vouloir executer cette action?",
        buttonsStyling: false,
        confirmButtonClass: "btn btn-info",
        cancelButtonClass: 'btn btn-outline-default',
        confirmButtonText: 'Oui, supprimer',
        cancelButtonText: 'Non, annuler',
        showCancelButton: true,
        reverseButtons: false
    }).then(async (result) => {
        if (result.value) {

            //Envoyer la requet au server
            await axios.put(`${process.env.REACT_APP_SERVER_NODE_URL}/donation/${props.slugin}/status`, { headers: authHeader() })
                .then(() => {

                    dispatch({
                        type: DELETE_DONATION_USER,
                        payload: props.slugin
                    })

                    toast.success(
                        <SuccessToast name={'Success'} description={'Don supprimé avec succès'} />, {
                        position: toast.POSITION.TOP_RIGHT,
                        hideProgressBar: true
                      })
                      
                }).catch(error => console.error(error))
        }
    })
}