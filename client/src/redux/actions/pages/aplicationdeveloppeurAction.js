import {
    GET_ALL_APPLICATIONS_USER,
    GET_SHOW_APPLICATIONS_USER,
    CHANGE_UNACTIVE_STATUS_APPLICATION,
    CHANGE_ACTIVE_STATUS_APPLICATION,
    DELETE_APPLICATIONS_USER
} from "../index"
import axios from "axios"
import { authHeader } from '@components/service'
import Swal from 'sweetalert2'
import { SuccessToast } from '@components/toastalert'
import { toast } from 'react-toastify'

export const loadApplicationDevelopperUser = (userslugin) => async (dispatch) => {

    await axios.get(`${process.env.REACT_APP_SERVER_NODE_URL}/developpeur/app_user/${userslugin}`, { headers: authHeader() })
        .then((response) => (
            dispatch({
                type: GET_ALL_APPLICATIONS_USER,
                payload: response.data
            }))

        ).catch(error => console.error(error))
}

export const loadShowApplicationDevelopperUse = (application) => async (dispatch) => {

    try {
        const response = await axios.get(`${process.env.REACT_APP_SERVER_NODE_URL}/developpeur/app/${application}`, { headers: authHeader() })
        if (response) {
            dispatch({
                type: GET_SHOW_APPLICATIONS_USER,
                payload: response.data
            })
        }
    } catch (error) { console.error(error) }
}

//export const handleSearchQuery = val => dispatch => dispatch({ type: 'HANDLE_SEARCH_QUERY', val })

export const onChangeStatus = (application) => async (dispatch) => {

    await axios.get(`${process.env.REACT_APP_SERVER_NODE_URL}/developpeur/app/${application.slugin}/status`, { headers: authHeader() })
        .then(res => (
            dispatch({
                type: application.statusApplication ? CHANGE_UNACTIVE_STATUS_APPLICATION : CHANGE_ACTIVE_STATUS_APPLICATION,
                payload: application.slugin
            }),
            toast.success(
                <SuccessToast name={'Success'} description={'Status change successfully'} />, {
                position: toast.POSITION.TOP_RIGHT,
                hideProgressBar: true
            })
        )).catch(error => console.error(error))
}

export const deleteItem = (props) => async (dispatch) => {

    Swal.fire({
        title: 'Suppression?',
        text: "Êtes-vous sûr de vouloir executer cette action?",
        confirmButtonText: 'Oui, supprimer',
        cancelButtonText: 'Non, annuler',
        buttonsStyling: false,
        confirmButtonClass: "btn btn-primary",
        cancelButtonClass: 'btn btn-outline-default',
        showCancelButton: true,
        reverseButtons: false
    }).then(async (result) => {
        if (result.value) {

            await axios.put(`${process.env.REACT_APP_SERVER_NODE_URL}/developpeur/app/${props.slugin}/status`, { headers: authHeader() })
                .then(() => (
                    dispatch({
                        type: DELETE_APPLICATIONS_USER,
                        payload: props.slugin
                    }),
                    toast.success(
                        <SuccessToast name={'Success'} description={'Application supprimé avec succès'} />, {
                        position: toast.POSITION.TOP_RIGHT,
                        hideProgressBar: true
                    })
                )).catch(error => console.error(error))
        }
    })
}
