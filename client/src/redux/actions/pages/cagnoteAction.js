import {
    GET_SHOW_CAGNOTE,
    GET_CAGNOTES_USER,
    DELETE_CAGNOTE_USER,
    CLOSE_CAGNOTE_USER,
    GET_SHOW_CAGNOTE_CONTRIBUTES
} from '../index'
import axios from 'axios'
import { authHeader } from '@components/service'
import Swal from 'sweetalert2'
import { SuccessToast } from '@components/toastalert'
import { toast } from 'react-toastify'


export const loadShowCagnoteUser = (cagnoteslugin) => async (dispatch) => {

    await axios.get(`${process.env.REACT_APP_SERVER_NODE_URL}/cagnotes_show/${cagnoteslugin}`, { headers: authHeader() })
        .then((response) => (
            dispatch({
                type: GET_SHOW_CAGNOTE,
                payload: response.data
            }))

        ).catch(error => console.error(error))
}

export const loadShowCagnoteUserContributes = (cagnoteslugin) => async (dispatch) => {

    await axios.get(`${process.env.REACT_APP_SERVER_NODE_URL}/cagnotes/${cagnoteslugin}/contributes`, { headers: authHeader() })
        .then((response) => (
            dispatch({
                type: GET_SHOW_CAGNOTE_CONTRIBUTES,
                payload: response.data
            }))

        ).catch(error => console.error(error))
}

export const loadCagnoteUser = (userslugin) => async (dispatch) => {

    await axios.get(`${process.env.REACT_APP_SERVER_NODE_URL}/mycagnotes/${userslugin}`, { headers: authHeader() })
        .then((response) => (
            dispatch({
                type: GET_CAGNOTES_USER,
                payload: response.data
            }))

        ).catch(error => console.error(error))
}

export const closeItem = (cagnote) => async (dispatch) => {

    Swal.fire({
        title: 'Suppression?',
        text: "Êtes-vous sûr de vouloir executer cette action?",
        confirmButtonText: 'Oui, supprimer',
        cancelButtonText: 'Non, annuler',
        buttonsStyling: false,
        confirmButtonClass: "btn btn-primary",
        cancelButtonClass: 'btn btn-outline-default',
        showCancelButton: true,
        reverseButtons: false
    }).then(async (result) => {
        if (result.value) {

            //Envoyer la requet au server
            await axios.put(`${process.env.REACT_APP_SERVER_NODE_URL}/cagnotes/${cagnote.slugin}/close`, { headers: authHeader() })
                .then(() => {

                    dispatch({
                        type: CLOSE_CAGNOTE_USER,
                        payload: cagnote.slugin
                    })

                    toast.success(
                        <SuccessToast name={'Success'} description={'Cagnote cloturé avec succès'} />, {
                        position: toast.POSITION.TOP_RIGHT,
                        hideProgressBar: true
                    })

                }).catch(error => console.error(error))
        }
    })
}
export const deleteItem = (cagnote) => async (dispatch) => {

    Swal.fire({
        title: 'Suppression?',
        text: "Êtes-vous sûr de vouloir executer cette action?",
        confirmButtonText: 'Oui, supprimer',
        cancelButtonText: 'Non, annuler',
        buttonsStyling: false,
        confirmButtonClass: "btn btn-primary",
        cancelButtonClass: 'btn btn-outline-default',
        showCancelButton: true,
        reverseButtons: false
    }).then(async (result) => {
        if (result.value) {

            //Envoyer la requet au server
            await axios.put(`${process.env.REACT_APP_SERVER_NODE_URL}/cagnotes/${cagnote.slugin}/status`, { headers: authHeader() })
                .then(() => {

                    dispatch({
                        type: DELETE_CAGNOTE_USER,
                        payload: cagnote.slugin
                    })

                    toast.success(
                        <SuccessToast name={'Success'} description={'Cagnote supprimé avec succès'} />, {
                        position: toast.POSITION.TOP_RIGHT,
                        hideProgressBar: true
                    })

                }).catch(error => console.error(error))
        }
    })
}