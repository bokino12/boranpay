import {
    GET_ALL_CURRENCIES,
    GET_ALL_ABILITY_FORUSER_PERMISSIONS
} from "../index"
import axios from "axios"
import { authHeader } from '@components/service'


export const loadAllCurrencies = () => async (dispatch) => {

    await axios.get(`${process.env.REACT_APP_SERVER_NODE_URL}/currencies`, { headers: authHeader() })
        .then(response => (
                dispatch({ type: GET_ALL_CURRENCIES, payload: response.data }))

        ).catch(error => console.error(error))
}

export const loadAllAbilityforuserPermissions = () => async (dispatch) => {

    await axios.get(`${process.env.REACT_APP_SERVER_NODE_URL}/ability_foruser_permissions`, { headers: authHeader() })
        .then(response => (
                dispatch({ type: GET_ALL_ABILITY_FORUSER_PERMISSIONS, payload: response.data }))

        ).catch(error => console.error(error))
}