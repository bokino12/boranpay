import {
    GET_ALL_COUNTRIES_RETRAIT,
    GET_ALL_COUNTRIES,
    GET_ALL_PAYEMENTMETHODSUSER_RETRAIT,
    GET_ALL_PAYEMENTMETHODS_RETRAIT,
    GET_ALL_PAYEMENTMETHODS_RECHARGE,
    DELETE_PAYEMENTMETHOD_USER
} from '../index'
import axios from 'axios'
import { authHeader } from '@components/service'
import Swal from 'sweetalert2'
import { SuccessToast } from '@components/toastalert'
import { toast } from 'react-toastify'

export const loadAllCountries = () => async (dispatch) => {

    await axios.get(`${process.env.REACT_APP_SERVER_NODE_URL}/countries`, { headers: authHeader() })
        .then((response) => (
            dispatch({
                type: GET_ALL_COUNTRIES,
                payload: response.data
            }))

        ).catch(error => console.error(error))
}

export const loadAllCountriesretrait = () => async (dispatch) => {

    await axios.get(`${process.env.REACT_APP_SERVER_NODE_URL}/countries_retrait`, { headers: authHeader() })
        .then((response) => (
            dispatch({
                type: GET_ALL_COUNTRIES_RETRAIT,
                payload: response.data
            }))

        ).catch(error => console.error(error))
}

export const loadAllPayementmethodsretrait = () => async (dispatch) => {

    await axios.get(`${process.env.REACT_APP_SERVER_NODE_URL}/payementmethods_retrait`, { headers: authHeader() })
        .then((response) => (
            dispatch({
                type: GET_ALL_PAYEMENTMETHODS_RETRAIT,
                payload: response.data
            }))

        ).catch(error => console.error(error))
}

export const loadAllPayementmethodsuserretrait = (user) => async (dispatch) => {

    await axios.get(`${process.env.REACT_APP_SERVER_NODE_URL}/mypayementmethod/${user.slugin}`, { headers: authHeader() })
        .then((response) => (
            dispatch({
                type: GET_ALL_PAYEMENTMETHODSUSER_RETRAIT,
                payload: response.data
            }))

        ).catch(error => console.error(error))
}

export const loadAllPayementmethodsrechage = () => async (dispatch) => {

    await axios.get(`${process.env.REACT_APP_SERVER_NODE_URL}/payementmethods_recharge`, { headers: authHeader() })
        .then((response) => (
            dispatch({
                type: GET_ALL_PAYEMENTMETHODS_RECHARGE,
                payload: response.data
            }))

        ).catch(error => console.error(error))
}

export const deletePMUItem = (payementmethoduser) => async (dispatch) => {

    Swal.fire({
        title: 'Suppression?',
        text: "Êtes-vous sûr de vouloir executer cette action?",
        confirmButtonText: 'Oui, supprimer',
        cancelButtonText: 'Non, annuler',
        buttonsStyling: false,
        confirmButtonClass: "btn btn-primary",
        cancelButtonClass: 'btn btn-outline-default',
        showCancelButton: true,
        reverseButtons: false
    }).then(async (result) => {
        if (result.value) {

            //Envoyer la requet au server
            await axios.put(`${process.env.REACT_APP_SERVER_NODE_URL}/mypayementmethods/${payementmethoduser.slugin}/status`, { headers: authHeader() })
                .then(() => {

                    dispatch({
                        type: DELETE_PAYEMENTMETHOD_USER,
                        payload: payementmethoduser.slugin
                    })

                    toast.success(
                        <SuccessToast name={'Success'} description={'Supprimé avec succès'} />, {
                        position: toast.POSITION.TOP_RIGHT,
                        hideProgressBar: true
                    })

                }).catch(error => console.error(error))
        }
    })
}