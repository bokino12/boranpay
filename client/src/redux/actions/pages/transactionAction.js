import {
    GET_SHOW_TRANSACTION,
    GET_ALL_TRANSACTIONS_USER,
    GET_ALL_TRANSACTION_USER_SEND,
    GET_ALL_TRANSACTION_USER_RECEV,
    GET_ALL_TRANSACTIONS_CAGNOTES_USER,
    GET_ALL_TRANSACTION_USER_SEND_USER,
    GET_ALL_TRANSACTION_USER_RETRAIT,
    GET_SHOW_TRANSACTION_SERVICE,
    GET_ALL_TRANSACTIONS_SERVICES_USER,
    GET_ALL_TRANSACTIONS_DONATIONS_USER
} from '../index'
import transactionServices from '../../../services/transactionServices'

export const loadShowTransactionUser = (slugin) => async (dispatch) => {

    await transactionServices.getShowTransactionUser(slugin) 
        .then((response) => (
            dispatch({
                type: GET_SHOW_TRANSACTION,
                payload: response.data
            }))

        ).catch(error => console.error(error))
}

export const loadShowTransactionserviceUser = (slugin) => async (dispatch) => {

    try {
        const response = await transactionServices.getShowTransactionserviceUser(slugin) 
        if (response) {
            dispatch({
                type: GET_SHOW_TRANSACTION_SERVICE,
                payload: response.data
            })
        }
    } catch (error) { console.error(error) }
}

export const loadAllTransactionUser = (user) => async (dispatch) => {

    await transactionServices.getAllTransactionUser(user)
        .then((response) => (
            dispatch({
                type: GET_ALL_TRANSACTIONS_USER,
                payload: response.data
            }))

        ).catch(error => console.error(error))
}

export const loadShowTransactionUserSend = (user) => async (dispatch) => {

    await transactionServices.getAllTransactionUserSend(user)
        .then((response) => (
            dispatch({
                type: GET_ALL_TRANSACTION_USER_SEND,
                payload: response.data
            }))

        ).catch(error => console.error(error))
}

export const loadShowTransactionUserSendUser = (usersend, userrecev) => async (dispatch) => {

    await transactionServices.getAllTransactionUserSendUser(usersend, userrecev)
        .then((response) => (
            dispatch({
                type: GET_ALL_TRANSACTION_USER_SEND_USER,
                payload: response.data
            }))

        ).catch(error => console.error(error))
}

export const loadShowTransactionUserRetrait = (user) => async (dispatch) => {

    await transactionServices.getAllTransactionUserRetrait(user)
        .then((response) => (
            dispatch({
                type: GET_ALL_TRANSACTION_USER_RETRAIT,
                payload: response.data
            }))

        ).catch(error => console.error(error))
}

export const loadShowTransactionUserRecev = (user) => async (dispatch) => {

    await transactionServices.getAllTransactionUserRecev(user)
        .then((response) => (
            dispatch({
                type: GET_ALL_TRANSACTION_USER_RECEV,
                payload: response.data
            }))

        ).catch(error => console.error(error))
}

export const loadAllTransactionDonationsUser = (user) => async (dispatch) => {

    await transactionServices.getAllTransactionDonationsUser(user)
        .then((response) => (
            dispatch({
                type: GET_ALL_TRANSACTIONS_DONATIONS_USER,
                payload: response.data
            }))

        ).catch(error => console.error(error))
}

export const loadAllTransactionCagnotesUser = (user) => async (dispatch) => {

    await transactionServices.getAllTransactionCagnotesUser(user)
        .then((response) => (
            dispatch({
                type: GET_ALL_TRANSACTIONS_CAGNOTES_USER,
                payload: response.data
            }))

        ).catch(error => console.error(error))
}

export const loadAllTransactionServicesUser = (user) => async (dispatch) => {

    await transactionServices.getAllTransactionServicesUser(user)
        .then((response) => (
            dispatch({
                type: GET_ALL_TRANSACTIONS_SERVICES_USER,
                payload: response.data
            }))

        ).catch(error => console.error(error))
}