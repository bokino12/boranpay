// ** Navigation sections imports
import dashboards from './dashboards'
import transaction from './transaction'
import pages from './pages'
import developpeurs from './developpeurs'
import supports from './supports'

// ** Merge & Export
export default [
    ...dashboards,
    ...transaction,
    ...pages,
    ...developpeurs,
    ...supports
]