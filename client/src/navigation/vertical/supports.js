import {
    HelpCircle,
    Info,
    MessageCircle
} from 'react-feather'
export default [
    {
        header: 'Support',
        action: 'read',
        resource: 'ACL'
    },
    {
        id: "faq",
        title: "FAQ",
        icon: <HelpCircle size={20} />,
        action: 'read',
        resource: 'ACL',
        navLink: "/faq/"
    },
    {
        id: "about",
        title: "A propos",
        icon: <Info size={20} />,
        action: 'read',
        resource: 'ACL',
        navLink: "/about/"
    },
    {
        id: "contact",
        title: "Contact",
        icon: <MessageCircle size={20} />,
        action: 'read',
        resource: 'ACL',
        navLink: "/contact/"
    }
]
