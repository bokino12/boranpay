import {
  Circle,
  Octagon,
  PlusSquare
} from 'react-feather'
export default [
  {
    id: "services",
    title: "Services",
    icon: <Octagon size={20} />,
    action: 'read',
    resource: 'ACL',
    children: [
      {
        id: "donations",
        title: "Donations",
        icon: <Circle size={12} />,
        action: 'read',
        resource: 'ACL',
        navLink: "/donations/"
      },
      {
        id: "mycagnotes",
        title: "Cagnotes",
        icon: <Circle size={12} />,
        action: 'read',
        resource: 'ACL',
        navLink: "/mycagnotes/"
      },
      {
          id: "marketplaces",
          title: "Marketplaces",
          icon: <Circle size={12} />,
          action: 'read',
          resource: 'ACL',
          navLink: '/marketplaces/'
      }
    ]
  },
  {
    id: "sections",
    title: "Sections",
    icon: <PlusSquare size={20} />,
    action: 'read',
    resource: 'ACL',
    children: [
      {
        id: "directories",
        title: "Répertoire",
        icon: <Circle size={12} />,
        action: 'read',
        resource: 'ACL',
        navLink: "/directories/"
      },
      {
        id: "groupes",
        title: "Groupes",
        icon: <Circle size={12} />,
        action: 'read',
        resource: 'ACL',
        navLink: "/groupes/"
      }
    ]
  }
]
