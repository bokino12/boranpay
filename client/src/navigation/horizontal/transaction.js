import { Activity, Archive, ArrowDownCircle } from 'react-feather'

export default [
    {
        id: 'transactions',
        title: 'Transactions',
        icon: <Activity size={20} />,
        action: 'read',
        resource: 'ACL',
        navLink: '/transactions/'
    },
    {
        id: 'retraits',
        title: 'Retraits',
        icon: <Archive size={20} />,
        action: 'read',
        resource: 'ACL',
        navLink: '/retraits/'
    },
    {
        id: 'requests',
        title: 'Requests',
        icon: <ArrowDownCircle size={20} />,
        action: 'read',
        resource: 'ACL',
        navLink: '/requests/'
    }
]