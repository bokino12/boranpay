import {
    Settings,
    Circle
} from 'react-feather'

export default [
    {
        id: 'developpeur',
        title: 'Developpeur',
        icon: <Settings size={20} />,
        action: 'read',
        resource: 'ACL',
        children: [
            {
                id: "'applications",
                title: 'Mes applications',
                icon: <Circle size={12} />,
                action: 'read',
                resource: 'ACL',
                navLink: '/developpeur/app/'
            },
            {
                id: 'sandbox',
                title: 'Sandbox',
                icon: <Circle size={12} />,
                action: 'read',
                resource: 'ACL',
                children: [
                    {
                        id: 'compte',
                        title: 'Compte',
                        action: 'read',
                        resource: 'ACL',
                        navLink: '/developpeur/accounts/'
                    }
                ]
            }
        ]
    }
]