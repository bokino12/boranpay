
import { useHistory } from 'react-router-dom'
import { Button } from 'reactstrap'
import { ArrowLeft, ArrowRight } from 'react-feather'

const UndoRedo = () => {
    const history = useHistory()
    return (
        <>
            <Button.Ripple onClick={history.goBack} className="mr-1 mb-1 border-primary text-primary" color="flat-primary" size="sm">
                <ArrowLeft size={14} />
            </Button.Ripple>
            <Button.Ripple onClick={history.goForward} className="mr-1 mb-1 border-primary text-primary" color="flat-primary" size="sm">
                <ArrowRight size={14} />
            </Button.Ripple>
        </>
    )
}
export default UndoRedo