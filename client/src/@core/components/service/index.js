
export const authHeader = () => {
  const user = JSON.parse(localStorage.getItem(process.env.REACT_APP_BASE_NAMETOKEN))
  if (user && user.accessToken) {
    return { Authorization: `Bearer ${user.accessToken}` }
  } else {
    return {}
  }
}

export const authuserInfo = () => {
  const user = JSON.parse(localStorage.getItem(process.env.REACT_APP_BASE_NAMETOKEN))
  const token = user.accessToken
  if (!token) { return }
  const base64Url = token.split('.')[1]
  const base64 = base64Url.replace('-', '+').replace('_', '/')
  return JSON.parse(window.atob(base64))
}

export const logout = () => { localStorage.removeItem(process.env.REACT_APP_BASE_NAMETOKEN) }

export const getCurrentToken = () => JSON.parse(localStorage.getItem(process.env.REACT_APP_BASE_NAMETOKEN))