import {Input} from "reactstrap"

const FieldInput = (props) => {
    const {
        name,
        type,
        placeholder,
        value,
        minLength,
        maxLength,
        rows,
        min,
        step,
        required,
        handleFieldChange,
        hasErrorFor,
        renderErrorFor
    } = props
        return (
            <>
                <Input id={name}
                       min={min}
                       step={step}
                       minLength={minLength}
                       maxLength={maxLength}
                       type={type}
                       required={required}
                       className={`form-control ${hasErrorFor(name) ? 'is-invalid' : ''}`}
                       name={name}
                       placeholder={placeholder}
                       aria-label={placeholder}
                       value={value}
                       rows={rows}
                       onChange={handleFieldChange}
                       autoComplete={name}
                />
                {renderErrorFor(name)}
            </>
        )
}
export default FieldInput