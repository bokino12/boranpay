import React, { useState, useEffect } from "react";
import { Card, CardBody, CardHeader } from "reactstrap"
import DataTable from "react-data-table-component"
import { Link } from "react-router-dom"
import AuthService from "../../../services/authservice";
import { useDispatch, useSelector } from "react-redux"
import { loadDataProfileUser } from "../../../redux/actions/auth/profileActions";
import moment from "moment";


const DataTableLastTransactioncagnote = (props) => {
  const [user] = useState(AuthService.getcurrentUser());
  const profile = useSelector(state => state.profile.dataItem)
  const dispatch = useDispatch();

  useEffect(() => {
    const loadItems = async () => {
      await dispatch(loadDataProfileUser(user))
    }
    loadItems(user)
  }, [user])

  const columns = [
    {
      name: <h5>Date</h5>,
      selector: "date",
      sortable: true
    },
    {
      name: <h5>Montant</h5>,
      selector: "price_transaction",
      sortable: true,
      grow: 2,
    },
    {
      name: <h5>Motif | Identifiant</h5>,
      selector: "title_transaction",
      sortable: true,
      grow: 2,
    },
  ]

  const items = profile.transactioncagnoterecevs;
  const data = items.length >= 0 ? (
    items.map(item =>
    (
      {
        date: <Link to={`/transactions/${item.slugin}/`} className="text-dark"><b>{moment(item.createdAt).format('ll')}</b></Link>,
        price_transaction: <Link to={`/transactions/${item.slugin}/`}><h4 className={`${item.statusSend ? "text-success" : "text-danger"}`}>{item.statusSend ? <>+ {item.amount.total}</> : <>- {item.amount.total}</>} <small>{item.amount.currency}</small></h4></Link>,
        title_transaction: <Link to={`/transactions/${item.slugin}/`} className="text-dark"><b>{(item.title.length > 50 ? item.title.substring(0, 50) + "..." : item.title)}</b></Link>,
      }
    )
    )
  ) : (
      <></>
    );

  return (
    <>
      {items.length > 0 && (
        <Card>
          <CardHeader className='flex-md-row flex-column align-md-items-center align-items-start border-bottom'>
            <div className="tasks-info">
              <h5 className="mb-75">
                <strong>Dernière Transactions des Cagnotes  </strong>
              </h5>
            </div>
          </CardHeader>
          <CardBody>
            <br />
            <DataTable

              data={data}
              columns={columns}
              noHeader />

          </CardBody>


          {items.length >= 5 ?
            <Link to={`/transaction/cagnote/`} className="text-center" >
              <p><b>Voir plus</b></p>
            </Link>
            : ""}

        </Card>
      )}
    </>
  )
}

export default DataTableLastTransactioncagnote;