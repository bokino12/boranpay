import React, { useEffect } from 'react'
import { Card, CardBody, CardHeader, Row, Button } from 'reactstrap'
import DataTable from 'react-data-table-component'
import { useDispatch, useSelector } from 'react-redux'
import { Link, useHistory } from 'react-router-dom'
import { ChevronDown } from 'react-feather'
import Avatar from '@components/avatar'
import { loadDataProfileUser } from '../../../redux/actions/auth/profileActions'
import moment from 'moment'


const renderClient = item => {
    const stateNum = Math.floor(Math.random() * 6),
        states = ['light-success', 'light-danger', 'light-warning', 'light-info', 'light-primary', 'light-secondary'],
        color = states[stateNum]

    if (item?.userto?.avatar) {
        return <Avatar className='mr-50' img={item?.userto?.avatar || 'https://ivemo-cm.s3.eu-central-1.amazonaws.com/img/locations/de2a5dc5106a1d9d1a250251f78436c16c7923b7.png'} width='32' height='32' />
    } else {
        return <Avatar color={color} className='mr-50' content={`${item?.userto?.firstName} ${item?.userto?.lastName}`} initials />
    }
}
const LastTransactionTable = (props) => {
    const history = useHistory()
    const profile = useSelector(state => state.profile.dataItem)
    const dispatch = useDispatch()

    useEffect(() => {
        const loadItems = async () => {
            await dispatch(loadDataProfileUser(props))
        }
        loadItems()
    }, [props])

    const items = profile?.transactions

    const columns = [
        {
            name: <h5>Profile</h5>,
            selector: "profile_user",
            sortable: true,
            grow: 4
        },
        {
            name: <h5>Amount</h5>,
            selector: "price_transaction",
            sortable: true,
            grow: 2
        },
        {
            name: <h5>Date</h5>,
            selector: "date",
            sortable: true,
            grow: 2
        },  
        {
            name: <h5>Motif</h5>,
            selector: "title_transaction",
            sortable: true,
            grow: 3
        }
    ]

    const data = items?.length >= 0 && (
        items.map(item => (

            {
                profile_user: (
                    <>
                        <div className='d-flex justify-content-left align-items-center'>
                            {renderClient(item)}
                            <div className='d-flex flex-column'>
                                <h6 className='user-name text-truncate mb-0'>{item?.userto?.firstName} {item?.userto?.lastName}</h6>
                                <small className='text-truncate text-muted mb-0'>{item?.userto?.email}</small>
                            </div>
                        </div>
                    </>
                ),
                price_transaction: <Link to={`/transactions/${item.slugin}/`}><h4 className={`${item.statusSend ? "text-success" : "text-danger"}`}>{item.statusSend ? <>+ {(item?.amount?.total).formatMoney(2, '.', ',')}</> : <>- {(item?.amount?.total).formatMoney(2, '.', ',')}</>} <small>{item?.amount?.currency}</small></h4></Link>,
                date: <Link to={`/transactions/${item.slugin}/`} className="text-dark"><b>{moment(item.createdAt).format('ll')}</b></Link>,
                title_transaction: <Link to={`/transactions/${item.slugin}/`} className="text-dark"><b>{(item.title.length > 50 ? item.title.substring(0, 50) : item.title)}</b></Link>
            }
        ))
    )

    return (
        <>
            {items.length >= 0 && (
                <Card>
                    <CardHeader className='flex-md-row flex-column align-md-items-center align-items-start border-bottom'>
                        <div className="tasks-info">
                            <h5 className="mb-75">
                                <strong>Dernières Transactions</strong>
                            </h5>
                        </div>
                    </CardHeader>
                    <CardBody>
                        <br />
                        <DataTable
                            className='react-dataTable'
                            sortIcon={<ChevronDown size={10} />}
                            data={data}
                            columns={columns}
                            defaultSortField="createdAt"
                            defaultSortAsc={false}
                            overflowY={true}
                            noHeader />

                        {items.length >= 5 ? (
                            <div className="text-center">
                                <Button.Ripple color={'link'} onClick={() => history.push(`/transactions/`)}>
                                    <p><b>Voir plus</b></p>
                                </Button.Ripple>
                            </div>
                        ) : ""}
                    </CardBody>

                </Card>
            )}

        </>
    )
}

export default LastTransactionTable