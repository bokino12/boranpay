import React from 'react'
import { Button, Card, CardBody, CardText } from 'reactstrap'
import { useHistory } from 'react-router-dom'

const SalesCard = (props) => {
  const history = useHistory()
  return (
    <>
      <Card className='card-congratulations'>
        <CardBody className='text-center'>
          <div className='award-info text-center'>
            <h2 className="mb-2 text-white">{props.sex === "male" ? "Mr" : "Mme"} {props.firstName} {props.lastName} </h2>
            <p className="m-auto mb-0 w-75">
              Nous sommes disponible 7j/7 et 24h/24 et
              pour plus d'information
            </p>
            <br />
            <Button color="info" onClick={() => history.push('/faq/')}>
              Centre d'aide
            </Button>
          </div>
        </CardBody>
      </Card>
    </>
  )
}
export default SalesCard
