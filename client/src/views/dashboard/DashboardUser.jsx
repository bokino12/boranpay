import React, { useState } from 'react'
import HelmetSite from '@components/helmet/HelmetSite'
import { Row, Col, Alert } from 'reactstrap'
import { authuserInfo } from '@components/service'
import LastTransactionTable from './analytics/LastTransactionTable'
import VerificationInfoUser from '../profile/inc/VerificationInfoUser'
import SalesCard from './analytics/SalesCard'
import SoldsCard from './analytics/SoldsCard'

const DashboardUser = () => {
  const [userSite] = useState(authuserInfo())

  return (
    <>
      <HelmetSite title={`Dashboard`} />

      <VerificationInfoUser userItem={userSite} />

      <Row className="match-height">

        <Col lg="7" md="6" sm="6">
          <SoldsCard {...userSite} />
        </Col>

        <Col lg="5" md="6" sm="6">
          <SalesCard {...userSite} />
        </Col>

        <Col lg="12" sm="12">
          <LastTransactionTable {...userSite} />
        </Col>

      </Row>
    </>
  )
}

export default DashboardUser
