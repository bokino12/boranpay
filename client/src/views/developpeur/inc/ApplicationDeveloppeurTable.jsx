import React from 'react'
import { Card, Badge, CardBody, CardHeader, UncontrolledDropdown, DropdownMenu, DropdownToggle, DropdownItem } from 'reactstrap'
import DataTable from 'react-data-table-component'
import { Link } from 'react-router-dom'
import {
    MoreVertical,
    ChevronDown,
    CheckCircle,
    XOctagon,
    Trash,
    Edit
} from 'react-feather'
import moment from 'moment'
import { useDispatch } from 'react-redux'
import { deleteItem, onChangeStatus } from '../../../redux/actions/pages/aplicationdeveloppeurAction'


const ApplicationDeveloppeurTable = ({ applications }) => {
    const dispatch = useDispatch()

    const columns = [
        {
            name: <h5>Name</h5>,
            selector: "name_app",
            sortable: true,
            grow: 2
        },
        {
            name: <h5>Date</h5>,
            selector: "date",
            sortable: true,
            grow: 2
        },
        {
            name: <h5>Status</h5>,
            selector: "status_app",
            sortable: true,
            grow: 4
        },
        {
            name: "",
            selector: "row_button",
            ignoreRowClick: true,
            allowOverflow: true,
            button: true
        }
    ]

    const data = applications?.length >= 0 ? (
        applications.map(item => (
            {
                name_app: <Link to={`/developpeur/app/${item.slugin}/show/`} className="text-dark"><b>{item.name}</b></Link>,
                date: <Link to={`/developpeur/app/${item.slugin}/show/`} className="text-dark"><b>{moment(item.createdAt).format('ll')}</b></Link>,
                status_app: <Link to={`/developpeur/app/${item.slugin}/show/`} className="text-dark">
                    {item.statusApplication ? (
                        <Badge color='success'> <CheckCircle size={14} className='mr-50' /> Application Online </Badge>
                    ) : (<Badge color='danger'> <XOctagon size={14} className='mr-50' /> Application Offline </Badge>)}
                </Link>,
                row_button: (
                    <>
                        <div className='column-action d-flex align-items-center'>
                            <UncontrolledDropdown>
                                <DropdownToggle tag='span'>
                                    <MoreVertical size={17} className='cursor-pointer' />
                                </DropdownToggle>
                                <DropdownMenu right>
                                    <DropdownItem tag='a' href={void (0)}
                                        onClick={() => dispatch(onChangeStatus(item))}
                                        className='w-100'>
                                        {item.statusApplication ? (
                                            <>
                                                <CheckCircle size={14} className='mr-50' />
                                                <span className='align-middle'>Online</span>
                                            </>
                                        ) : (
                                            <>
                                                <XOctagon size={14} className='mr-50' />
                                                <span className='align-middle'>Offline</span>
                                            </>
                                        )}
                                    </DropdownItem>
                                    <DropdownItem tag={Link} to={`/developpeur/app/${item.slugin}/edit/`} className='w-100'>
                                        <Edit size={14} className='mr-50' />
                                        <span className='align-middle'>Editer</span>
                                    </DropdownItem>
                                    <DropdownItem tag='a' href={void (0)} className='w-100' onClick={() => dispatch(deleteItem(item))}>
                                        <Trash size={14} className='mr-50' />
                                        <span className='align-middle'>Supprimer</span>
                                    </DropdownItem>
                                </DropdownMenu>
                            </UncontrolledDropdown>
                        </div>
                    </>
                )
            }
        )
        )
    ) : (
        <></>
    )

    return (
        <>
            <Card>
                <CardHeader className='flex-md-row flex-column align-md-items-center align-items-start border-bottom'>
                    <div className="tasks-info">
                        <h4 className="mb-75">
                            <strong>My apps & credentials</strong>
                        </h4>
                    </div>
                </CardHeader>
                <CardBody>
                    <br />
                    <DataTable
                        className='react-dataTable'
                        sortIcon={<ChevronDown size={10} />}
                        paginationRowsPerPageOptions={[10, 25, 50, 100]}
                        data={data}
                        columns={columns}
                        defaultSortField="createdAt"
                        defaultSortAsc={false}
                        overflowY={true}
                        pagination={true}
                        noHeader />
                </CardBody>
            </Card>
        </>
    )
}


export default ApplicationDeveloppeurTable

