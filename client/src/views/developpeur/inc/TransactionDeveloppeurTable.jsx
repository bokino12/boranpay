import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { ChevronDown } from 'react-feather'
import ReactPaginate from 'react-paginate'
import { CardBody } from 'reactstrap'
import DataTable from 'react-data-table-component'
import moment from 'moment'


const TransactionDeveloppeurTable = ({ items }) => {
    const [currentPage, setCurrentPage] = useState(0)

    const columns = [
        {
            name: <h5>Date</h5>,
            selector: "date",
            sortable: true
        },
        {
            name: <h5>Montant</h5>,
            selector: "price_transaction",
            sortable: true,
            grow: 2
        },
        {
            name: <h5>Motif</h5>,
            selector: "title_transaction",
            sortable: true,
            grow: 2
        }
    ]

    const data = items.length >= 0 && (
        items.map(item => (
            {
                date: <b>{moment(item.createdAt).format('ll')}</b>,
                price_transaction: <h4 className={`${item.statusSend ? "text-success" : "text-danger"}`}>{item.statusSend ? <>+ {(item?.amount?.totalNoTaxe).formatMoney(2, '.', ',')}</> : <>- {(item?.amount?.totalNoTaxe).formatMoney(2, '.', ',')}</>} <small>{item.amount.currency}</small></h4>,
                title_transaction: <b>Test_{item.title}</b>
            }
        ))
    )

    // ** Function to handle Pagination
    const handlePagination = page => {
        setCurrentPage(page.selected)
    }
    // ** Custom Pagination
    const CustomPagination = () => (
        <ReactPaginate
            previousLabel=''
            nextLabel=''
            forcePage={currentPage}
            onPageChange={page => handlePagination(page)}
            pageCount={data.length / 7 || 1}
            breakLabel='...'
            pageRangeDisplayed={2}
            marginPagesDisplayed={2}
            activeClassName='active'
            pageClassName='page-item'
            breakClassName='page-item'
            breakLinkClassName='page-link'
            nextLinkClassName='page-link'
            nextClassName='page-item next'
            previousClassName='page-item prev'
            previousLinkClassName='page-link'
            pageLinkClassName='page-link'
            breakClassName='page-item'
            breakLinkClassName='page-link'
            containerClassName='pagination react-paginate separated-pagination pagination-sm justify-content-end pr-1 mt-1'
        />
    )
    return (
        <CardBody>
            <br />
            <DataTable
                noHeader
                pagination
                columns={columns}
                defaultSortField="createdAt"
                defaultSortAsc={false}
                paginationPerPage={7}
                className='react-dataTable'
                sortIcon={<ChevronDown size={10} />}
                paginationDefaultPage={currentPage + 1}
                paginationComponent={CustomPagination}
                data={data}
            />
        </CardBody>
    )
}

export default TransactionDeveloppeurTable

