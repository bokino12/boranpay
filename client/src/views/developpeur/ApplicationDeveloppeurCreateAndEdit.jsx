import HelmetSite from '@components/helmet/HelmetSite'
import { useDispatch, useSelector } from 'react-redux'
import React, { useState, useEffect } from 'react'
import {
  Card,
  Alert,
  CardBody,
  FormGroup,
  Row,
  Col,
  Form,
  Button,
  Label
} from 'reactstrap'
import { Edit, AlertCircle } from 'react-feather'
import { authHeader } from '@components/service'
import { useParams, useHistory } from 'react-router-dom'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import axios from 'axios'
import { SuccessToast, ErrorToast } from '@components/toastalert'
import { loadShowApplicationDevelopperUse } from '../../redux/actions/pages/aplicationdeveloppeurAction'
import { toast } from 'react-toastify'

const schema = yup.object().shape({
  name: yup.string().required().min(3).max(200)
})

const ApplicationDeveloppeurCreateAndEdit = () => {
  const { application } = useParams()
  const history = useHistory()
  const isAddMode = !application
  const {
    register,
    handleSubmit,
    reset,
    setValue,
    formState
  } = useForm({ resolver: yupResolver(schema) })
  const { errors, isSubmitting } = formState
  /** Get currencies with redux */
  const [errormessage, setErrormessage] = useState('')
  const applicationData = useSelector(state => state.developpeurs?.application)
  const dispatch = useDispatch()

  useEffect(() => {
    const loadItems = async () => {
      if (!isAddMode) {
        await dispatch(loadShowApplicationDevelopperUse(application))
        axios.get(`${process.env.REACT_APP_SERVER_NODE_URL}/developpeur/app/${application}`, { headers: authHeader() })
          .then(groupe => {
            const fields = ['name', 'content']
            fields.forEach(field => setValue(field, groupe.data[field]))
          })
      }
    }
    loadItems()
  }, [application])

  const updateItem = async (application, data) => {
    await axios.put(`${process.env.REACT_APP_SERVER_NODE_URL}/developpeur/app/${application}`, data, { headers: authHeader() })
      .then((res) => {
        toast.success(
          <SuccessToast name={'Success'} description={'Application updated successfully'} />, {
          position: toast.POSITION.TOP_RIGHT,
          hideProgressBar: true
        })
        history.goBack()
      }).catch((error) => {
        toast.error(
          <ErrorToast name={'Opp error'} description={'Oops! Quelque chose ne va pas ...'} />, {
          position: toast.POSITION.TOP_RIGHT,
          hideProgressBar: true
        })
        setErrormessage(error.response.data.message)
      })
  }

  const createItem = async (data, e) => {
    await axios.post(`${process.env.REACT_APP_SERVER_NODE_URL}/developpeur/app`, data, { headers: authHeader() })
      .then((res) => {
        toast.success(
          <SuccessToast name={'Success'} description={'Application create successfully'} />, {
          position: toast.POSITION.TOP_RIGHT,
          hideProgressBar: true
        })
        history.goBack()
      }).catch((error) => {
        toast.error(
          <ErrorToast name={'Opp error'} description={'Oops! Quelque chose ne va pas ...'} />, {
          position: toast.POSITION.TOP_RIGHT,
          hideProgressBar: true
        })
        setErrormessage(error.response.data.message)
      })
  }

  const onSubmit = (data) => {
    return isAddMode ? createItem(data) : updateItem(application, data)
  }

  return (
    <>
      <HelmetSite title={`${isAddMode ? `Nouvelle application` : applicationData.name}`} />
      <Card>

        <CardBody>
          <div className="col-md-10 mx-auto">
            <div className="tasks-info">
              <h4 className="mb-75">
                <strong>{`${isAddMode ? `Nouvelle application` : applicationData.name}`}</strong>&nbsp;
                {/**<Edit size={14} className='cursor-pointer mr-50' /> */}
              </h4>
            </div>
            <Form className="mt-2" onSubmit={handleSubmit(onSubmit)} onReset={reset}>

              {errormessage && (
                <Alert color='danger'>
                  <div className='alert-body text-center'>
                    <AlertCircle size={15} />{' '}
                    <span className='ml-1'>
                      {errormessage}
                    </span>
                  </div>
                </Alert>
              )}

              <Row>

                <Col sm="12">
                  <FormGroup>
                    <Label for="name"><strong>Name</strong></Label>
                    <input className={`form-control ${errors.name ? 'is-invalid' : ''}`}
                      type="text"
                      id="name"
                      {...register('name')}
                      placeholder="Name application"
                      required
                    />
                    <span className='invalid-feedback'>
                      <strong>{errors.name?.message}</strong>
                    </span>
                  </FormGroup>
                </Col>
                <Col sm="12">
                  <FormGroup>
                    <Label for="content"><strong>Description</strong> (facultatif)</Label>
                    <textarea className={`form-control ${errors.content ? 'is-invalid' : ''}`}
                      type="text"
                      rows="2"
                      name="content"
                      id="content"
                      {...register('content')}
                      placeholder={`content`}
                    />
                    <span className='invalid-feedback'>
                      <strong>{errors.content?.message}</strong>
                    </span>
                  </FormGroup>
                </Col>

                <div className="col-md-12 text-center">
                  <FormGroup className="form-label-group">
                    <Button.Ripple
                      disabled={isSubmitting}
                      type="submit"
                      className="btn-block"
                      color="primary"
                    >
                      Sauvegarder
                        </Button.Ripple>
                    <Button.Ripple
                      onClick={() => history.goBack()}
                      className="btn-block"
                      color="default"
                    >
                      Annuler
                        </Button.Ripple>
                  </FormGroup>
                </div>
              </Row>
            </Form>
          </div>
        </CardBody>
      </Card>
    </>
  )
}

export default ApplicationDeveloppeurCreateAndEdit