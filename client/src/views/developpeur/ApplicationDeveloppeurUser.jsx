import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import {
  Plus
} from 'react-feather'
import HelmetSite from '@components/helmet/HelmetSite'
import ApplicationDeveloppeurTable from './inc/ApplicationDeveloppeurTable'
import { authuserInfo } from '@components/service'
import { useDispatch, useSelector } from 'react-redux'
import { loadApplicationDevelopperUser } from '../../redux/actions/pages/aplicationdeveloppeurAction'

const ApplicationDeveloppeurUser = () => {
  const [userSite] = useState(authuserInfo())
  const items = useSelector(state => state?.developpeurs?.applications)
  const dispatch = useDispatch()

  useEffect(() => {
    const loadItems = async () => {
      await dispatch(loadApplicationDevelopperUser(userSite.slugin))
    }
    loadItems()
  }, [userSite.slugin])

  return (
    <>
      <HelmetSite title={`Application`} />

      <div className='text-right' >
        <Link to={`/developpeur/app/create/`} className="mr-1 mb-1 btn btn-primary btn-sm">
          <Plus size={14} /> <b>Create App</b>
        </Link>
      </div>
      <ApplicationDeveloppeurTable
        applications={items}
      />
    </>
  )
}

export default ApplicationDeveloppeurUser
