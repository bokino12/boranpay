import { CardElement, useElements, useStripe } from '@stripe/react-stripe-js'
import React, { useState } from 'react'
import {
    Form,
    Input,
    Label,
    Col,
    Row,
    FormGroup,
    Button
} from 'reactstrap'
import { authHeader } from '@components/service'
import { useHistory } from "react-router-dom"
import axios from "axios"
import { SuccessToast, ErrorToast } from '@components/toastalert'
import { toast } from 'react-toastify'

const CARD_OPTIONS = {
    iconStyle: "solid",
    style: {
        base: {
            iconColor: "#c4f0ff",
            color: "#7367f0",
            fontWeight: 600,
            fontFamily: "Roboto, Open Sans, Segoe UI, sans-serif",
            fontSize: "16px",
            fontSmoothing: "antialiased",
            ":-webkit-autofill": { color: "#fce883" },
            "::placeholder": { color: "#87bbfd" }
        },
        invalid: {
            iconColor: "red",
            color: "red"
        }
    }
}

const RechargeStripeForm = () => {
    const history = useHistory()
    const [inputs, setInputs] = useState({ total: '', currency: 'EUR', name: 'Stripe recharge' })
    const [success, setSuccess] = useState(false)
    const stripe = useStripe()
    const elements = useElements()


    const handleChange = (e) => {
        //get name and value
        const { name, value } = e.target
        setInputs({ ...inputs, [name]: value })
        //same as this.setState  get name and value off input and match to this object 
        // setInputs(prev/* same as prevState */ => ({ ...prev, [name]: value }))
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        const { error, paymentMethod } = await stripe.createPaymentMethod({
            type: "card",
            card: elements.getElement(CardElement)
        })

        if (!error) {
            try {
                const item = { inputs, paymentMethod }
                const response = await axios.post(`${process.env.REACT_APP_SERVER_NODE_URL}/recharges/stripe`, item, { headers: authHeader() })
                if (response.data.success) {
                    setSuccess(true)
                    toast.success(
                        <SuccessToast name={'Success'} description={'Recharge éffectuée avec succès'} />, {
                        position: toast.POSITION.TOP_RIGHT,
                        hideProgressBar: true
                    })
                    history.push('/dashboard/')
                }

            } catch (error) {
                toast.error(
                    <ErrorToast name={'Opp error'} description={'Quelque chose ne va pas ...'} />, {
                    position: toast.POSITION.TOP_RIGHT,
                    hideProgressBar: true
                })
                console.log("Error", error)
            }
        } else {
            console.log(error.message)
        }
    }

    return (
        <>
            {!success && (
                <Form onSubmit={handleSubmit}>
                    <Row>
                        <Col md={8} sm={8}>
                            <FormGroup>
                                <Label for="total"><strong>Montant de la recharge</strong></Label>
                                <Input
                                    className={`form-control`}
                                    pattern="[0-9]*"
                                    inputMode="numeric"
                                    type="number" min="1" step="1" id="total" name="total"
                                    value={inputs.total} onChange={handleChange}
                                    placeholder="Montant recharge" required />
                            </FormGroup>
                        </Col>
                        <Col md={4} sm={4}>
                            <FormGroup>
                                <Label for="total"></Label>
                                <Input
                                    className={`form-control`}
                                    type="currency"
                                    defaultValue={inputs.currency}
                                    placeholder="Devise" readOnly={true} />
                            </FormGroup>
                        </Col>
                    </Row>
                    <FormGroup>
                        <CardElement options={CARD_OPTIONS} />
                    </FormGroup>
                    <Row>
                        <Col md={12} sm={12}>
                            <FormGroup>
                                <Label for="fullName"><strong>Nom complet</strong></Label>
                                <Input
                                    className={`form-control`}
                                    type="text" id="fullName" name="fullName"
                                    value={inputs.fullName} onChange={handleChange}
                                    placeholder="Nom complet" required />
                            </FormGroup>
                        </Col>
                    </Row>
                    <br />
                    <Button.Ripple color='primary' type="submit" block>
                        <b>Terminer</b>
                    </Button.Ripple>
                </Form>
            )}

        </>
    )
}

export default RechargeStripeForm