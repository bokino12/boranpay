import React, { useState } from 'react'
import {
  FormGroup,
  Form,
  Button,
  Alert,
  Label
} from 'reactstrap'
import { AlertCircle } from 'react-feather'
import { useHistory } from "react-router-dom"
import { useForm } from "react-hook-form"
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import axios from "axios"
import { authHeader } from '@components/service'
import { SuccessToast, ErrorToast } from '@components/toastalert'
import { toast } from 'react-toastify'

const schema = yup.object().shape({
  total: yup.number().required().positive().integer()
})

const RechargeCouponForm = () => {
  const history = useHistory()
  const [errormessage, setErrormessage] = useState('')
  const {
    register,
    handleSubmit,
    formState
  } = useForm({ resolver: yupResolver(schema) })
  const { errors, isSubmitting } = formState

  const onSubmit = async (data, e) => {
    await axios.post(`${process.env.REACT_APP_SERVER_NODE_URL}/recharges/coupons`, data, { headers: authHeader() })
      .then((res) => {
        toast.success(
          <SuccessToast name={'Success'} description={'Recharge éffectuée avec succès'} />, {
          position: toast.POSITION.TOP_RIGHT,
          hideProgressBar: true
        })
        history.push('/dashboard/')
      }).catch((error) => {
        toast.error(
          <ErrorToast name={'Opp error'} description={'Quelque chose ne va pas ...'} />, {
          position: toast.POSITION.TOP_RIGHT,
          hideProgressBar: true
        })
        setErrormessage(error.response.data.message)
      })
  }

  return (
    <>
      <Form className="mt-2" onSubmit={handleSubmit(onSubmit)}>

        {errormessage && (
          <>
            <Alert color='danger'>
              <div className='alert-body text-center'>
                <AlertCircle size={15} />{' '}
                <span className='ml-1'>
                  {errormessage}
                </span>
              </div>
            </Alert>
            <br />
          </>
        )}

        <FormGroup>
          <Label for="total"><strong>Insérer le code secret du coupon</strong></Label>
          <input className={`form-control ${errors.total ? 'is-invalid' : ''}`}
            type="number"
            pattern="[0-9]*"
            inputMode="numeric"
            min="1" step="1"
            name="total"
            id="total"
            placeholder="Insérer le code Ex: 3XXXXXX"
            {...register('total')}
            autoComplete="off"
            required
          />
          <span className='invalid-feedback'>
            <strong>{errors.total?.message}</strong>
          </span>
        </FormGroup>
        <Button.Ripple color='primary' disabled={isSubmitting} type="submit" block>
          <b>Coupon recharge</b>
        </Button.Ripple>
        <br />
        <div className="col-md-12 text-center">
          S'il vous plait vérifier que le coupon ne soit pas ancore utilisé puis vérifier bien le numéro sur le coupon avant la recharge
        </div>
      </Form>
    </>
  )
}

export default RechargeCouponForm