import Breadcrumbs from '@components/breadcrumbs'
import HelmetSite from '@components/helmet/HelmetSite'
import UndoRedo from '@components/undoredo'
import {
  Card,
  CardBody,
  Row,
  Col,
  CardHeader
} from 'reactstrap'
import SelectedMethodeCharge from './inc/SelectedMethodeCharge'


const RechargeUser = () => {
  return (
    <>
      <HelmetSite title={`Recharge`} />
      <Breadcrumbs
        breadCrumbTitle="Recharge"
        breadCrumbParent="Function"
        breadCrumbActive="Recharge"
      />

      <UndoRedo />

      <Card>
        <CardBody>

          
          <SelectedMethodeCharge />

        </CardBody>
      </Card>
    </>
  )
}

export default RechargeUser