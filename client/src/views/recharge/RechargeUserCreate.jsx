import React, { useState } from "react";
import Breadcrumbs from "../../../components/@vuexy/breadCrumbs/BreadCrumb"
import HelmetSite from "../../../components/@vuexy/helmet/HelmetSite";
import {
  Card,
  CardBody,
  FormGroup,
  Row,
  Col,
  Form,
  Button,
  Input,
  Label,
} from "reactstrap";
import { ArrowLeft, ArrowRight } from "react-feather";
import StripeCheckout from 'react-stripe-checkout';
import axios from "axios";
import { history } from "../../../history";
import authHeader from '../../../services/authHeader';

import { toast } from "react-toastify";
toast.configure();

const RechargeUserCreate = () => {
  const [inputs, setInputs] = useState({ total: '', currency: 'EUR', name: 'Stripe recharge' })

  const handleChange = (e) => {
    //get name and value
    const { name, value } = e.target
    //same as this.setState  get name and value off input and match to                                                             this object 
    setInputs(prev/* same as prevState */ => ({ ...prev, [name]: value }))
  }

  const makePaypalPayement = async (e) => {
    e.preventDefault();

    const item = { inputs }
    try {
      let respose = await axios.post(`${process.env.REACT_APP_SERVER_NODE_URL}/recharges/paypal`, item, { headers: authHeader() })
      console.log(respose)
      window.location = "google.com";
    } catch (err) {
      console.log(err)
    };
  }

  const makeStripePayement = async (token) => {
    const item = { token, inputs }

    await axios.post(`${process.env.REACT_APP_SERVER_NODE_URL}/recharges/stripe`, item, { headers: authHeader() })
      .then((res) => {
        toast.info('Recharge éfféctuée avec succès', {
          position: "bottom-left",
          autoClose: 5000,
          hideProgressBar: true,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        history.push('/dashboard/');
      }).catch((err) => { console.log(err) });
  }

  return (
    <>
      <HelmetSite title={`New Recharge`} />
      <Breadcrumbs
        breadCrumbTitle="Recharge"
        breadCrumbParent="Pages"
        breadCrumbActive="Nouvelle recharge"
      />

      <Button.Ripple onClick={history.goBack} className="mr-1 mb-1 border-primary text-primary" color="flat-primary" size="sm">
        <ArrowLeft size={14} />
      </Button.Ripple>
      <Button.Ripple onClick={history.goForward} className="mr-1 mb-1 border-primary text-primary" color="flat-primary" size="sm">
        <ArrowRight size={14} />
      </Button.Ripple>

      <Row>
        <Col sm="12">
          <Card>
            <CardBody>

              <div className="col-md-8 mx-auto">

                <Form className="mt-2" onSubmit={makePaypalPayement} >

                  <Row>

                    <Col md="12" sm="12">
                      <FormGroup className="form-label-group">
                        <Input
                          className={`form-control`}
                          type="number" min="1" step="1" id="total" name="total"
                          value={inputs.total} onChange={handleChange}
                          placeholder="Amount recharge" required />
                        <Label for="total">Montant de la recharge</Label>
                      </FormGroup>
                    </Col>
                  </Row>

                  <Row>

                    <Col lg={6} md={12}>
                      <StripeCheckout
                        image="" // the pop-in header image (default none)
                        token={makeStripePayement}
                        currency={inputs.currency}
                        stripeKey={process.env.REACT_APP_STRIPE_PUBLIC_KEY}
                        amount={inputs.total * 100}
                        panelLabel="Recharge"
                      >
                        <button type="button" className="btn-lg btn-block btn btn-outline-primary">
                          <b>Pay with Card</b>
                        </button>
                      </StripeCheckout>
                    </Col>
                    <Col lg={6} md={12}>
                      <button type="submit" className="btn-lg btn-block btn btn-outline-primary">
                        <b>Pay with Paypal</b>
                      </button>
                    </Col>
                  </Row>
                </Form>
              </div>

            </CardBody>
          </Card>
        </Col>
      </Row>
    </>
  )
}

export default RechargeUserCreate;