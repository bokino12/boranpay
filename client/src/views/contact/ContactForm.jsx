import React, { useEffect } from 'react'
import {
  Card,
  CardBody,
  FormGroup,
  Row,
  Col,
  Form,
  Button,
  Label
} from 'reactstrap'
import { useDispatch, useSelector } from 'react-redux'
import { loadAllCountries } from '../../redux/actions/pages/countryAction'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import axios from "axios"
import { SuccessToast } from '@components/toastalert'
import { toast } from 'react-toastify'

const schema = yup.object().shape({
  name: yup.string().required().min(3).max(200),
  email: yup.string().email().required().min(3).max(200),
  countryId: yup.number().required(),
  subject: yup.string().required().min(3).max(200),
  content: yup.string().required().min(5).max(65000)
})

const ContactForm = () => {
  const { register, handleSubmit, formState } = useForm({
    resolver: yupResolver(schema)
  })
  const { errors, isSubmitting } = formState
  const countries = useSelector(state => state?.countries?.countries)
  const dispatch = useDispatch()

  useEffect(() => {
      const loadItems = async () => {
          await dispatch(loadAllCountries())
      }
      loadItems()
  }, [])

  const onSubmit = async (data, e) => {
    await axios.post(`${process.env.REACT_APP_SERVER_NODE_URL}/contacts`, data)
      .then((res) => {
        toast.success(
          <SuccessToast name={'Success'} description={'Message envoyé avec succès'} />, {
          position: toast.POSITION.TOP_RIGHT,
          hideProgressBar: true
        })
        e.target.reset()
      }).catch((err) => {
        console.log(err)
      })
  }

  return (
    <Card>
      <CardBody>
        <div className="col-md-10 mx-auto">
          <Form className="mt-2" onSubmit={handleSubmit(onSubmit)}>

            <Row>

              <Col md="6" sm="6">
                <FormGroup>
                  <Label for="name"><strong>Nom complet</strong></Label>
                  <input
                    className={`form-control ${errors.name ? 'is-invalid' : ''}`}
                    type="text" id="name" {...register('name')}
                    placeholder="Name" required/>
                  {/* errors will return when field validation fails  */}
                  <span className='invalid-feedback'>
                    <strong>{errors.name?.message}</strong>
                  </span>
                </FormGroup>
              </Col>

              <Col md="6" sm="6">
                <FormGroup>
                  <Label for="email"><strong>Email</strong></Label>
                  <input className={`form-control ${errors.email ? 'is-invalid' : ''}`}
                    type="email"
                    id="email"
                    {...register('email')}
                    placeholder="Email" required />
                  <span className='invalid-feedback'>
                    <strong>{errors.email?.message}</strong>
                  </span>
                </FormGroup>
              </Col>

              <Col md="6" sm="6">
                <FormGroup>
                  <Label for="subject"><strong>Object</strong></Label>
                  <input className={`form-control ${errors.subject ? 'is-invalid' : ''}`}
                    type="text"
                    id="subject"
                    {...register('subject')}
                    placeholder="Subject"
                    required
                  />
                  <span className='invalid-feedback'>
                    <strong>{errors.subject?.message}</strong>
                  </span>
                </FormGroup>
              </Col>

              <Col md="6" sm="6">
                <FormGroup>
                  <Label className='form-label' for='countryId'>
                    <strong>Pays</strong>
                  </Label>
                  <select className={`form-control ${errors.countryId ? 'is-invalid' : ''}`} 
                    {...register('countryId')} id="countryId" required="required">
                      <option value={''}>Choose country</option>
                    {countries.map((item, index) => <option key={index} value={item.id} >{item.name}</option>)}
                  </select>
                  <span className='invalid-feedback'>
                    <strong>{errors.countryId?.message}</strong>
                  </span>
                </FormGroup>
              </Col>

              <Col sm="12">
                <FormGroup>
                  <Label for="content"><strong>Contenu</strong></Label>
                  <textarea className={`form-control ${errors.content ? 'is-invalid' : ''}`}
                    type="text"
                    rows="4"
                    minLength="5"
                    maxLength="65000"
                    id="content"
                    {...register('content')}
                    placeholder="Contant message ..."
                    required
                  />
                  <span className='invalid-feedback'>
                    <strong>{errors.content?.message}</strong>
                  </span>
                </FormGroup>
              </Col>

              <div className="col-md-12 text-center">
                <FormGroup className="form-label-group">
                  <Button.Ripple
                    disabled={isSubmitting}
                    color="primary"
                    type="submit"
                    className="mr-1 mb-1"
                  >
                    Envoyer
                </Button.Ripple>
                </FormGroup>
              </div>

            </Row>
          </Form>
        </div>
      </CardBody>
    </Card>
  )
}
export default ContactForm
