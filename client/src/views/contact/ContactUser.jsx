import React from 'react'
import Breadcrumbs from '@components/breadcrumbs'
import HelmetSite from '@components/helmet/HelmetSite'
import UndoRedo from '@components/undoredo'
import { Row, Col } from 'reactstrap'
import { Link } from 'react-router-dom'
import ContactForm from './ContactForm'

const ContactUser = () => {
    return (
        <>
            <HelmetSite title={`Contact`} />
            <Breadcrumbs
                breadCrumbTitle="Contact"
                breadCrumbParent="Support"
                breadCrumbActive={`Contact`}
            />
            {/** Undo redo */}
            <UndoRedo />
            <div className="text-center" >
                <h1>Contact</h1>
                <p>Laisser nous un message ou visiter notre <Link to="/faq/">FAQS</Link> pour votre voute</p>
            </div>
            <ContactForm />
        </>
    )
}

export default ContactUser