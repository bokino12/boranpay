import React, { useState, useEffect, Fragment } from 'react'
import Breadcrumbs from '@components/breadcrumbs'
import HelmetSite from '@components/helmet/HelmetSite'
import UndoRedo from '@components/undoredo'
import VerificationInfoUser from '../profile/inc/VerificationInfoUser'
import { authuserInfo } from '@components/service'
import { Button, Row, Col, Card, CardBody } from 'reactstrap'
import { Plus } from 'react-feather'
import DonationStatistiqueTabe from './inc/DonationStatistiqueTabe'
import { useDispatch, useSelector } from 'react-redux'
import { ShowDonationUser } from '../../redux/actions/pages/donationAction'
import { useParams, useHistory } from 'react-router-dom'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import { SuccessToast } from '@components/toastalert'
import { toast } from 'react-toastify'

const DonationStatistiqueUser = () => {
    const history = useHistory()
    const { slugin } = useParams()
    const [userSite] = useState(authuserInfo())
    const itemDonation = useSelector(state => state.donations.donation)
    const dispatch = useDispatch()

    useEffect(() => {
        const loadItems = async () => {
            await dispatch(ShowDonationUser(slugin, userSite.slugin))
        }
        loadItems()
    }, [slugin, userSite.slugin])

    const onCopy = () => {
        toast.success(
            <SuccessToast name={'Success'} description={'Lien copié avec succès partager a present'} />, {
            position: toast.POSITION.TOP_RIGHT,
            hideProgressBar: true
        })
    }
    const currencyUser = itemDonation?.user?.profile?.currency?.symbol
    return (
        <>
            <HelmetSite title={`${itemDonation.title || process.env.REACT_APP_NAME}`} />
            <Breadcrumbs
                breadCrumbTitle="Donations"
                breadCrumbParent="Discover"
                breadCrumbActive={`Donations`}
            />
            <VerificationInfoUser userItem={userSite} />

            <Row className="match-height">
                <Col lg="12" md="12" sm="12">
                    <Card className="bg-analytics">
                        <CardBody className="text-center">
                            <h4 className="text-dark"><b>Montant reçu sur ce don</b></h4>
                            <br />
                            <div className="text-center">
                                <h1 className='font-weight-bolder mb-0'>
                                    {itemDonation?.amountdonations?.length > 0 ? <>
                                        <p className="text-dark" >{itemDonation?.amountdonations.map((lk, index) => (<Fragment key={index} >{(lk.totalAmountdonation * itemDonation?.user?.profile?.currency?.currencyNumber).formatMoney(2, '.', ',')} </Fragment>))}
                                            {currencyUser}</p></> : <>0,00 {currencyUser}</>}
                                </h1>
                            </div>
                            <div className="text-center my-sm-75">
                                <CopyToClipboard
                                    onCopy={onCopy}
                                    text={`${process.env.REACT_APP_LINK}/donation/${itemDonation?.slugin}/contribution/`}
                                >
                                    <Button
                                        className="mr-1 mb-1"
                                        color="primary"
                                        size="sm">
                                        copier le lien
                                </Button>
                                </CopyToClipboard>
                            </div>
                        </CardBody>
                    </Card>
                </Col>
            </Row>

            {/** Undo redo */}
            <UndoRedo />
            <Button.Ripple onClick={() => history.push(`/donation/new/`)} className="mr-1 mb-1" color="primary" size="sm">
                <Plus size={14} /> <b>Nouvelle campagne donation</b>
            </Button.Ripple>
            <DonationStatistiqueTabe donation={itemDonation} />
        </>
    )
}

export default DonationStatistiqueUser