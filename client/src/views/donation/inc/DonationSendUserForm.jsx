import React, { useState, useEffect } from 'react'
import {
  Card,
  CardBody,
  Alert,
  FormGroup,
  ListGroup,
  Row,
  Col,
  Form,
  Button,
  Media,
  Label
} from 'reactstrap'
import userServices from '../../../services/userServices'
import { AlertCircle, Edit, Copy } from 'react-feather'
import { Link, useHistory } from "react-router-dom"
import { useDispatch, useSelector } from "react-redux"
import { loadAllCurrencies } from "../../../redux/actions/pages/currencyAction"
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import ContactUserForm from "../../contact/ContactUserForm"
import Swal from 'sweetalert2'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import * as yup from 'yup'
import axios from 'axios'
import moment from 'moment'
import { authHeader } from '@components/service'
import { SuccessToast, ErrorToast } from '@components/toastalert'
import { toast } from 'react-toastify'
import SelectedContributionMethods from './contributionmethods/SelectedContributionMethods'

const schema = yup.object().shape({
  password: yup.string().required().min(8).max(200),
  total: yup.number().required().positive().integer(),
  currency: yup.string().required().min(1).max(5)
})

const DonationSendUserForm = ({ donation, userSite }) => {
  const history = useHistory()
  const [statusSend, setStatusSend] = useState(false)
  const [message, setMessage] = useState('')
  const [statusPayement, setStatusPayement] = useState(false)
  const {
    register,
    handleSubmit,
    formState
  } = useForm({ resolver: yupResolver(schema) })
  const { errors, isSubmitting } = formState
  /** Get currencies with redux */
  const currencies = useSelector(state => state?.currencies?.currencies)
  const dispatch = useDispatch()
  useEffect(() => {
    const loaddonations = async () => {
      await dispatch(loadAllCurrencies())
    }
    loaddonations()
  }, [])

  const toggleStatus = () => { setStatusPayement(!statusPayement) }
  const toggleStatusSend = () => { setStatusSend(!statusSend) }

  const onCopy = () => {
    toast.success(
      <SuccessToast name={'Success'} description={'Lien copié avec succès'} />, {
      position: toast.POSITION.TOP_CENTER,
      hideProgressBar: true
    })
  }

  const onSubmit = async (data, e) => {
    Swal.fire({
      title: 'Confirmez-vous la transaction ?',
      text: "Êtes-vous sûr de vouloir executer cette action?",
      confirmButtonText: 'Oui, Confirmer',
      cancelButtonText: 'Non, annuler',
      buttonsStyling: false,
      confirmButtonClass: "btn btn-primary",
      cancelButtonClass: 'btn btn-outline-default',
      showCancelButton: true,
      reverseButtons: false,
      showLoaderOnConfirm: true,
      preConfirm: () => {
        return userServices.userCheckValidate(userSite, data)
          .then(async () => {

            try {
              const response = await axios.post(`${process.env.REACT_APP_SERVER_NODE_URL}/donation/${donation.slugin}/contribute`, data, { headers: authHeader() })
              if (response) {
                toast.success(
                  <SuccessToast name={'Success'} description={'Don envoyé avec succès'} />, {
                  position: toast.POSITION.TOP_RIGHT,
                  hideProgressBar: true
                })
                history.push('/dashboard/')
              }
            } catch (error) {
              toast.error(
                <ErrorToast name={'Opp error'} description={'Quelque chose ne va pas ...'} />, {
                position: toast.POSITION.TOP_RIGHT,
                hideProgressBar: true
              })
              setMessage(error.response.data.message)
            }

          }).catch(error => {
            Swal.showValidationMessage(`${error.response.data.message}`)
          })
      },
      allowOutsideClick: () => !Swal.isLoading()
    })
  }

  const getDescription = (donation) => {
    return { __html: (donation.content) }
  }

  return (
    <>
      <Row>

        <Col lg="8" md="7" sm="12">
          <Card>
            <CardBody>
              <div className="d-flex justify-content-start align-donations-center mb-1">

                <div className="avatar mr-1">
                  <img
                    src="https://ivemo-cm.s3.eu-central-1.amazonaws.com/img/locations/de2a5dc5106a1d9d1a250251f78436c16c7923b7.png"
                    alt="avtar img holder"
                    height="45"
                    width="45"
                  />
                </div>
                <div className="user-page-info">
                  <p className="mb-0"><b>{donation.user.firstName} {donation.user.lastName}</b></p>
                  <span className="font-small-2">{moment(donation.createdAt).format('ll')}</span>
                </div>
                {!donation.isDelete && (
                  <div className="ml-auto user-like">
                    {(donation.userId === userSite.userId) && (
                      <>
                        <Button.Ripple onClick={() => history.push(`/donation/${donation.slugin}/edit/`)} className="mr-1 mb-1 border-primary text-primary" color="flat-primary" size="sm">
                          <b>Editer</b>
                        </Button.Ripple>
                        <Button.Ripple onClick={() => history.push(`/donation/${donation.slugin}/statistique/`)} className="mr-1 mb-1 border-primary text-info" color="flat-primary" size="sm">
                          <b>Statistique</b>
                        </Button.Ripple>
                      </>
                    )}

                    {donation.slugin && (
                      <CopyToClipboard
                        onCopy={onCopy}
                        text={`${process.env.REACT_APP_LINK}/donation/${donation.slugin}/contribution/`}
                      >
                        <Button.Ripple className="mr-1 mb-1" color="primary" size="sm">
                          <Copy size={17} className='cursor-pointer' /> <b>Copier le lien</b>
                        </Button.Ripple>
                      </CopyToClipboard>

                    )}
                  </div>
                )}

              </div>

              <br />
              <h4>{donation.title}</h4>
              <br />
              <span className="title text-justify" dangerouslySetInnerHTML={getDescription(donation)} />
              <br />

              {!donation.isDelete && (
                <ListGroup flush>

                  {!statusPayement && (
                    <Form className="mt-2" onSubmit={handleSubmit(onSubmit)}>

                      {message && (
                        <Alert color='danger'>
                          <div className='alert-body text-center'>
                            <AlertCircle size={15} />{' '}
                            <span className='ml-1'>
                              {message}
                            </span>
                          </div>
                        </Alert>
                      )}

                      <br />
                      <Row>
                        <Col md="8" sm="8">
                          <FormGroup>
                            <Label for="total"><strong>Montant de votre don</strong></Label>
                            <input className={`form-control ${errors.total ? 'is-invalid' : ''}`}
                              type="number"
                              pattern="[0-9]*"
                              inputMode="numeric"
                              min="1" step="1"
                              id="total"
                              placeholder="Montant de la donation"
                              {...register('total')}
                              autoComplete="off"
                              required
                            />
                            <span className='invalid-feedback'>
                              <strong>{errors.total?.message}</strong>
                            </span>
                          </FormGroup>
                        </Col>

                        <Col md="4" sm="4">
                          <FormGroup>
                            <Label for="currency"><strong>Devise</strong></Label>
                            <select className={`form-control ${errors.currency ? 'is-invalid' : ''}`} {...register('currency')} id="currency" required>
                              <option value=''>Selectioner une devise</option>
                              {currencies.map((donation, index) => (
                                <option key={index} value={donation.code} >{donation.code}</option>)
                              )}
                            </select>
                            <span className='invalid-feedback'>
                              <strong>{errors.currency?.message}</strong>
                            </span>
                          </FormGroup>
                        </Col>

                        <Col sm="12">
                          <FormGroup>
                            <Label for="content"><strong>Message</strong> (facultatif)</Label>
                            <textarea className={`form-control ${errors.content ? 'is-invalid' : ''}`}
                              type="text"
                              rows="2"
                              id="content"
                              {...register('content')}
                              placeholder={`Laisser message à ${donation.user.firstName} ...`}
                            />
                            <span className='invalid-feedback'>
                              <strong>{errors.content?.message}</strong>
                            </span>
                          </FormGroup>
                        </Col>
                        {statusSend && (
                          <Col sm={12}>
                            <FormGroup>
                              <Label for="password">Veuillez saisir votre mot de passe</Label>
                              <input className={`form-control ${errors.password ? 'is-invalid' : ''}`}
                                type="password"
                                id="password"
                                {...register('password')}
                                autoComplete="off"
                                placeholder="Mot de passe pour valider la transaction" required />

                              <span className='invalid-feedback'>
                                <strong>{errors.password?.message}</strong>
                              </span>
                            </FormGroup>
                          </Col>
                        )}
                        <div className="col-md-12 text-center">
                          <FormGroup className="form-label-group">
                            {statusSend ? (
                              <Button.Ripple
                                disabled={isSubmitting}
                                type="submit"
                                className={`${isSubmitting ? "btn-loading" : ""
                                  } btn-load`}
                                color="primary"
                                block
                              >
                                Faire le don avec {process.env.REACT_APP_NAME}
                              </Button.Ripple>
                            ) : (
                              <Button.Ripple
                                className="btn-block"
                                color="primary"
                                onClick={toggleStatusSend}
                                block
                              >
                                Continuer
                              </Button.Ripple>
                            )}
                          </FormGroup>
                        </div>

                        <div className="col-md-12 text-center">
                          <FormGroup className="form-label-group">
                            <Button.Ripple
                              className="btn-block"
                              color="dark"
                              onClick={toggleStatus}
                              outline >
                              Sélectioner un autre moyen
                            </Button.Ripple>
                          </FormGroup>
                        </div>
                        <div className="col-md-12 text-center">
                          Ce don sera envoyé à <strong>{donation.user.firstName}.</strong> <br /> Pour plus d'informations, voir les questions fréquement posées et vérifier les termes et conditions d'utilisations.
                        </div>
                      </Row>
                    </Form>

                  )}

                  {statusPayement && (
                    <>
                      <Row>
                        <SelectedContributionMethods donation={donation} />
                        <div className="col-lg-12 text-center">
                          <Button.Ripple color="link" onClick={toggleStatus} ><b>Contribuer avec {process.env.REACT_APP_NAME}</b></Button.Ripple>
                        </div>
                      </Row>
                    </>
                  )}
                  <br />
                  <div className="col-md-12 text-center">
                    <Link to="/dashboard/" ><b>Annuler</b></Link>
                  </div>


                </ListGroup>
              )}

            </CardBody>
          </Card>
        </Col>
        <Col lg="4" md="5" sm="12">
          <Card>
            <CardBody className="knowledge-base-category">
              <ListGroup flush>

                <ContactUserForm dataItem={donation} />


              </ListGroup>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </>
  )
}
export default DonationSendUserForm
