import React, { useState, useEffect } from 'react'
import {
  Card,
  CardBody,
  Alert,
  FormGroup,
  Row,
  Col,
  Form,
  Button,
  Media,
  Label
} from 'reactstrap'
import userServices from '../../../services/userServices'
import { AlertCircle } from 'react-feather'
import { Link, useHistory } from 'react-router-dom'
import { authHeader } from '@components/service'
import { useDispatch, useSelector } from 'react-redux'
import { loadAllCurrencies } from '../../../redux/actions/pages/currencyAction'
import { useForm } from "react-hook-form"
import { yupResolver } from '@hookform/resolvers/yup'
import Swal from 'sweetalert2'
import * as yup from 'yup'
import axios from "axios"
import { SuccessToast, ErrorToast } from '@components/toastalert'
import { toast } from 'react-toastify'


const schema = yup.object().shape({
  password: yup.string().required().min(8).max(200),
  total: yup.number().required().positive().integer(),
  currency: yup.string().required().min(1).max(5)
})

const DonationUserForm = ({ userData }) => {
  const history = useHistory()
  const [statusSend, setStatusSend] = useState(false)
  const [message, setMessage] = useState('')
  const {
    register,
    handleSubmit,
    formState
  } = useForm({ resolver: yupResolver(schema) })
  const { errors, isSubmitting } = formState
  /** Get currencies with redux */
  const currencies = useSelector(state => state.currencies.currencies)
  const dispatch = useDispatch()
  useEffect(() => {
    const loadItems = async () => {
      await dispatch(loadAllCurrencies())
    }
    loadItems()
  }, [])

  const toggleStatus = () => { setStatusSend(!statusSend) }

  const onSubmit = async (data, e) => {
    Swal.fire({
      title: 'Confirmez-vous la transaction ?',
      text: "Êtes-vous sûr de vouloir executer cette action?",
      confirmButtonText: 'Oui, Confirmer',
      cancelButtonText: 'Non, annuler',
      buttonsStyling: false,
      confirmButtonClass: "btn btn-primary",
      cancelButtonClass: 'btn btn-outline-default',
      showCancelButton: true,
      reverseButtons: false,
      showLoaderOnConfirm: true,
      preConfirm: () => {
        return userServices.userCheckValidate(userSite, data)
          .then(async () => {

            try {

              const response = await axios.post(`${process.env.REACT_APP_SERVER_NODE_URL}/donations/${userData.slugin}/new`, data, { headers: authHeader() })
              if (response) {
                toast.success(
                  <SuccessToast name={'Success'} description={'Don éffectué avec succès'} />, {
                  position: toast.POSITION.TOP_RIGHT,
                  hideProgressBar: true
                })
                history.push('/dashboard/')
              }
            } catch (error) {
              toast.error(
                <ErrorToast name={'Danger'} description={'Oops! Quelque chose ne va pas.'} />, {
                position: toast.POSITION.TOP_RIGHT,
                hideProgressBar: true
              })
              setMessage(error.response.data.message)
            }

          }).catch(error => {
            Swal.showValidationMessage(`${error.response.data.message}`)
          })
      },
      allowOutsideClick: () => !Swal.isLoading()
    })
  }

  return (
    <Card>
      <CardBody>
        <div className="col-md-8 mx-auto">
          <Form className="mt-2" onSubmit={handleSubmit(onSubmit)}>

            {message && (
              <Alert color='danger'>
                <div className='alert-body text-center'>
                  <AlertCircle size={15} />{' '}
                  <span className='ml-1'>
                    {message}
                  </span>
                </div>
              </Alert>
            )}


            <div className="col-md-12 text-center">
              <Media
                className="rounded-circle"
                object
                src="https://ivemo-cm.s3.eu-central-1.amazonaws.com/img/locations/de2a5dc5106a1d9d1a250251f78436c16c7923b7.png"
                height="74"
                width="74"
                alt={userData.firstName}
              />
              <h2><strong>{userData.firstName} {userData.lastName}</strong></h2>

            </div>
            <br />
            <Row>

              <Col md="8" sm="8">
                <FormGroup>
                  <Label for="total">Montant de votre don</Label>
                  <input className={`form-control ${errors.total ? 'is-invalid' : ''}`}
                    type="number"
                    pattern="[0-9]*"
                    inputMode="numeric"
                    min="1" step="1"
                    id="total"
                    placeholder="Montant de la donation"
                    {...register('total')}
                    autoComplete="off"
                    required
                  />
                  <span className='invalid-feedback'>
                    <strong>{errors.total?.message}</strong>
                  </span>
                </FormGroup>
              </Col>

              <Col md={4} sm={4}>
                <FormGroup>
                  <Label for="currency">Devise</Label>
                  <select className={`form-control ${errors.currency ? 'is-invalid' : ''}`} {...register('currency')} id="currency" required>
                    <option value=''>Selectioner une devise</option>
                    {currencies.map((item, index) => (
                      <option key={index} value={item.code} >{item.code}</option>
                    ))}
                  </select>
                  <span className='invalid-feedback'>
                    <strong>{errors.currency?.message}</strong>
                  </span>
                </FormGroup>
              </Col>

              <Col sm="12">
                <FormGroup>
                  <Label for="content">Message</Label>
                  <textarea className={`form-control ${errors.content ? 'is-invalid' : ''}`}
                    type="text"
                    rows="2"
                    id="content"
                    {...register('content')}
                    placeholder={`Laisser un message à ${userData.firstName} ...`}
                  />
                  <span className='invalid-feedback'>
                    <strong>{errors.content?.message}</strong>
                  </span>
                </FormGroup>
              </Col>
              {statusSend && (
                <Col sm={12}>
                  <FormGroup>
                    <Label for="password">Veuillez saisir votre mot de passe</Label>
                    <input className={`form-control ${errors.password ? 'is-invalid' : ''}`}
                      type="password"
                      id="password"
                      {...register('password')}
                      autoComplete="off"
                      placeholder="Mot de passe pour valider la transaction" required />

                    <span className='invalid-feedback'>
                      <strong>{errors.password?.message}</strong>
                    </span>
                  </FormGroup>
                </Col>
              )}
              <div className="col-md-12 text-center">
                <FormGroup className="form-label-group">
                  {statusSend ? (
                    <Button.Ripple
                      disabled={isSubmitting}
                      type="submit"
                      block
                      className={`${isSubmitting ? "btn-loading" : ""
                        } btn-load`}
                      color="primary"
                    >
                      Faire le don avec {process.env.REACT_APP_NAME}
                    </Button.Ripple>
                  ) : (
                      <Button.Ripple
                        className="btn-block"
                        color="primary"
                        onClick={toggleStatus}
                        block
                      >
                        Continuer
                      </Button.Ripple>
                    )}
                </FormGroup>
              </div>
              <br />
              <div className="col-md-12 text-center">
                <Link to="/dashboard/" ><b>Annuler</b></Link>
              </div>
              <div className="col-md-12 text-center">
                Ce don sera envoyé <strong>{userData.firstName}.</strong> <br /> Pour plus d'informations, Pour plus d'informations,
                voir les questions fréquement posées et vérifier les termes et conditions d'utilisations.
              </div>

            </Row>
          </Form>
        </div>
      </CardBody>
    </Card>
  )
}
export default DonationUserForm
