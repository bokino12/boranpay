import React, { useState } from 'react'
import Breadcrumbs from '@components/breadcrumbs'
import HelmetSite from '@components/helmet/HelmetSite'
import VerificationInfoUser from '../profile/inc/VerificationInfoUser'
import UndoRedo from '@components/undoredo'
import { authuserInfo } from '@components/service'
import { Button } from 'reactstrap'
import DonationTable from './inc/DonationTable'
import DonationCard from './inc/DonationCard'
import { useHistory } from 'react-router-dom'
import { Plus } from 'react-feather'

const DonationUser = () => {
    const history = useHistory()
    const [userSite] = useState(authuserInfo())

    return (
        <>
            <HelmetSite title={`Donations - ${userSite.firstName}`} />
            <Breadcrumbs
                breadCrumbTitle="Donations"
                breadCrumbParent="Discover"
                breadCrumbActive={`Donations ${userSite.firstName}`}
            />

            <VerificationInfoUser userItem={userSite} />
            <DonationCard userData={userSite} />
            {/** Undo redo */}
            <UndoRedo />
            <Button.Ripple onClick={() => history.push(`/donation/new/`)} className="mr-1 mb-1" color="primary" size="sm">
                <Plus size={14} /> <b>Nouvelle campagne donation</b>
            </Button.Ripple>
            <DonationTable {...userSite} />
        </>
    )
}

export default DonationUser