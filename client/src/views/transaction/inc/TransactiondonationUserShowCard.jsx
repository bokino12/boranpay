// ** Third Party Components
import { Card, CardBody, CardText, Row, Col } from 'reactstrap'
import Skeleton from 'react-loading-skeleton'
import moment from 'moment'
import Avatar from '@components/avatar'
const TransactiondonationUserShowCard = ({ transaction }) => {
  const AmountTotal = transaction.amount.total
  return transaction !== null ? (
    <Card className='invoice-preview-card'>
      <CardBody className='invoice-padding pb-0'>
        {/* Header */}
        <div className='d-flex justify-content-between flex-md-row flex-column invoice-spacing mt-0'>
          <div>
            <div className='logo-wrapper'>
              {/** <Avatar className='mr-50' img={'https://ivemo-cm.s3.eu-central-1.amazonaws.com/img/locations/de2a5dc5106a1d9d1a250251f78436c16c7923b7.png'} width='60' height='60' /> */}
              <h4 className='text-primary invoice-logo'>{transaction.userto.firstName || <Skeleton width={100} />} {transaction.userto.lastName}</h4>
            </div>
          </div>
          <div className='mt-md-0 mt-2'>
            <div className='invoice-title'>
              {transaction.invoiceNumber ? (
                <b><span className={`invoice-number ${transaction.statusSend ? "text-success" : "text-danger"}`}>{transaction.statusSend ? <>+ {AmountTotal.formatMoney(2, '.', ',')}</> : <>- {AmountTotal.formatMoney(2, '.', ',')}</>} {transaction.amount.currency}</span></b>
              ) : (<Skeleton width={100} />)}
            </div>
            <div className='invoice-date-wrapper'>
              {transaction.invoiceNumber ? (
                <small>{moment(transaction.createdAt).format('ll')}</small>
              ) : (<Skeleton width={50} />)}
            </div>
          </div>
        </div>
        {/* /Header */}
      </CardBody>

      <hr className='invoice-spacing' />

      <CardBody className='invoice-padding pb-0'>
        {/* Header */}
        <div className='d-flex justify-content-between flex-md-row flex-column invoice-spacing mt-0'>
          {transaction.userto ? (
            <div>
              <div className='logo-wrapper'>
                <h5 className='invoice-logo'>{transaction.user.firstName} {transaction.user.lastName}</h5>
              </div>
              <CardText className='mb-25'>{transaction.user.phone} </CardText>
              <CardText className='mb-25'><a href={`${transaction.user.profile.siteInternet}`} target="_blanck">{transaction.user.profile.siteInternet}</a></CardText>
              <CardText className='mb-0'>{transaction.user.profile.country.name} {transaction.user.profile.addresse}</CardText>
            </div>
          ) : (<Skeleton count={3} width={150} />)}

          <div className='mt-md-0 mt-1'>
            <h5 className='invoice-title'>
              <span className='invoice-number'>Detail</span>
            </h5>
            <div className='invoice-date-wrapper'>
              {transaction.userto && (
                <CardText className='mb-25'>{transaction.userto.firstName} {transaction.userto.lastName}</CardText>
              )}
              <CardText className='mb-25'><a href={`mailto:${transaction.userto.email}`}>{transaction.userto.email}</a></CardText>
              {transaction.invoiceNumber && (
                <CardText className='mb-25'><b>Code transaction</b> {transaction.invoiceNumber}</CardText>
              )}
              {transaction.userto.profile.country && (
                <CardText className='mb-25'>{transaction.userto.profile.country.name} {transaction.userto.profile.addresse} </CardText>
              )}
              {transaction.userto.profile.siteInternet && (
                <CardText className='mb-25'><a href={`${transaction.userto.profile.siteInternet}`}>{transaction.userto.profile.siteInternet}</a></CardText>
              )}
              <hr className='invoice-spacing' />
              {transaction.amount.total ? (
                <>
                  {transaction.amount.taxeTransaction !== null && (
                    <>
                      <CardText className='mb-25'>Montant: {(AmountTotal + transaction.amount.taxeTransaction).formatMoney(2, '.', ',')} <small>{transaction.amount.currency}</small> </CardText>
                      <CardText className='mb-25'>Taxe: {(transaction.amount.taxeTransaction).formatMoney(2, '.', ',')} <small>{transaction.amount.currency}</small></CardText>
                      <hr className='invoice-spacing' />
                    </>
                  )}
                  <CardText className='mb-25 mr-25'>Total:  <strong>{AmountTotal.formatMoney(2, '.', ',')} {transaction.amount.currency}</strong></CardText>
                </>

              ) : (<Skeleton width={100} />)}

            </div>
          </div>
        </div>
        {/* /Header */}
      </CardBody>

      {/* /Invoice Description */}
      <hr className='invoice-spacing' />

      {/* Invoice Note */}
      <CardBody className='invoice-padding pt-0'>
        <Row>
          <Col sm='12'>
            <span className='font-weight-bold'>Note: </span>
            <span>
              Ce résumé, à pour but de servir de trançabilité et preuve pendant un potentiel contentieux
            </span>
          </Col>
        </Row>
      </CardBody>
      {/* /Invoice Note */}
    </Card>
  ) : null
}

export default TransactiondonationUserShowCard
