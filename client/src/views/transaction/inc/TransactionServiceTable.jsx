import { CardBody } from 'reactstrap'
import { Link } from 'react-router-dom'
import DataTable from 'react-data-table-component'
import moment from 'moment'
import Avatar from '@components/avatar'

const renderClient = item => {
    const stateNum = Math.floor(Math.random() * 6),
        states = ['light-success', 'light-danger', 'light-warning', 'light-info', 'light-primary', 'light-secondary'],
        color = states[stateNum]

    if (item?.user?.avatar) {
        return <Avatar className='mr-50' img={item?.user?.avatar || 'https://ivemo-cm.s3.eu-central-1.amazonaws.com/img/locations/de2a5dc5106a1d9d1a250251f78436c16c7923b7.png'} width='32' height='32' />
    } else {
        return <Avatar color={color} className='mr-50' content={(item?.user && item?.user?.firstName) || 'Boclair Temgoua'} initials />
    }
}
const TransactionServiceTable = ({ items }) => {

    const columns = [
        {
            name: <h5>Profile</h5>,
            selector: "profile_user",
            sortable: true,
            grow: 4
        },
        {
            name: <h5>Montant</h5>,
            selector: "price_transaction",
            sortable: true,
            grow: 2
        },
        {
            name: <h5>Date</h5>,
            selector: "date",
            sortable: true,
            grow: 2
        },
        {
            name: <h5>Motif | Identifiant</h5>,
            selector: "title_transaction",
            sortable: true,
            grow: 3
        }

    ]

    const data = items?.length >= 0 ? (
        items.map(item => (
            {
                profile_user: (
                    <>
                        <div className='d-flex justify-content-left align-items-center'>
                            {renderClient(item)}
                            <div className='d-flex flex-column'>
                                <h6 className='user-name text-truncate mb-0'>{item?.userto?.firstName} {item?.userto?.lastName}</h6>
                                <small className='text-truncate text-muted mb-0'>{item?.userto?.email}</small>
                            </div>
                        </div>
                    </>
                ),
                price_transaction: <Link to={`/transactions_service/${item.slugin}/`} className="text-dark"><h4 className={`${item.statusSend ? "text-success" : "text-danger"}`}>{item.statusSend ? <>+ {(item?.amount?.totalNoTaxe).formatMoney(2, '.', ',')}</> : <>- {(item?.amount?.totalNoTaxe).formatMoney(2, '.', ',')}</>} <small>{item.amount.currency}</small></h4></Link>,
                date: <Link to={`/transactions_service/${item.slugin}/`} className="text-dark"><b className="text-dark">{moment(item.createdAt).format('ll')}</b></Link>,
                title_transaction: <Link to={`/transactions_service/${item.slugin}/`} className="text-dark"><b className="text-dark">{item.title}</b></Link>
            }
        )
        )
    ) : (
            <></>
        )

    return (
        <CardBody>
            <br />
            <DataTable
                data={data}
                columns={columns}
                defaultSortField="createdAt"
                className='react-dataTable'
                defaultSortAsc={false}
                overflowY={true}
                pagination={true}
                noHeader />
        </CardBody>
    )
}

export default TransactionServiceTable

