import { Card, CardBody, CardHeader } from 'reactstrap'
import DataTable from 'react-data-table-component'
import { Link } from 'react-router-dom'
import moment from 'moment'

const TransactionDonationTable = ({ items }) => {

    const columns = [
        {
            name: <h5>Date</h5>,
            selector: "date",
            sortable: true
        },
        {
            name: <h5>Montant</h5>,
            selector: "price_transaction",
            sortable: true,
            grow: 2
        },
        {
            name: <h5>Motif | Identifiant</h5>,
            selector: "title_transaction",
            sortable: true,
            grow: 2
        }
    ]

    const data = items?.length >= 0 ? (
        items.map(item => (
            {
                date: <Link to={`/transactions_donation/${item.slugin}/`} className="text-dark"><b className="text-dark">{moment(item.createdAt).format('ll')}</b></Link>,
                price_transaction: <Link to={`/transactions_donation/${item.slugin}/`} className="text-dark"><h4 className={`${item.statusSend ? "text-success" : "text-danger"}`}>{item.statusSend ? <>+ {(item.amount.total).formatMoney(2, '.', ',')}</> : <>- {(item.amount.total).formatMoney(2, '.', ',')}</>} <small>{item.amount.currency}</small></h4></Link>,
                title_transaction: <Link to={`/transactions_donation/${item.slugin}/`} className="text-dark"><b className="text-dark">{item.title} | {item.userto.firstName}</b></Link>
            }
        )
        )
    ) : (
            <></>
        )

    return (
        <Card>

            <CardHeader className='flex-md-row flex-column align-md-items-center align-items-start border-bottom'>
                <div className="tasks-info">
                    <h4 className="mb-75">
                        <strong>Transactions Donations </strong>
                    </h4>
                </div>
            </CardHeader>

            <CardBody>
                <br />
                <DataTable
                    data={data}
                    columns={columns}
                    defaultSortField="createdAt"
                    className='react-dataTable'
                    defaultSortAsc={false}
                    overflowY={true}
                    pagination={true}
                    noHeader />
            </CardBody>
        </Card>
    )
}

export default TransactionDonationTable

