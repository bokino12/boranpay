import React, { useEffect, useState } from 'react'
import HelmetSite from '@components/helmet/HelmetSite'
import VerificationInfoUser from '../profile/inc/VerificationInfoUser'
import UndoRedo from '@components/undoredo'
import Breadcrumbs from '@components/breadcrumbs'
import TransfertToUserForm from "./inc/TransfertToUserForm"
import { authuserInfo } from '@components/service'
import { useParams } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { loadDataProfileUserPublic } from "../../redux/actions/auth/profileActions"

const TransfertToUserCreate = () => {
    const { user } = useParams()
    const [userSite] = useState(authuserInfo())
    const userData = useSelector(state => state?.profile?.user)
    const dispatch = useDispatch()

    useEffect(() => {
        const loadItems = async () => {
            await dispatch(loadDataProfileUserPublic(user))
        }
        loadItems()
    }, [user])

    return (
        <>
            <HelmetSite title={`Nouveau Transfert pour ${userData.email}`} />
            <Breadcrumbs
                breadCrumbTitle="Transfert"
                breadCrumbParent="Function"
                breadCrumbActive={`Nouveau Transfert pour ${userData.email || process.env.REACT_APP_NAME}`}
            />
            <VerificationInfoUser userItem={userSite} />

            <UndoRedo />
            <TransfertToUserForm
                userData={userData}
                userSite={userSite}
            />
        </>
    )
}

export default TransfertToUserCreate