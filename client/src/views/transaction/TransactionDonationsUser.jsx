import React, { useEffect, useState } from 'react'
import Breadcrumbs from '@components/breadcrumbs'
import HelmetSite from '@components/helmet/HelmetSite'
import VerificationInfoUser from '../profile/inc/VerificationInfoUser'
import UndoRedo from '@components/undoredo'
import { authuserInfo } from '@components/service'
import { Plus } from 'react-feather'
import { Row, Col, Button } from 'reactstrap'
import { useDispatch, useSelector } from 'react-redux'
import { loadAllTransactionDonationsUser } from '../../redux/actions/pages/transactionAction'
import TransactionDonationsTable from './inc/TransactionDonationsTable'
import SalesCard from '../dashboard/analytics/SalesCard'
import SoldsDonationCard from '../dashboard/analytics/SoldsDonationCard'
import { useHistory } from 'react-router-dom'

const TransactionUser = () => {
    const history = useHistory()
    const [userSite] = useState(authuserInfo())
    const items = useSelector(state => state?.transactions?.transactiondonations)
    const dispatch = useDispatch()


    useEffect(() => {
        const loadItems = async () => {
            await dispatch(loadAllTransactionDonationsUser(userSite.slugin))
        }
        loadItems()
    }, [userSite.slugin])

    return (
        <>
            <HelmetSite title={`Transactions donations - ${userSite.firstName}`} />
            <Breadcrumbs
                breadCrumbTitle="Transactions"
                breadCrumbParent="Funtion"
                breadCrumbActive={`Transactions donations ${userSite.firstName}`}
            />
            <VerificationInfoUser userItem={userSite} />
            <Row className="match-height">
                <Col lg="7" md="6" sm="6">
                    <SoldsDonationCard {...userSite} />
                </Col>

                <Col lg="5" md="6" sm="6">
                    <SalesCard {...userSite} />
                </Col>
            </Row>
            {/** Undo redo */}
            <UndoRedo />
            <Button.Ripple onClick={() => history.push(`/donation/new/`)} className="mr-1 mb-1" color="primary" size="sm">
            <Plus size={14} /> <b>Nouvelle campage de donation</b>  
            </Button.Ripple>
            <TransactionDonationsTable items={items} />
        </>
    )
}

export default TransactionUser