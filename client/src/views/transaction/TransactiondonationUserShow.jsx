import React, { useEffect } from "react"
import Breadcrumbs from '@components/breadcrumbs'
import HelmetSite from '@components/helmet/HelmetSite'
import VerificationInfoUser from '../profile/inc/VerificationInfoUser'
import UndoRedo from '@components/undoredo'
import { useParams } from "react-router-dom"
import { useDispatch, useSelector } from "react-redux"
import { Row, Col } from "reactstrap"
import TransactiondonationUserShowCard from './inc/TransactiondonationUserShowCard'
import { loadShowTransactiondonationUser } from "../../redux/actions/pages/transactionAction"

const TransactiondonationUserShow = () => {
    const { transactiondonation } = useParams()
    const item = useSelector(state => state?.transactions?.transaction)
    const dispatch = useDispatch()

    useEffect(() => {
        const loadItems = async () => {
            await dispatch(loadShowTransactiondonationUser(transactiondonation))
        }
        loadItems()
    }, [transactiondonation])


    return (
        <>
            <HelmetSite title={`${item.title || process.env.REACT_APP_NAME}`} />
            <Breadcrumbs
                breadCrumbTitle="Transactions"
                breadCrumbParent={process.env.REACT_APP_NAME}
                breadCrumbActive={item.title || process.env.REACT_APP_NAME}
            />
            <VerificationInfoUser userItem={userSite} />
            {/** Undo redo */}
            <UndoRedo />
            <div className='invoice-preview-wrapper'>
                <Row className='invoice-preview'>
                    <Col xl={12} md={12} sm={12}>
                        <TransactiondonationUserShowCard transaction={item} />
                    </Col>
                </Row>
            </div>
        </>
    )
}

export default TransactiondonationUserShow