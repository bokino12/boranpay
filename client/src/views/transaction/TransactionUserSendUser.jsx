import React, { useEffect, useState } from 'react'
import Breadcrumbs from '@components/breadcrumbs'
import HelmetSite from '@components/helmet/HelmetSite'
import VerificationInfoUser from '../profile/inc/VerificationInfoUser'
import UndoRedo from '@components/undoredo'
import { authuserInfo } from '@components/service'
import { useParams } from 'react-router-dom'
import { Card, CardHeader} from 'reactstrap'
import { useDispatch, useSelector } from 'react-redux'
import { loadShowTransactionUserSendUser } from '../../redux/actions/pages/transactionAction'
import TransactionTable from './inc/TransactionTable'


const TransactionUserSendUser = () => {
    const { usersend, userrecev } = useParams()
    const [userSite] = useState(authuserInfo())
    const items = useSelector(state => state?.transactions?.transactions)
    const dispatch = useDispatch()
  
  
    useEffect(() => {
      const loadItems = async () => {
        if (userSite.slugin === usersend) {
          await dispatch(loadShowTransactionUserSendUser(usersend, userrecev))
        }
      }
      loadItems()
    }, [usersend, userrecev])

    return (
        <>
            <HelmetSite title={`Transactions - ${process.env.REACT_APP_NAME}`} />
            <Breadcrumbs
                breadCrumbTitle="Transactions"
                breadCrumbParent="Funtion"
                breadCrumbActive={`Transactions - ${process.env.REACT_APP_NAME}`}
            />
            <VerificationInfoUser userItem={userSite} />

            {/** Undo redo */}
            <UndoRedo />

            <Card>
                <CardHeader className='flex-md-row flex-column align-md-items-center align-items-start border-bottom'>
                    <div className="tasks-info">
                        <h4 className="mb-75">
                            <strong>Transactions  </strong>
                        </h4>
                    </div>
                </CardHeader>
                <TransactionTable items={items} />
            </Card>
        </>
    )
}

export default TransactionUserSendUser