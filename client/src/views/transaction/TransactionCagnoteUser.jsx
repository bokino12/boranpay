import React, { useEffect, useState } from 'react'
import Breadcrumbs from '@components/breadcrumbs'
import HelmetSite from '@components/helmet/HelmetSite'
import UndoRedo from '@components/undoredo'
import VerificationInfoUser from '../profile/inc/VerificationInfoUser'
import { authuserInfo } from '@components/service'
import { Plus } from 'react-feather'
import { Row, Col, Button } from 'reactstrap'
import { useDispatch, useSelector } from 'react-redux'
import { loadAllTransactionCagnotesUser } from '../../redux/actions/pages/transactionAction'
import TransactionCagnoteTable from './inc/TransactionCagnoteTable'
import SalesCard from '../dashboard/analytics/SalesCard'
import SoldsCagnoteCard from '../dashboard/analytics/SoldsCagnoteCard'
import { useHistory } from 'react-router-dom'

const TransactionCagnoteUser = () => {
    const history = useHistory()
    const [userSite] = useState(authuserInfo())
    const items = useSelector(state => state?.transactions?.transactioncagnotes)
    const dispatch = useDispatch()


    useEffect(() => {
        const loadItems = async () => {
            await dispatch(loadAllTransactionCagnotesUser(userSite.slugin))
        }
        loadItems()
    }, [userSite.slugin])

    return (
        <>
            <HelmetSite title={`Transactions Cagnotes - ${userSite.firstName}`} />
            <Breadcrumbs
                breadCrumbTitle="Transactions"
                breadCrumbParent="Funtion"
                breadCrumbActive={`Transactions cagnotes ${userSite.firstName}`}
            />
            <VerificationInfoUser userItem={userSite} />
            <Row className="match-height">
                <Col lg="7" md="6" sm="6">
                    <SoldsCagnoteCard {...userSite} />
                </Col>
                <Col lg="5" md="6" sm="6">
                    <SalesCard {...userSite} />
                </Col>
            </Row>
            {/** Undo redo */}
            <UndoRedo />
            <Button.Ripple onClick={() => history.push(`/mycagnotes/new/`)} className="mr-1 mb-1" color="primary" size="sm">
                <Plus size={14} /> <b>Nouvelle cagnote</b>
            </Button.Ripple>
            <Row>
                <Col sm="12">
                    <TransactionCagnoteTable items={items} />
                </Col>
            </Row>
        </>
    )
}

export default TransactionCagnoteUser