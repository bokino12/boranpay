// ** Third Party Components
import { Card, CardBody, CardText, Row, Col, Button } from 'reactstrap'
import Skeleton from 'react-loading-skeleton'
import { useHistory } from 'react-router-dom'
import moment from 'moment'

const InviceTransactionUserCard = ({ transaction }) => {
  const history = useHistory()
  const AmountTotal = transaction?.amount?.total
  const transactionUserProfile = transaction?.user?.profile
  const transactionUsertoProfile = transaction?.userto.profile
  return transaction !== null ? (
    <Card className='invoice-preview-card'>
      <CardBody className='invoice-padding pb-0'>
        {/* Header */}
        <div className='d-flex justify-content-between flex-md-row flex-column invoice-spacing mt-0'>
          {transaction?.userto ? (
            <div>
              <div className='logo-wrapper'>
                <h3>{transaction?.userto?.firstName || <Skeleton width={100} />} {transaction?.userto?.lastName}</h3>
              </div>
            </div>
          ) : (<Skeleton count={3} width={150} />)}
          <div className='mt-md-0 mt-2'>
            <h4 className='invoice-title'>
              <span className='invoice-number'>
                {transaction.invoiceNumber ? (
                  <b><span className={`invoice-number ${transaction.statusSend ? "text-success" : "text-danger"}`}>{transaction.statusSend ? <>+ {AmountTotal.formatMoney(2, '.', ',')}</> : <>- {AmountTotal.formatMoney(2, '.', ',')}</>} {transaction.amount.currency}</span></b>
                ) : (<Skeleton width={100} />)}
              </span>
            </h4>
            <div className='invoice-date-wrapper'>
              <p className='invoice-date-title'>Date Issued:</p>
              <p className='invoice-date'><strong>

                {transaction.invoiceNumber ? (
                  <small>{moment(transaction.createdAt).format('ll')}</small>
                ) : (<Skeleton width={50} />)}</strong></p>
            </div>
          </div>
        </div>
        {/* /Header */}
      </CardBody>

      <hr className='invoice-spacing' />

      {/* Address and Contact */}
      <CardBody className='invoice-padding pt-0'>
        <Row className='invoice-spacing'>
          <Col className='p-0' lg='8'>
            <h6 className='mb-2'>Invoice To:</h6>
            {transaction?.user && (
              <h6 className='mb-25'>{transaction?.user?.firstName} {transaction?.user?.lastName}</h6>
            )}
            <CardText className='mb-25'><a href={`mailto:${transaction?.user?.email}`}>{transaction?.user?.email}</a></CardText>
            {transaction.invoiceNumber && (
              <CardText className='mb-25'><b>#</b> {transaction.invoiceNumber}</CardText>
            )}
            {transaction?.user?.profile?.country && (
              <CardText className='mb-25'>{transactionUserProfile.country.name} {transactionUserProfile.addresse} </CardText>
            )}
            {transactionUserProfile.siteInternet && (
              <CardText className='mb-25'><a href={`${transactionUserProfile.siteInternet}`}>{transactionUserProfile.siteInternet}</a></CardText>
            )}
          </Col>
          <Col className='p-0 mt-xl-0 mt-2' lg='4'>
            <h6 className='mb-2'>Details:</h6>
            <table>
              <tbody>
                <tr>
                  <td>
                    <span className='font-weight-bolder'>{transaction?.userto?.firstName} {transaction?.userto?.lastName}</span>
                  </td>
                </tr>
                <tr>
                  <td><a href={`mailto:${transaction?.userto?.email}`}>{transaction?.userto?.email}</a></td>
                </tr>
                <tr>
                  <td><b>#</b> {transaction.invoiceNumber}</td>
                </tr>
                <tr>
                  {transaction?.userto?.profile?.country && (
                    <td>{transactionUsertoProfile.country.name} {transactionUsertoProfile.addresse} </td>
                  )}
                </tr>
                <tr>
                  {transactionUsertoProfile.siteInternet && (
                    <td><a href={`${transactionUsertoProfile.siteInternet}`}>{transactionUsertoProfile.siteInternet}</a></td>
                  )}
                </tr>
              </tbody>
            </table>
          </Col>
        </Row>
      </CardBody>
      {/* /Address and Contact */}

      <hr className='invoice-spacing' />

      {/* Total & Sales Person */}
      <CardBody className='invoice-padding pb-0'>
        <Row className='invoice-sales-total-wrapper'>
          <Col className='mt-md-0 mt-3' md='6' order={{ md: 1, lg: 2 }}>
            <CardText className='mb-0'>
              {(transaction.categoryId !== 2) ? ( // 2 correspond a la category du retrait
                <>
                  <Button.Ripple
                    onClick={() => history.push(`/transfert/user/${transaction.userto.slugin}/new`)}
                    className="mr-1 mb-1"
                    color="primary"
                  >
                    Transferts
                  </Button.Ripple>
                  {!transaction.statusSend && (
                    <>{/**
                     * <Button.Ripple
                      onClick={() => history.push("/transfert/new/")}
                      className="mr-1 mb-1"
                      color="warning"
                    >
                      Reclamer
                    </Button.Ripple> */}</>)}
                </>
              ) : (
                <>
                {/**
                <Button.Ripple
                  onClick={() => history.push("/transfert/new/")}
                  className="mr-1 mb-1"
                  color="primary"
                >
                  Annuler le retrait
                </Button.Ripple> */}
                </>
              )}
            </CardText>
          </Col>
          <Col className='d-flex justify-content-end' md='6' order={{ md: 2, lg: 1 }}>
            {transaction.amount.total ? (
              <>
                <div className='invoice-total-wrapper'>
                  <div className='invoice-total-item'>
                    <p className='invoice-total-title'>Montant:</p>
                    <p className='invoice-total-amount'>{(transaction.amount.total - transaction.amount.taxeTransaction)} <small>{transaction.amount.currency}</small></p>
                  </div>
                  {transaction.amount.taxeTransaction !== null && (
                    <>
                      <div className='invoice-total-item'>
                        <p className='invoice-total-title'>Tax:</p>
                        <p className='invoice-total-amount'>{transaction.amount.taxeTransaction} <small>{transaction.amount.currency}</small></p>
                      </div>
                    </>
                  )}
                  <hr className='my-50' />
                  <div className='invoice-total-item'>
                    <p className='invoice-total-title'>Total:</p>
                    <p className='invoice-total-amount'>{AmountTotal.formatMoney(2, '.', ',')} {transaction.amount.currency}</p>
                  </div>
                </div>
              </>

            ) : (<Skeleton width={100} />)}
          </Col>
        </Row>
      </CardBody>
      {/* /Total & Sales Person */}

      <hr className='invoice-spacing' />

      {/* Invoice Note */}
      <CardBody className='invoice-padding pt-0'>
        <Row>
          <Col sm='12'>
            <span className='font-weight-bold'>Note: </span>
            <span>
              {transaction.title} | {transaction.content}
            </span>
          </Col>
        </Row>
      </CardBody>
      {/* /Invoice Note */}
    </Card>
  ) : null
}

export default InviceTransactionUserCard
