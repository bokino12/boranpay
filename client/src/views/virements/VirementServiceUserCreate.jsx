import React, { useState } from 'react'
import HelmetSite from '../../@core/components/helmet/HelmetSite'
import UndoRedo from '@components/undoredo'
import { authuserInfo } from '@components/service'
import Breadcrumbs from '@components/breadcrumbs'
import VirementServiceUserForm from "./inc/VirementServiceUserForm"

const VirementServiceUserCreate = () => {
    const [userSite] = useState(authuserInfo())

    return (
        <>
            <HelmetSite title={`Nouveau Virement`} />
            <Breadcrumbs
                breadCrumbTitle="Virement"
                breadCrumbParent="Function"
                breadCrumbActive="Nouveau virement"
            />

            <UndoRedo />

            <VirementServiceUserForm userSite={userSite} />
        </>
    )
}

export default VirementServiceUserCreate