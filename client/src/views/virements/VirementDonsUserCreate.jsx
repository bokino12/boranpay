import React, { useState } from 'react'
import HelmetSite from '../../@core/components/helmet/HelmetSite'
import UndoRedo from '@components/undoredo'
import { authuserInfo } from '@components/service'
import Breadcrumbs from '@components/breadcrumbs'
import VirementDonsUserForm from "./inc/VirementDonsUserForm"

const VirementDonsUserCreate = () => {
    const [userSite] = useState(authuserInfo())

    return (
        <>
            <HelmetSite title={`Nouveau Virement`} />
            <Breadcrumbs
                breadCrumbTitle="Virement"
                breadCrumbParent="Function"
                breadCrumbActive="Nouveau virement"
            />

            <UndoRedo />

            <VirementDonsUserForm userSite={userSite} />
        </>
    )
}

export default VirementDonsUserCreate