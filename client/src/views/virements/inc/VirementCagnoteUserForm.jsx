import React, { useState, useEffect } from 'react'
import {
    Card,
    CardBody,
    Alert,
    CardHeader,
    FormGroup,
    Row,
    Col,
    Form,
    Button,
    Label
} from 'reactstrap'
import { AlertCircle } from 'react-feather'
import { Link, useHistory } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { loadAllCurrencies } from '../../../redux/actions/pages/currencyAction'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import Swal from 'sweetalert2'
import axios from 'axios'
import { authHeader } from '@components/service'
import { SuccessToast, ErrorToast } from '@components/toastalert'
import { toast } from 'react-toastify'
import userServices from '../../../services/userServices'

const schema = yup.object().shape({
    title: yup.string().required().min(3).max(200),
    total: yup.number().required().positive().integer(),
    currency: yup.string().required(),
    password: yup.string().required().min(8).max(200)
})

const VirementCagnoteUserForm = ({ userSite }) => {
    const history = useHistory()
    const [statusSend, setStatusSend] = useState(false)
    const [errormessage, setErrormessage] = useState('')
    const {
        register,
        handleSubmit,
        formState
    } = useForm({ resolver: yupResolver(schema) })
    const { errors, isSubmitting } = formState
    /** Get currencies with redux */
    const currencies = useSelector(state => state?.currencies?.currencies)
    const dispatch = useDispatch()
    useEffect(() => {
        const loadItems = async () => {
            await dispatch(loadAllCurrencies())
        }
        loadItems()
    }, [])

    const toggleStatus = () => { setStatusSend(!statusSend) }

    const onSubmit = async (data) => {
        Swal.fire({
            title: 'Confirmez-vous la transaction ?',
            text: "Êtes-vous sûr de vouloir executer cette action?",
            confirmButtonText: 'Oui, Confirmer',
            cancelButtonText: 'Non, annuler',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-primary",
            cancelButtonClass: 'btn btn-outline-default',
            showCancelButton: true,
            reverseButtons: false,
            showLoaderOnConfirm: true,
            preConfirm: () => {
                return userServices.userCheckValidate(userSite, data)
                    .then(async () => {

                        try {
                            const response = await axios.post(`${process.env.REACT_APP_SERVER_NODE_URL}/transfert_cagnote`, data, { headers: authHeader() })
                            if (response) {
                                toast.success(
                                    <SuccessToast name={'Success'} description={'Transaction éffectuée avec succès'} />, {
                                    position: toast.POSITION.TOP_RIGHT,
                                    hideProgressBar: true
                                })
                                history.push('/dashboard/')
                            }
                        } catch (error) {
                            toast.error(
                                <ErrorToast name={'Opp error'} description={'Quelque chose ne va pas ...'} />, {
                                position: toast.POSITION.TOP_RIGHT,
                                hideProgressBar: true
                            })
                            setErrormessage(error.response.data.message)
                        }

                    }).catch(error => {
                        Swal.showValidationMessage(`${error.response.data.message}`)
                    })
            },
            allowOutsideClick: () => !Swal.isLoading()
        })
    }

    return (
        <Card>
            <CardBody>
                <div className="col-md-10 mx-auto">
                    <div className="tasks-info">
                        <h3 className="mb-75">
                            <h3 className="mb-75">
                                <strong>Nouveau virement</strong>
                            </h3>
                        </h3>
                    </div>
                    <Form className="mt-2" onSubmit={handleSubmit(onSubmit)}>

                        {errormessage && (
                            <Alert color='danger'>
                                <div className='alert-body text-center'>
                                    <AlertCircle size={15} />{' '}
                                    <span className='ml-1'>
                                        {errormessage}
                                    </span>
                                </div>
                            </Alert>
                        )}

                        <br />

                        <Row>
                            <Col md="8" sm="8">
                                <FormGroup>
                                    <Label for="total"><strong>Montant virement</strong></Label>
                                    <input className={`form-control ${errors.total ? 'is-invalid' : ''}`}
                                        type="number"
                                        pattern="[0-9]*"
                                        inputMode="numeric"
                                        min="1" step="1"
                                        id="total"
                                        placeholder="Montant à virer"
                                        {...register('total')}
                                        autoComplete="off"
                                        required
                                    />
                                    <span className='invalid-feedback'>
                                        <strong>{errors.total?.message}</strong>
                                    </span>
                                </FormGroup>
                            </Col>

                            <Col md="4" sm="4">
                                <FormGroup>
                                    <Label for="currency"><strong>Devise</strong></Label>
                                    <select className={`form-control ${errors.currency ? 'is-invalid' : ''}`} {...register('currency')} id="currency" required>
                                        <option value=''>Selectioner une devise</option>
                                        {currencies.map((item, index) => (
                                            <option key={index} value={item.code} >{item.code}</option>
                                        ))}
                                    </select>
                                    <span className='invalid-feedback'>
                                        <strong>{errors.currency?.message}</strong>
                                    </span>
                                </FormGroup>
                            </Col>

                            <Col sm="12">
                                <FormGroup>
                                    <Label for="title"><strong>Titre</strong></Label>
                                    <input className={`form-control ${errors.title ? 'is-invalid' : ''}`}
                                        type="text"
                                        id="title"
                                        {...register('title')}
                                        placeholder="Donner un titre à la transaction"
                                        required
                                    />
                                    <span className='invalid-feedback'>
                                        <strong>{errors.title?.message}</strong>
                                    </span>
                                </FormGroup>
                            </Col>
                            {statusSend && (
                                <Col sm={12}>
                                    <FormGroup>
                                        <Label for="password"><strong>Veuillez saisir votre mot de passe</strong></Label>
                                        <input className={`form-control ${errors.password ? 'is-invalid' : ''}`}
                                            type="password"
                                            id="password"
                                            {...register('password')}
                                            autocomplete="off"
                                            placeholder="Mot de passe pour valider la transaction" required />
                                        <span className='invalid-feedback'>
                                            <strong>{errors.password?.message}</strong>
                                        </span>
                                    </FormGroup>
                                </Col>
                            )}
                            <div className="col-md-12 text-center">
                                <FormGroup className="form-label-group">
                                    {statusSend ? (
                                        <Button.Ripple
                                            disabled={isSubmitting}
                                            type="submit"
                                            className="btn-block"
                                            color="primary"
                                            block
                                        >
                                            Terminer
                                        </Button.Ripple>
                                    ) : (
                                            <Button.Ripple
                                                className="btn-block"
                                                color="primary"
                                                onClick={toggleStatus}
                                                block
                                            >
                                                Continuer
                                            </Button.Ripple>
                                        )}
                                </FormGroup>
                            </div>
                            <br />
                            <div className="col-md-12 text-center">
                                <Link to="/dashboard/" ><b>Annuler</b></Link>
                            </div>
                            <div className="col-md-12 text-center">
                                Sélectionnez attentivement votre devise avant d'éffectuer le virement. Une fois fait vérifier
                                votre solde principale.
                            </div>

                        </Row>
                    </Form>
                </div>
            </CardBody>
        </Card>
    )
}
export default VirementCagnoteUserForm
