import React from 'react'
import { Row, Col } from "reactstrap"
import UndoRedo from '@components/undoredo'
import Breadcrumbs from '@components/breadcrumbs'
import HelmetSite from '@components/helmet/HelmetSite'

const LandingpageUser = () => {
    return (
        <>
         <HelmetSite title={"About"} />
          <Breadcrumbs
            breadCrumbTitle="About"
            breadCrumbParent="Helps"
            breadCrumbActive="About"
          />
           <UndoRedo />
          <Row>
            <Col sm="12">
    
            </Col>
          </Row>
        </>
      )
}

export default LandingpageUser