import React, { useState, useEffect } from 'react'
import Breadcrumbs from '@components/breadcrumbs'
import HelmetSite from '@components/helmet/HelmetSite'
import UndoRedo from '@components/undoredo'
import {
    Card,
    CardBody,
    FormGroup,
    Alert,
    Row,
    Col,
    Form,
    Button,
    Label
} from 'reactstrap'
import { Plus, AlertCircle } from 'react-feather'
import { authHeader, authuserInfo } from '@components/service'
import VerificationInfoUser from '../profile/inc/VerificationInfoUser'
import { useDispatch, useSelector } from 'react-redux'
import { loadAllCurrencies } from '../../redux/actions/pages/currencyAction'
import { useParams, useHistory } from 'react-router-dom'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import Swal from 'sweetalert2'
import axios from 'axios'
import { SuccessToast, ErrorToast } from '@components/toastalert'
import { toast } from 'react-toastify'

const schema = yup.object().shape({
    title: yup.string().required().min(3).max(200),
    total: yup.number().required().positive().integer(),
    currency: yup.string().required().min(1).max(5)
})

const ReclamationUserCreateAndEdit = () => {
    const { reclamation, userslugin } = useParams()
    const history = useHistory()
    const [userSite] = useState(authuserInfo())
    const isAddMode = !reclamation
    const {
        register,
        handleSubmit,
        reset,
        setValue,
        formState
    } = useForm({ resolver: yupResolver(schema) })
    const { errors, isSubmitting } = formState
    /** Get currencies with redux */
    const [errormessage, setErrormessage] = useState('')

    const currencies = useSelector(state => state?.currencies?.currencies)
    const dispatch = useDispatch()

    useEffect(() => {
        const loadItems = async () => {
            await dispatch(loadAllCurrencies())
            if (!isAddMode && userSite.slugin === userslugin) {
                axios.get(`${process.env.REACT_APP_SERVER_NODE_URL}/reclamations/${reclamation}/${userslugin}`, { headers: authHeader() })
                    .then(response => {
                        const fields = ['title', 'total', 'content', 'currency']
                        fields.forEach(field => setValue(field, response.data[field]))
                    })
            }
        }
        loadItems()
    }, [reclamation, userslugin])

    const deleteItem = (reclamation) => {

        Swal.fire({
            title: 'Suppression?',
            text: "Êtes-vous sûr de vouloir executer cette action?",
            confirmButtonText: 'Oui, supprimer',
            cancelButtonText: 'Non, annuler',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-primary",
            cancelButtonClass: 'btn btn-outline-default',
            showCancelButton: true,
            reverseButtons: false
        }).then(async (result) => {
            if (result.value) {

                //Envoyer la requet au server
                await axios.delete(`${process.env.REACT_APP_SERVER_NODE_URL}/reclamations/${reclamation}`, { headers: authHeader() }).then(() => {
                    toast.success(
                        <SuccessToast name={'Success'} description={'Reclamation supprimé avec succès'} />, {
                        position: toast.POSITION.TOP_RIGHT,
                        hideProgressBar: true
                    })
                    history.push('/requests/')
                }).catch(() => {
                    toast.error(
                        <ErrorToast name={'Opp error'} description={'Oops! Quelque chose ne va pas ...'} />, {
                        position: toast.POSITION.TOP_RIGHT,
                        hideProgressBar: true
                    })
                })
            }
        })
    }

    const updateItem = async (reclamation, data) => {
        await axios.put(`${process.env.REACT_APP_SERVER_NODE_URL}/reclamations/${reclamation}`, data, { headers: authHeader() })
            .then((res) => {
                toast.success(
                    <SuccessToast name={'Success'} description={'Reclamation mis à jour avec succès'} />, {
                    position: toast.POSITION.TOP_RIGHT,
                    hideProgressBar: true
                })
                history.goBack()
            }).catch((error) => {
                toast.error(
                    <ErrorToast name={'Opp error'} description={'Oops! Quelque chose ne va pas ...'} />, {
                    position: toast.POSITION.TOP_RIGHT,
                    hideProgressBar: true
                })
                setErrormessage(error.response.data.message)
            })
    }

    const createItem = async (data, e) => {
        await axios.post(`${process.env.REACT_APP_SERVER_NODE_URL}/reclamations`, data, { headers: authHeader() })
            .then((res) => {
                toast.success(
                    <SuccessToast name={'Success'} description={'Reclamation crée avec succès'} />, {
                    position: toast.POSITION.TOP_RIGHT,
                    hideProgressBar: true
                })
                history.push('/requests/')
            }).catch((error) => {
                toast.error(
                    <ErrorToast name={'Opp error'} description={'Oops! Quelque chose ne va pas ...'} />, {
                    position: toast.POSITION.TOP_RIGHT,
                    hideProgressBar: true
                })
                setErrormessage(error.response.data.message)
            })
    }

    const onSubmit = (data) => {
        return isAddMode ? createItem(data) : updateItem(reclamation, data)
    }

    return (
        <>
            <HelmetSite title={`${isAddMode ? `Nouvelle reclamation` : `Editer cette reclamation`}`} />
            <Breadcrumbs
                breadCrumbTitle="Reclamations"
                breadCrumbParent="Sections"
                breadCrumbActive={isAddMode ? `Nouvelle reclamation` : `Editer cette reclamation`}
            />
            <VerificationInfoUser userItem={userSite} />
            {/** Undo redo */}
            <UndoRedo />

            {!isAddMode && (
                <Button.Ripple onClick={() => history.push(`/requests/new/`)} className="mr-1 mb-1" color="primary" size="sm">
                    <Plus size={14} /> <b>Nouvelle reclamation</b>
                </Button.Ripple>
            )}
            <Card>
                <CardBody>
                    <div className="col-md-10 mx-auto">
                        <div className="tasks-info">
                            <h4 className="mb-75">
                                <strong>{`${isAddMode ? `Nouvelle reclamation` : `Editer cette reclamation`}`}</strong>
                            </h4>
                        </div>
                        <Form className="mt-2" onSubmit={handleSubmit(onSubmit)} onReset={reset}>

                            {errormessage && (
                                <Alert color='danger'>
                                    <div className='alert-body text-center'>
                                        <AlertCircle size={15} />{' '}
                                        <span className='ml-1'>
                                            {errormessage}
                                        </span>
                                    </div>
                                </Alert>
                            )}

                            <br />
                            <Row>

                                <Col md={12} sm={12}>
                                    <FormGroup>
                                        <Label for="title"><strong>Titre</strong></Label>
                                        <input className={`form-control ${errors.title ? 'is-invalid' : ''}`}
                                            type="text"
                                            id="title"
                                            {...register('title')}
                                            placeholder="Titre ou object ..."
                                            required
                                        />
                                        <span className='invalid-feedback'>
                                            <strong>{errors.title?.message}</strong>
                                        </span>
                                    </FormGroup>
                                </Col>

                                <Col md={6} sm={6}>
                                    <FormGroup>
                                        <Label for="total"><strong>Montant à reclamer</strong></Label>
                                        <input className={`form-control ${errors.total ? 'is-invalid' : ''}`}
                                            type="number"
                                            pattern="[0-9]*"
                                            inputMode="numeric"
                                            min="1" step="1"
                                            id="total"
                                            placeholder="Montant à reclamer"
                                            {...register('total')}
                                            autoComplete="off"
                                            required
                                        />
                                        <span className='invalid-feedback'>
                                            <strong>{errors.total?.message}</strong>
                                        </span>
                                    </FormGroup>
                                </Col>
                                <Col md={6} sm={6}>
                                    <FormGroup>
                                        <Label for="currency"><strong>Devise</strong></Label>
                                        <select className={`form-control ${errors.currency ? 'is-invalid' : ''}`} {...register('currency')} id="currency" required>
                                            <option value=''>Selectioner une devise</option>
                                            {currencies.map((item, index) => (
                                                <option key={index} value={item.code} >{item.code}</option>
                                            ))}
                                        </select>
                                        <span className='invalid-feedback'>
                                            <strong>{errors.currency?.message}</strong>
                                        </span>
                                    </FormGroup>
                                </Col>
                                <Col sm="12">
                                    <FormGroup>
                                        <Label for="content"><strong>Description</strong> (facultatif)</Label>
                                        <textarea className={`form-control ${errors.content ? 'is-invalid' : ''}`}
                                            type="text"
                                            rows="3"
                                            maxLength="6000"
                                            id="content"
                                            {...register('content')}
                                            placeholder="Donner une description ..."
                                        />
                                        <span className='invalid-feedback'>
                                            <strong>{errors.content?.message}</strong>
                                        </span>
                                    </FormGroup>
                                </Col>
                                <div className="col-md-12 text-center">
                                    <FormGroup className="form-label-group">
                                        <Button.Ripple
                                            disabled={isSubmitting}
                                            type="submit"
                                            className="btn-block"
                                            color="primary">
                                            Sauvegarder
                                        </Button.Ripple>
                                        <Button.Ripple
                                            onClick={() => history.goBack()}
                                            className="btn-block"
                                            color="default"
                                        >
                                            Annuler
                                        </Button.Ripple>
                                    </FormGroup>
                                </div>
                                {(!isAddMode && userSite.slugin === userslugin) && (
                                    <div className="col-md-12 text-center">
                                        <a href={void (0)} onClick={() => deleteItem(reclamation)} className="nav-link text-danger" ><b>Supprimer</b></a>
                                    </div>
                                )}
                            </Row>
                        </Form>
                    </div>
                </CardBody>
            </Card>
        </>
    )
}

export default ReclamationUserCreateAndEdit