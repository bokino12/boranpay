import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import {
    Eye,
    Copy,
    MoreVertical,
    Edit,
    Trash,
    ChevronDown
} from 'react-feather'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import { Card, CardBody, CardHeader, UncontrolledDropdown, DropdownMenu, DropdownToggle, DropdownItem } from 'reactstrap'
import DataTable from 'react-data-table-component'
import { loadReclamationUser, deleteItem } from '../../../redux/actions/pages/reclamationAction'
import { SuccessToast } from '@components/toastalert'
import { toast } from 'react-toastify'
import moment from 'moment'

class ReclamationTable extends Component {

    onCopy = () => {
        toast.success(
            <SuccessToast name={'Success'} description={'Lien copié avec succès partager a present'} />, {
            position: toast.POSITION.TOP_CENTER,
            hideProgressBar: true
        })
    }

    async componentDidMount() {
        await this.props.loadReclamationUser(this.props.slugin)
    }

    render() {
        const { items } = this.props
        const columns = [
            {
                name: <h5>Montant</h5>,
                selector: "total",
                sortable: true,
                grow: 4
            },
            {
                name: <h5>Titre</h5>,
                selector: "title",
                sortable: true,
                grow: 2
            },
            {
                name: <h5>Date</h5>,
                selector: "date",
                sortable: true,
                grow: 2
            },
            {
                name: "",
                selector: "row_button",
                ignoreRowClick: true,
                allowOverflow: true,
                button: true
            }
        ]

        const data = items?.length >= 0 ? (
            items.map(item => (
                {
                    total: <Link to={`/requests/${item.slugin}/`} className="text-dark"><h4><b>{(item.total).formatMoney(2, '.', ',')} <small>{item.currency}</small></b></h4> </Link>,
                    title: <Link to={`/requests/${item.slugin}/`} className="text-dark"><b>{(item.title.length > 35 ? item.title.substring(0, 35) : item.title)}</b></Link>,
                    date: <Link to={`/requests/${item.slugin}/`} className="text-dark"><b>{moment(item.createdAt).format('ll')}</b></Link>,
                    row_button: (
                        <>
                            <div className='column-action d-flex align-items-center'>
                                <CopyToClipboard
                                    onCopy={this.onCopy}
                                    text={`${process.env.REACT_APP_LINK}/requests/${item.slugin}/`}
                                >
                                    <Copy size={17} className='mx-1 cursor-pointer' />
                                </CopyToClipboard>
                                <UncontrolledDropdown>
                                    <DropdownToggle tag='span'>
                                        <MoreVertical size={17} className='cursor-pointer' />
                                    </DropdownToggle>
                                    <DropdownMenu right>
                                        <DropdownItem tag={Link} to={`/requests/${item.slugin}/${item.user.slugin}/edit/`} className='w-100'>
                                            <Edit size={14} className='mr-50' />
                                            <span className='align-middle'>Editer</span>
                                        </DropdownItem>
                                        <DropdownItem tag='a' href={void (0)} className='w-100' onClick={() => this.props.deleteItem(item)}>
                                            <Trash size={14} className='mr-50' />
                                            <span className='align-middle'>Supprimer</span>
                                        </DropdownItem>
                                    </DropdownMenu>
                                </UncontrolledDropdown>
                            </div>
                        </>
                    )
                }
            )
            )
        ) : (
                <></>
            )

        return (
            <Card>

                <CardHeader className='flex-md-row flex-column align-md-items-center align-items-start border-bottom'>
                    <div className="tasks-info">
                        <h3 className="mb-75">
                            <strong>Reclamations </strong>
                        </h3>
                    </div>
                </CardHeader>

                <CardBody>
                    <br />
                    <DataTable
                        className='react-dataTable'
                        sortIcon={<ChevronDown size={10} />}
                        paginationRowsPerPageOptions={[10, 25, 50, 100]}
                        data={data}
                        columns={columns}
                        defaultSortField="createdAt"
                        defaultSortAsc={false}
                        overflowY={true}
                        pagination={true}
                        noHeader />
                </CardBody>
            </Card>
        )
    }
}
ReclamationTable.propTypes = {
    loadReclamationUser: PropTypes.func.isRequired
}

const mapStoreToProps = store => ({
    items: store?.reclamations?.reclamations
})

export default connect(mapStoreToProps, {
    loadReclamationUser, deleteItem
})(ReclamationTable)

