import React, { Fragment, Component } from 'react'
import { Card, CardBody, CardHeader, UncontrolledDropdown, DropdownMenu, DropdownToggle, DropdownItem, Badge } from 'reactstrap'
import DataTable from 'react-data-table-component'
import { Link } from 'react-router-dom'
import {
    Eye,
    Activity,
    Copy,
    MoreVertical,
    Edit,
    ChevronDown,
    CheckCircle,
    XOctagon,
    Users,
    Key,
    Trash
} from 'react-feather'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import { loadCagnoteUser, deleteItem, closeItem } from '../../../redux/actions/pages/cagnoteAction'
import moment from 'moment'
import { SuccessToast } from '@components/toastalert'
import { toast } from 'react-toastify'


class CagnoteTable extends Component {

    onCopy = () => {
        toast.success(
            <SuccessToast name={'Success'} description={'Lien copié avec succès partager a present'} />, {
            position: toast.POSITION.TOP_CENTER,
            hideProgressBar: true
        })
    }

    async componentDidMount() {
        await this.props.loadCagnoteUser(this.props.slugin)
    }

    render() {
        const { items } = this.props
        const columns = [
            {
                name: <h5>Date</h5>,
                selector: "date",
                sortable: true
            },
            {
                name: <h5>Collectées</h5>,
                selector: "price_collecte",
                sortable: true,
                grow: 2
            },
            {
                name: <h5>Objectif</h5>,
                selector: "price_objectif",
                sortable: true,
                grow: 2
            },
            {
                name: <h5>Contributeurs</h5>,
                selector: "contributors",
                sortable: true,
                grow: 2
            },
            {
                name: <h5></h5>,
                selector: "isclosing",
                sortable: true,
                grow: 2
            },
            {
                name: "",
                selector: "row_button",
                ignoreRowClick: true,
                allowOverflow: true,
                button: true
            }
        ]

        const data = items?.length >= 0 ? (
            items.map(item => (
                {
                    date: <Link to={`/cagnote/${item.slug}/`} className="text-dark"><b>{moment(item.createdAt).format('ll')}</b></Link>,
                    price_collecte: <Link to={`/cagnote/${item.slug}/`}><h4 className="text-success"> {item.amountcagnotes.length > 0 ? <><p className="text-success">
                        {/** Ici c'est pour capturer et convertire */}
                        + {(item.amountcagnotes[0].totalAmountcagnote * item.currency.currencyNumber).formatMoney(2, '.', ',')}
                        <small> {item.currency.code}</small></p></> : <>0,00 <small>{item.currency.code}</small></>}</h4></Link>,
                    price_objectif: <Link to={`/cagnote/${item.slug}/`}><h4 className="text-dark"> {(item.total).formatMoney(2, '.', ',')} <small>{item.currency.code}</small></h4></Link>,
                    contributors: <h4 className="text-dark">{item.contributes.length ? <>{(item.contributes[0].contributes_count - 1)}</> : 0} <Users size={20} /></h4>,
                    isclosing: <p>{!item.isClosing ? (
                        <Badge color='success'> <CheckCircle size={14} className='mr-50' /> Ouvert </Badge>
                    ) : (<Badge color='danger'> <XOctagon size={14} className='mr-50' /> Cloturer </Badge>)}</p>,
                    row_button: (
                        <>
                            <div className='column-action d-flex align-items-center'>
                                <CopyToClipboard
                                    onCopy={this.onCopy}
                                    text={`${process.env.REACT_APP_LINK}/cagnote/${item.slug}/`}
                                >
                                    <Copy size={17} className='mx-1 cursor-pointer' />
                                </CopyToClipboard>

                                <Link to={`/cagnote/${item.slug}/`}>
                                    <Eye size={17} className='mx-1' />
                                </Link>
                                {/**<Link to={`/cagnote/${item.slug}/${item.slugin}/`}>
                                    <Activity size={17} className='mx-1' />
                                </Link> */}
                                <UncontrolledDropdown>
                                    <DropdownToggle tag='span'>
                                        <MoreVertical size={17} className='cursor-pointer' />
                                    </DropdownToggle>
                                    <DropdownMenu right>
                                        <DropdownItem tag={Link} to={`/cagnote/${item.slug}/statistique/`} className='w-100'>
                                            <Activity size={14} className='mr-50' />
                                            <span className='align-middle'>Statistique</span>
                                        </DropdownItem>
                                        <DropdownItem tag={Link} to={`/mycagnotes/${item.slugin}/edit/`} className='w-100'>
                                            <Edit size={14} className='mr-50' />
                                            <span className='align-middle'>Editer</span>
                                        </DropdownItem>
                                        {!item.isClosing && (
                                            <DropdownItem tag='a' href={void (0)} className='w-100' onClick={() => this.props.closeItem(item)} className='w-100'>
                                                <Key size={14} className='mr-50' />
                                                <span className='align-middle'>Cloturer</span>
                                            </DropdownItem>
                                        )}
                                        <DropdownItem tag='a' href={void (0)} className='w-100' onClick={() => this.props.deleteItem(item)}>
                                            <Trash size={14} className='mr-50' />
                                            <span className='align-middle'>Supprimer</span>
                                        </DropdownItem>
                                    </DropdownMenu>
                                </UncontrolledDropdown>
                            </div>
                        </>
                    )
                }
            )
            )
        ) : (
            <></>
        )

        return (
            <>
                <Card>

                    <CardHeader className='flex-md-row flex-column align-md-items-center align-items-start border-bottom'>
                        <div className="tasks-info">
                            <h3 className="mb-75">
                                <strong>Cagnotes</strong>
                            </h3>
                        </div>
                    </CardHeader>

                    <CardBody>
                        <br />
                        <DataTable
                            className='react-dataTable'
                            sortIcon={<ChevronDown size={10} />}
                            paginationRowsPerPageOptions={[10, 25, 50, 100]}
                            data={data}
                            columns={columns}
                            defaultSortField="createdAt"
                            defaultSortAsc={false}
                            overflowY={true}
                            pagination={true}
                            noHeader />
                    </CardBody>
                </Card>
            </>
        )

    }
}
CagnoteTable.propTypes = {
    loadCagnoteUser: PropTypes.func.isRequired
}

const mapStoreToProps = store => ({
    items: store?.cagnotes?.cagnotes
})

export default connect(mapStoreToProps, {
    loadCagnoteUser, deleteItem, closeItem
})(CagnoteTable)

