import React, { useEffect, useState, useCallback } from "react"
import classnames from 'classnames'
import Avatar from '@components/avatar'
import { useDispatch, useSelector } from "react-redux"
import { loadShowCagnoteUserContributes } from '../../../redux/actions/pages/cagnoteAction'
import { Button, Card, CardBody } from 'reactstrap'
import moment from 'moment'

const renderClient = item => {
    const stateNum = Math.floor(Math.random() * 6),
        states = ['light-success', 'light-danger', 'light-warning', 'light-info', 'light-primary', 'light-secondary'],
        color = states[stateNum]

    if (item?.user?.avatar) {
        return <Avatar className='mr-50' img={item?.user?.avatar} width='50' height='50' />
    } else {
        return <Avatar color={color} className='mr-50' content={`${item?.user?.firstName} ${item?.user?.lastName}`} initials  imgHeight='50' imgWidth='50'/>
    }
}

const ReadMore = ({ children }) => {
    const text = children
    const [isReadMore, setIsReadMore] = useState(true)
    const toggleReadMore = () => {
        setIsReadMore(!isReadMore)
    }
    return (
        <p className="text">
            {isReadMore ? text?.slice(0, 70) : text || ''}
            {text?.length >= 70 && (
                <span style={{ cursor: 'pointer' }} onClick={toggleReadMore} className="text-info">
                    {isReadMore ? '...lire plus' : ' moins'}
                </span>
            )}
        </p>
    )
}

const ContributorsUserList = ({ cagnote }) => {
    const [maxRange, setMaxRange] = useState(6)
    const contributes = useSelector(state => state?.cagnotes?.contributes)
    const dispatch = useDispatch()

    const loadMore = useCallback(() => {
        setMaxRange(prevRange => prevRange + 6)
    }, [])

    // const loadMore = () => {
    //     setMaxRange(prevRange => prevRange + 4)
    // }

    useEffect(() => {
        const loadItems = async () => {
            await dispatch(loadShowCagnoteUserContributes(cagnote))
        }
        loadItems()
    }, [cagnote])

    return (
        <Card>
            <CardBody>
                {contributes.slice(0, maxRange).map((item, index) => (
                    <div
                        key={index}
                        className={classnames('d-flex justify-content-start align-items-center', {
                            'mt-2': index === 0,
                            'mt-1': index !== 0
                        })}>
                        {renderClient(item)}
                        <div className='profile-user-info'>
                            <h6 className='mb-0'>{(item?.statusUser) ? 'Anonyme' : `${item?.user?.firstName} ${item?.user?.lastName}`}</h6>
                            <small className='text-muted'>{moment(item?.createdAt).format('ll')}</small>
                            <br />
                            <ReadMore>
                                {item.content}
                            </ReadMore>
                        </div>
                        <div className='ml-auto'>
                            <h6>{item?.total} {item?.currency}</h6>
                        </div>
                    </div>
                ))}
            </CardBody>
            {maxRange < contributes.length && (
                <div className='text-center'>
                    <Button onClick={loadMore} color={'link'}>Voir plus</Button>
                </div>
            )}
        </Card>
    )
}

export default ContributorsUserList
