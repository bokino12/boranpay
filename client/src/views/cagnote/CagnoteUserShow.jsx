import React, { useEffect, useState } from "react"
import Breadcrumbs from '@components/breadcrumbs'
import HelmetSite from '@components/helmet/HelmetSite'
import { authuserInfo } from '@components/service'
import { CardBody, ListGroup, Card, Row, Col, Button } from "reactstrap"
import { useParams, useHistory } from "react-router-dom"
import { Copy, Edit, Activity, Key } from "react-feather"
import ContributionUserForm from "./inc/ContributionUserForm"
import ContactUserForm from "../contact/ContactUserForm"
import ContributorsUserList from "../cagnote/inc/ContributorsUserList"
import PoucentageCagnote from './inc/PoucentageCagnote'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import { useDispatch, useSelector } from "react-redux"
import { loadShowCagnoteUser } from '../../redux/actions/pages/cagnoteAction'
import VerificationInfoUser from '../profile/inc/VerificationInfoUser'
import moment from 'moment'
import Avatar from '@components/avatar'
import { SuccessToast } from '@components/toastalert'
import { toast } from 'react-toastify'

const renderClient = item => {
    const stateNum = Math.floor(Math.random() * 6),
        states = ['light-success', 'light-danger', 'light-warning', 'light-info', 'light-primary', 'light-secondary'],
        color = states[stateNum]

    if (item?.user?.avatar) {
        return <Avatar img={item?.user?.avatar} width='50' height='50' />
    } else {
        return <Avatar color={color} content={`${item?.user?.firstName} ${item?.user?.lastName}`} initials imgHeight='50' imgWidth='50' />
    }
}

const CagnoteUserShow = () => {
    const history = useHistory()
    const { cagnoteslugin } = useParams()
    const [userSite] = useState(authuserInfo())
    const [statusPayement, setStatusPayement] = useState(false)
    const item = useSelector(state => state?.cagnotes?.cagnote)
    const dispatch = useDispatch()

    useEffect(() => {
        const loadItems = async () => {
            await dispatch(loadShowCagnoteUser(cagnoteslugin))
            window.scroll({ top: 0, left: 0, behavior: 'smooth' })
        }
        loadItems()
    }, [cagnoteslugin])


    const toggleStatus = () => { setStatusPayement(!statusPayement) }

    const onCopy = () => {
        toast.success(
            <SuccessToast name={'Success'} description={'Lien copié avec succès'} />, {
            position: toast.POSITION.TOP_CENTER,
            hideProgressBar: true
        })
    }

    const getDescription = (item) => {
        return { __html: (item.content) }
    }
    return (
        <>
            <HelmetSite title={`${item?.title || process.env.REACT_APP_NAME}`} />
            <Breadcrumbs
                breadCrumbTitle="Cagnote"
                breadCrumbParent="Discover"
                breadCrumbActive={'Cagnote'}
            />
            <VerificationInfoUser userItem={userSite} />
            <Row>

                <Col lg="8" md="7" sm="12">
                    <Card>
                        <CardBody>
                            <div className="d-flex justify-content-start align-items-center mb-1">

                                <div className="avatar mr-1">
                                    {renderClient(item)}
                                </div>
                                <div className="user-page-info">
                                    <p className="mb-0"><b>{item?.user?.firstName} {item?.user?.lastName}</b></p>
                                    <span className="font-small-2">{moment(item?.createdAt).format('ll')}</span>
                                </div>
                                <div className="ml-auto user-like">
                                    {(item?.userId === userSite.userId) && (
                                        <>
                                            <Button.Ripple onClick={() => history.push(`/mycagnotes/${item.slugin}/edit/`)} className="mr-1 mb-1 border-primary text-primary" color="flat-primary" size="sm">
                                                <Edit size={17} className='mr-50' /> <b>Editer</b>
                                            </Button.Ripple>
                                            <Button.Ripple onClick={() => history.push(`/cagnote/${item.slug}/statistique/`)} className="mr-1 mb-1 border-primary text-primary" color="flat-primary" size="sm">
                                                <Activity size={14} className='mr-50' /> <b>Statistique</b>
                                            </Button.Ripple>
                                        </>
                                    )}

                                    {item?.slug && (
                                        <CopyToClipboard
                                            onCopy={onCopy}
                                            text={`${process.env.REACT_APP_LINK}/cagnote/${item?.slug}`}
                                        >
                                            <Button.Ripple className="mr-1 mb-1" color="primary" size="sm">
                                                <Copy size={17} className='cursor-pointer' /> <b>Copier le lien</b>
                                            </Button.Ripple>
                                        </CopyToClipboard>

                                    )}
                                </div>
                                {/**
                                 * <div className="ml-auto user-like">
                                    <Heart fill="#EA5455" stroke="#EA5455" />
                                </div>
                                 */}

                            </div>

                            {item?.total && (
                                <PoucentageCagnote {...item} />
                            )}
                            <br />
                            {/**
                             * 
                            <div className="row">
                                <img
                                    src="https://ivemo-cm.s3.eu-central-1.amazonaws.com/img/locations/de2a5dc5106a1d9d1a250251f78436c16c7923b7.png"
                                    alt="avtar img holder"
                                    width="100%"
                                />
                            </div>
                             */}

                            <br />
                            <h4>{item.title}</h4>
                            <br />
                            <span className="title text-justify" dangerouslySetInnerHTML={getDescription(item)} />
                            <br />
                            {!item.isDelete && !item.isClosing  && (
                                <>
                                    <ListGroup flush>

                                        <ContributionUserForm cagnoteItem={item} />

                                    </ListGroup>
                                </>
                            )}
                            {item.isClosing && (
                                <Col sm="12" className="text-center">

                                    <Button.Ripple
                                        type="submit"
                                        color="danger"
                                    >
                                         Cagnote cloturé
                                    </Button.Ripple>
                                </Col>
                            )}
                            <br />
                            <ListGroup flush>

                                <Col sm="12" className="text-center">
                                    <h4>Contributeurs</h4>
                                </Col>

                                <ContributorsUserList cagnote={item.slugin} />

                            </ListGroup>

                        </CardBody>
                    </Card>
                </Col>
                <Col lg="4" md="5" sm="12">
                    <Card>
                        <CardBody className="knowledge-base-category">
                            <ListGroup flush>

                                <ContactUserForm dataItem={item} />

                            </ListGroup>
                        </CardBody>
                    </Card>
                </Col>
            </Row>
        </>
    )
}

export default CagnoteUserShow