import { Fragment, useState } from 'react'
import ProfileTabs from './ProfileTabs'
import { authuserInfo } from '@components/service'
import PersonalInfosTab from './PersonalInfosTab'
import Breadcrumbs from '@components/breadcrumbs'
import UserProfile from './UserProfile'
import PasswordTab from './PasswordTab'
import NotificationsTab from './NotificationsTab'
import { Row, Col, TabContent, TabPane, Card, CardBody } from 'reactstrap'

import '@styles/react/libs/flatpickr/flatpickr.scss'
import '@styles/react/pages/page-account-settings.scss'

const ProfileIndex = () => {
  const [userSite] = useState(authuserInfo())
  const [activeTab, setActiveTab] = useState('1')

  const toggleTab = tab => { setActiveTab(tab) }


  return (
    <Fragment>
      <Breadcrumbs breadCrumbTitle='Profil' breadCrumbParent='Pages' breadCrumbActive='Edition profil' />
      <Row>
        <Col className='mb-2 mb-md-0' md='3'>
          <Card>
            <ProfileTabs activeTab={activeTab} toggleTab={toggleTab} />
          </Card>
        </Col>
        <Col md='9'>
          <Card>
            <CardBody>
              <TabContent activeTab={activeTab}>
                <TabPane tabId='1'>
                  <UserProfile userItem={userSite} />
                </TabPane>
                <TabPane tabId='2'>
                  <PersonalInfosTab userItem={userSite} />
                </TabPane>
                <TabPane tabId='3'>
                  <PasswordTab userItem={userSite}/>
                </TabPane>
                {/**
                <TabPane tabId='4'>
                  <NotificationsTab />
                </TabPane>
                 */}
              </TabContent>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </Fragment>
  )
}

export default ProfileIndex
