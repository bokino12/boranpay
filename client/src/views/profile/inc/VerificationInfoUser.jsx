import React from 'react'
import { Alert, Col, Row } from 'reactstrap'
import { authHeader } from '@components/service'
import axios from 'axios'
import '@styles/base/pages/page-auth.scss'
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'
const MySwal = withReactContent(Swal)


const VerificationInfoUser = ({ userItem }) => {

    const sendItem = async (data, e) => {

        try {
            const response = await axios.post(`${process.env.REACT_APP_SERVER_NODE_URL}/resend_email/${userItem.slugin}`, data, { headers: authHeader() })
            if (response) {
                MySwal.fire({
                    title: 'Bien joué!',
                    text: `Nous vous avons renvoyé le lien de confirmation de compte sur cette ${userItem.email}`,
                    icon: 'success',
                    confirmButtonText: 'Oui, compris',
                    customClass: {
                        confirmButton: 'btn btn-primary'
                    },
                    buttonsStyling: false
                })
            }
        } catch (error) { setMessage(error.response.data.message) }
    }

    return (
        <>
            <Row>
                {!userItem.emailVerifiedStatus && (
                    <Col lg="12" md="12" sm="12">
                        <Alert color='info'>
                            <div className='alert-body'>
                                <span>
                                    Votre e-mail n'est pas confirmé. Veuillez consulter votre boite mail. {''}
                                    <a href={void (0)} className='alert-link' onClick={() => sendItem(userItem)}
                                    >
                                        Pas reçu? Renvoyer l'e-mail
                                    </a>{' '}
                                </span>
                            </div>
                        </Alert>
                    </Col>
                )}
            </Row>
        </>
    )
}

export default VerificationInfoUser