import React, { useState, useEffect } from 'react'
import { useForm } from 'react-hook-form'
import { authHeader } from '@components/service'
import { loadAllCountries } from '../../redux/actions/pages/countryAction'
import { useDispatch, useSelector } from 'react-redux'
import { loadAllCurrencies } from '../../redux/actions/pages/currencyAction'
import { Label, FormGroup, Row, Col, Button, Form } from 'reactstrap'
import axios from 'axios'
import VerificationInfoUser from './inc/VerificationInfoUser'
import { SuccessToast, ErrorToast } from '@components/toastalert'
import { toast } from 'react-toastify'


const PersonalInfosTab = ({ userItem }) => {
  const [errormessage, setErrormessage] = useState('')
  const {
    register,
    handleSubmit,
    setValue,
    formState
  } = useForm()
  const { errors, isSubmitting } = formState

  /** Get currencies with redux */
  const currencies = useSelector(state => state?.currencies?.currencies)
  const countries = useSelector(state => state?.countries?.countries)
  const dispatch = useDispatch()

  useEffect(() => {
    const loadItems = async () => {
      await dispatch(loadAllCurrencies())
      await dispatch(loadAllCountries())
      await axios.get(`${process.env.REACT_APP_SERVER_NODE_URL}/userinfo/${userItem.slugin}`, { headers: authHeader() })
        .then(res => {
          const fields = ['countryId', 'addresse', 'currencyId', 'siteInternet']
          fields.forEach(field => setValue(field, res.data.profile[field]))
        })
    }
    loadItems()
  }, [userItem.slugin])

  const onSubmit = async (data) => {

    try {
      const response = await axios.put(`${process.env.REACT_APP_SERVER_NODE_URL}/users/${userItem.slugin}/profile`, data, { headers: authHeader() })
      if (response) {
        toast.success(
          <SuccessToast name={'Success'} description={'Données sauvegardées avec succès'} />, {
          position: toast.POSITION.TOP_RIGHT,
          hideProgressBar: true
        })
      }
    } catch (error) {
      toast.error(
        <ErrorToast name={'Opp error'} description={'Oops! Quelque chose ne va pas ...'} />, {
        position: toast.POSITION.TOP_RIGHT,
        hideProgressBar: true
      })
      setErrormessage(error.response.data.message)
    }
  }

  return (
    <Form onSubmit={handleSubmit(onSubmit)}>
      <Row>
        <Col sm='6'>
          <FormGroup>
            <Label for="countryId">Pays de residence</Label>
            <select className={`form-control ${errors.countryId ? 'is-invalid' : ''}`} {...register("countryId")} id="countryId" required="required">
              {countries.map((item, index) => (
                <option key={index} value={item.id} >{item.name}</option>
              ))}
            </select>
            <span className='invalid-feedback'>
              <strong>{errors.countryId?.message}</strong>
            </span>
          </FormGroup>
        </Col>
        <Col sm='6'>
          <FormGroup>
            <Label for="currencyId">Devise</Label>
            <select className={`form-control ${errors.currencyId ? 'is-invalid' : ''}`} {...register("currencyId")} id="currencyId" required="required">
              {currencies.map((item, index) => (
                <option key={index} value={item.id} >{item.code}</option>
              ))}
            </select>
            <span className='invalid-feedback'>
              <strong>{errors.currencyId?.message}</strong>
            </span>
          </FormGroup>
        </Col>
        <Col sm='6'>
          <FormGroup >
            <Label for="siteInternet">Site internet</Label>
            <input className={`form-control ${errors.siteInternet ? 'is-invalid' : ''}`}
              type="url"
              id="siteInternet"
              placeholder="Ajouter votre site internet ..."
              {...register("siteInternet")}
            />
            <span className='invalid-feedback'>
              <strong>{errors.siteInternet?.message}</strong>
            </span>
          </FormGroup>
        </Col>
        <Col sm='6'>
          <FormGroup >
            <Label for="addresse">Adresse</Label>
            <input className={`form-control ${errors.addresse ? 'is-invalid' : ''}`}
              type="text"
              id="addresse"
              placeholder="Donner votre localisation"
              {...register("addresse")}
              required
            />
            <span className='invalid-feedback'>
              <strong>{errors.addresse?.message}</strong>
            </span>
          </FormGroup>
        </Col>
        <Col className='mt-2 text-center' sm='12'>
          <Button.Ripple
            type='submit'
            className='mr-1'
            color='primary'
            disabled={isSubmitting} >
            Sauvegarder
          </Button.Ripple>
        </Col>
      </Row>
      <br />
      <VerificationInfoUser userItem={userItem} />
    </Form>
  )
}

export default PersonalInfosTab
