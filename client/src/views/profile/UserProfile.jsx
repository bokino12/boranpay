import { Fragment, useEffect, useState } from 'react'
import { authHeader } from '@components/service'
import { Button, Media, Label, Row, Col, Input, FormGroup, Alert, Form } from 'reactstrap'
import Flatpickr from 'react-flatpickr'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import axios from 'axios'
import VerificationInfoUser from './inc/VerificationInfoUser'
import { SuccessToast, ErrorToast } from '@components/toastalert'
import { toast } from 'react-toastify'

import '@styles/react/libs/flatpickr/flatpickr.scss'

const schema = yup.object().shape({
    firstName: yup.string().required().min(3).max(200),
    lastName: yup.string().required().min(3).max(200),
    username: yup.string().required().min(3).max(200),
    phone: yup.string().required().min(7).max(15),
    email: yup.string().email().required().min(3).max(200)
})

const UserProfile = ({ userItem }) => {
    const [errormessage, setErrormessage] = useState('')
    const {
        register,
        handleSubmit,
        setValue,
        formState
    } = useForm({ resolver: yupResolver(schema) })
    const { errors, isSubmitting } = formState

    //const [avatar, setAvatar] = useState(data.avatar ? data.avatar : '')

    { /*const onChange = e => {
    const reader = new FileReader(),
      files = e.target.files
    reader.onload = function () {
      setAvatar(reader.result)
    }
    reader.readAsDataURL(files[0])
  }*/ }

    useEffect(() => {
        const loadItems = async () => {
            await axios.get(`${process.env.REACT_APP_SERVER_NODE_URL}/userinfo/${userItem.slugin}`, { headers: authHeader() })
                .then(res => {
                    const fields = ['username', 'firstName', 'sex', 'lastName', 'email', 'phone', 'birstday']
                    fields.forEach(field => setValue(field, res.data[field]))
                })
        }
        loadItems()
    }, [userItem.slugin])

    const onSubmit = async (data) => {

        await axios.put(`${process.env.REACT_APP_SERVER_NODE_URL}/users/${userItem.slugin}`, data, { headers: authHeader() })
            .then((res) => {
                if (res.data.accessToken) {
                    localStorage.setItem(process.env.REACT_APP_BASE_NAMETOKEN, JSON.stringify(res.data))
                }
                toast.success(
                    <SuccessToast name={'Success'} description={'Données sauvegardées avec succès'} />, {
                    position: toast.POSITION.TOP_RIGHT,
                    hideProgressBar: true
                })

            }).catch((error) => {
                toast.error(
                    <ErrorToast name={'Opp error'} description={'Oops! Quelque chose ne va pas ...'} />, {
                    position: toast.POSITION.TOP_RIGHT,
                    hideProgressBar: true
                })
                setErrormessage(error.response.data.message)
            })
    }

    return (
        <Fragment>
            <Media>
                <Media className='mr-25' left>
                    <Media object className='rounded mr-50' src={'https://ivemo-cm.s3.eu-central-1.amazonaws.com/img/locations/de2a5dc5106a1d9d1a250251f78436c16c7923b7.png'} alt='Generic placeholder image' height='80' width='80' />
                </Media>
                <Media className='mt-75 ml-1' body>
                    <Button.Ripple tag={Label} className='mr-75' size='sm' color='primary'>
                        Upload
                    <Input type='file' hidden accept='image/*' />
                    </Button.Ripple>
                    <Button.Ripple color='secondary' size='sm' outline>
                        Reset
                    </Button.Ripple>
                    <p>Allowed JPG, GIF or PNG. Max size of 800kB</p>
                </Media>
            </Media>
            <Form className='mt-2' onSubmit={handleSubmit(onSubmit)}>
                <Row>
                    <Col sm='6'>
                        <FormGroup>
                            <Label for="firstName">Nom</Label>
                            <input className={`form-control ${errors.firstName ? 'is-invalid' : ''}`}
                                type="text"
                                id="firstName"
                                placeholder="Nom "
                                {...register("firstName")}
                                required />
                            <span className='invalid-feedback'>
                                <strong>{errors.firstName?.message}</strong>
                            </span>
                        </FormGroup>
                    </Col>
                    <Col sm='6'>
                        <FormGroup>
                            <Label for="lastName">Prénom</Label>
                            <input className={`form-control ${errors.lastName ? 'is-invalid' : ''}`}
                                type="text"
                                id="lastName"
                                placeholder="Prénom "
                                {...register('lastName')}
                                required="required" />
                            <span className='invalid-feedback'>
                                <strong>{errors.lastName?.message}</strong>
                            </span>
                        </FormGroup>
                    </Col>
                    <Col sm='4'>
                        <FormGroup>
                            <Label for="username">Pseudo</Label>
                            <input className={`form-control ${errors.username ? 'is-invalid' : ''}`}
                                type="text"
                                id="username"
                                placeholder="Votre pseudo "
                                {...register('username')}
                                required />
                            <span className='invalid-feedback'>
                                <strong>{errors.username?.message}</strong>
                            </span>
                        </FormGroup>
                    </Col>
                    <Col sm='4'>
                        <FormGroup>
                            <Label for="birstday">Date de naissance</Label>
                            <input className={`form-control ${errors.birstday ? 'is-invalid' : ''}`}
                                type="text"
                                id="birstday"
                                placeholder="Date de naissance "
                                {...register('birstday')} />
                            <span className='invalid-feedback'>
                                <strong>{errors.birstday?.message}</strong>
                            </span>
                        </FormGroup>
                    </Col>
                    <Col sm="4">
                        <FormGroup>
                            <Label for='sex'>
                                Sexe
                            </Label>
                            <select className={`form-control ${errors.sex ? 'is-invalid' : ''}`} {...register('sex')} id="sex" required="required">
                                <option value="male" >Male</option>
                                <option value="female" >Female</option>
                            </select>
                            <span className='invalid-feedback'>
                                <strong>{errors.sex?.message}</strong>
                            </span>
                        </FormGroup>
                    </Col>
                    <Col sm='6'>
                        <FormGroup>
                            <Label for="email">E-mail</Label>
                            <input className={`form-control ${errors.email ? 'is-invalid' : ''}`}
                                type="email"
                                id="email"
                                placeholder="E-mail "
                                {...register('email')} required />
                            <span className='invalid-feedback'>
                                <strong>{errors.email?.message}</strong>
                            </span>
                        </FormGroup>
                    </Col>
                    <Col sm='6'>
                        <FormGroup>
                            <Label for="phone">Numéro de téléphone</Label>
                            <input className={`form-control ${errors.phone ? 'is-invalid' : ''}`}
                                type="number"
                                id="phone"
                                placeholder="Numéro de tél "
                                {...register('phone')} required />
                            <span className='invalid-feedback'>
                                <strong>{errors.phone?.message}</strong>
                            </span>
                        </FormGroup>
                    </Col>
                    <Col className='mt-2 text-center' sm='12'>
                        <Button.Ripple
                            type='submit'
                            className='mr-1'
                            color='primary'
                            disabled={isSubmitting} >
                            Sauvegarder
                        </Button.Ripple>
                    </Col>
                </Row>
            </Form>
            <br />
            <VerificationInfoUser userItem={userItem} />
        </Fragment>
    )
}

export default UserProfile
