import Breadcrumbs from '@components/breadcrumbs'
import HelmetSite from '@components/helmet/HelmetSite'
import UndoRedo from '@components/undoredo'
import { CardBody, Card, Row, Col } from "reactstrap"
import RetraitUserFormEdit from './inc/RetraitUserFormEdit'

const RetraitUserEdit = () => {
    return (
        <>
            <HelmetSite title={`Editer retrait`} />
            <Breadcrumbs
                breadCrumbTitle="Retrait"
                breadCrumbParent="Funtion"
                breadCrumbActive="Editer retrait"
            />

            <UndoRedo />

            <RetraitUserFormEdit />
        </>
    )
}

export default RetraitUserEdit