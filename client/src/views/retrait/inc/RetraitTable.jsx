import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { Card, CardBody, CardHeader, Badge, UncontrolledDropdown, DropdownMenu, DropdownToggle, DropdownItem } from 'reactstrap'
import { CheckCircle, Edit, MoreHorizontal, ChevronDown, MoreVertical, Eye } from 'react-feather'
import DataTable from 'react-data-table-component'
import ReactPaginate from 'react-paginate'
import moment from 'moment'

const RetraitTable = ({ retraits }) => {
    const [currentPage, setCurrentPage] = useState(0)

    const columns = [
        {
            name: <h5>Date</h5>,
            selector: "date",
            sortable: true
        },
        {
            name: <h5>Montant</h5>,
            selector: "price_transaction",
            sortable: true,
            grow: 2
        },
        {
            name: <h5>Contact d'envoi</h5>,
            selector: "send_to_transaction",
            sortable: true,
            grow: 4
        },
        {
            name: <h5>Statut</h5>,
            selector: "title_transaction",
            sortable: true,
            grow: 2
        },
        {
            name: "",
            selector: "row_button",
            ignoreRowClick: true,
            allowOverflow: true,
            button: true
        }
    ]

    const data = retraits?.length >= 0 ? (
        retraits.map(item => (
            {
                date: <Link to={`/transactions/${item.slugin}/`} className="text-dark"><b>{moment(item.createdAt).format('ll')}</b></Link>,
                price_transaction: <Link to={`/transactions/${item.slugin}/`}><h4 className={`${item?.retrait?.statusMovesold ? 'text-success' : 'text-danger'}`}>{(item?.retrait && (item?.retrait?.amountRetrait).formatMoney(2, '.', ','))} <small>{item?.amount?.currency}</small></h4></Link>,
                send_to_transaction: <Link to={`/transactions/${item.slugin}/`} className="text-dark"><b>{item?.retrait?.payementmethoduser?.contactRecev} | {item?.retrait?.payementmethoduser?.payementmethodId !== null && (<b>{item?.retrait?.payementmethoduser?.payementmethod?.name}</b>)}</b></Link>,
                title_transaction: <>{item?.retrait?.statusMovesold === false ? (
                    <Badge color="danger" className="mr-1 mb-1 badge-square">
                        <MoreHorizontal size={12} />
                        <span>Traitement en cour</span>
                    </Badge>) : (
                        <Badge color="success" className="mr-1 mb-1 badge-square">
                            <CheckCircle size={12} />
                            <span>Traitement terminé</span>
                        </Badge>)
                }</>,
                row_button: (
                    <>
                        <div className='column-action d-flex align-items-center'>
                            <UncontrolledDropdown>
                                <DropdownToggle tag='span'>
                                    <MoreVertical size={17} className='cursor-pointer' />
                                </DropdownToggle>
                                <DropdownMenu right>
                                    <DropdownItem tag={Link} to={`/retraits/${item?.retrait?.slugin}/show/`} className='w-100'>
                                        <Eye size={14} className='mr-50' />
                                        <span className='align-middle'>Consulter</span>
                                    </DropdownItem>
                                    {item?.retrait?.statusMovesold === false && (
                                        <DropdownItem tag={Link} to={`/retraits/${item?.retrait?.slugin}/edit/`} className='w-100'>
                                            <Edit size={14} className='mr-50' />
                                            <span className='align-middle'>Editer</span>
                                        </DropdownItem>
                                    )}
                                </DropdownMenu>
                            </UncontrolledDropdown>
                        </div>
                    </>
                )
            }
        )
        )
    ) : (
            <></>
        )

    // ** Function to handle Pagination
    const handlePagination = page => {
        setCurrentPage(page.selected)
    }
    // ** Custom Pagination
    const CustomPagination = () => (
        <ReactPaginate
            previousLabel=''
            nextLabel=''
            forcePage={currentPage}
            onPageChange={page => handlePagination(page)}
            pageCount={data.length / 7 || 1}
            breakLabel='...'
            pageRangeDisplayed={2}
            marginPagesDisplayed={2}
            activeClassName='active'
            pageClassName='page-item'
            breakClassName='page-item'
            breakLinkClassName='page-link'
            nextLinkClassName='page-link'
            nextClassName='page-item next'
            previousClassName='page-item prev'
            previousLinkClassName='page-link'
            pageLinkClassName='page-link'
            breakClassName='page-item'
            breakLinkClassName='page-link'
            containerClassName='pagination react-paginate separated-pagination pagination-sm justify-content-end pr-1 mt-1'
        />
    )
    return (
        <Card>

            <CardHeader className='flex-md-row flex-column align-md-items-center align-items-start border-bottom'>
                <div className="tasks-info">
                    <h3 className="mb-75">
                        <strong>Retraits  </strong>
                    </h3>
                </div>
            </CardHeader>

            <CardBody>
                <br />
                <DataTable
                    noHeader
                    pagination
                    columns={columns}
                    defaultSortField="createdAt"
                    defaultSortAsc={false}
                    paginationPerPage={7}
                    className='react-dataTable'
                    sortIcon={<ChevronDown size={10} />}
                    paginationDefaultPage={currentPage + 1}
                    paginationComponent={CustomPagination}
                    data={data}
                />
            </CardBody>
        </Card>
    )
}


export default RetraitTable
