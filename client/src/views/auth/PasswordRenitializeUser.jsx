import React, { useState } from 'react'
import { Link, useHistory, useParams } from 'react-router-dom'
import { AlertCircle } from 'react-feather'
import HelmetSite from '@components/helmet/HelmetSite'
import { Card, CardBody, CardTitle, Form, FormGroup, Label, Alert, Button } from 'reactstrap'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import axios from 'axios'
import '@styles/base/pages/page-auth.scss'
import { SuccessToast, ErrorToast } from '@components/toastalert'
import { toast } from 'react-toastify'

const schema = yup.object().shape({
    password: yup.string().required().min(8).max(200),
    confirmPassword: yup.string().required().min(8).max(200)
        .oneOf([yup.ref('password'), null], 'Passwords must match')
})
const PasswordRenitializeUser = () => {
    const history = useHistory()
    const { usertoken } = useParams()
    const { register, handleSubmit, formState } = useForm({
        resolver: yupResolver(schema)
    })
    const { errors, isSubmitting } = formState
    const [message, setMessage] = useState('')

    const onSubmit = async (data) => {

        try {
            const response = await axios.put(`${process.env.REACT_APP_SERVER_NODE_URL}/password/reset/${usertoken}`, data)
            if (response) {
                toast.success(
                    <SuccessToast name={'Success'} description={'Mot de passe mis à jour avec succès'} />, {
                    position: toast.POSITION.TOP_RIGHT,
                    hideProgressBar: true
                })
                history.push('/login/')
            }
        } catch (error) { setMessage(error.response.data.message) }
    }
    return (
        <>
            <HelmetSite title={'Réinitialiser le mot de passe'} />
            <div className='auth-wrapper auth-v1 px-2'>
                <div className='auth-inner py-2'>
                    <Card className='mb-0'>
                        <CardBody>
                            <Link className='brand-logo' to='/'>
                                <h2 className='brand-text text-primary ml-1'>{process.env.REACT_APP_NAME}</h2>
                            </Link>
                            <CardTitle tag='h4' className='text-center mb-1'>
                                Réinitialiser le mot de passe
                            </CardTitle>
                            <Form className='auth-login-form mt-2' onSubmit={handleSubmit(onSubmit)}>
                                {message && (
                                    <Alert color='danger'>
                                        <div className='alert-body'>
                                            <AlertCircle size={15} />{' '}
                                            <span className='ml-1'>
                                                {message}
                                            </span>
                                        </div>
                                    </Alert>
                                )}
                                <FormGroup>
                                    <div className='d-flex justify-content-between'>
                                        <Label className='form-label' for='password'>
                                            <strong>Nouveau mot de passe</strong>
                                        </Label>
                                    </div>
                                    <input className={`form-control ${errors.password ? 'is-invalid' : ''}`}
                                        type="password"
                                        name="password"
                                        id="password"
                                        placeholder="Nouveau mot de passe"
                                        {...register('password')} required />
                                    <span className='invalid-feedback'>
                                        <strong>{errors.password?.message}</strong>
                                    </span>
                                </FormGroup>
                                <FormGroup>
                                    <div className='d-flex justify-content-between'>
                                        <Label className='form-label' for='confirmPassword'>
                                            <strong>Confirmer mot de passe</strong>
                                        </Label>
                                    </div>
                                    <input className={`form-control ${errors.confirmPassword ? 'is-invalid' : ''}`}
                                        type="password"
                                        id="confirmPassword"
                                        placeholder="Confirm password"
                                        {...register('confirmPassword')} required />
                                    <span className='invalid-feedback'>
                                        <strong>{errors.confirmPassword?.message}</strong>
                                    </span>
                                </FormGroup>
                                <Button.Ripple disabled={isSubmitting} type="submit" color='primary' block>
                                    Réinitialiser
                                </Button.Ripple>
                            </Form>
                            <p className='text-center mt-2'>
                                <span className='mr-25'>Vous vous souvenez de votre mot de passe ?</span>
                                <Link to='/login/'>
                                    <span>Connectez-vous</span>
                                </Link>
                            </p>
                        </CardBody>
                    </Card>
                </div>
            </div>
        </>
    )
}

export default PasswordRenitializeUser