import React, { useState, useEffect } from 'react'
import { Link, useHistory } from 'react-router-dom'
import { AlertCircle } from 'react-feather'
import HelmetSite from '@components/helmet/HelmetSite'
import {
    Card,
    CardBody,
    CardTitle,
    Form,
    FormGroup,
    Label,
    Alert,
    Button
} from 'reactstrap'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import axios from 'axios'
import '@styles/base/pages/page-auth.scss'
import { SuccessToast, ErrorToast } from '@components/toastalert'
import { toast } from 'react-toastify'

const schema = yup.object().shape({
    email: yup.string().email().required().min(3).max(200),
    password: yup.string().required().min(8).max(200)
})

const LoginUser = () => {
    const history = useHistory()
    const { register, handleSubmit, formState } = useForm({
        resolver: yupResolver(schema)
    })
    const { isSubmitting, errors } = formState
    const [message, setMessage] = useState('')
    // ** State
    const [linkredirectData, setLinkredirectData] = useState(null)
    //** ComponentDidMount
    useEffect(() => {
        setLinkredirectData(localStorage.getItem(process.env.REACT_APP_BASE_NAMELINK_REDIRECT))
    }, [])

    const onSubmit = async (data, e) => {

        try {
            const response = await axios.post(`${process.env.REACT_APP_SERVER_NODE_URL}/login`, data)
            if (response) {
                if (response.data.accessToken) {
                    localStorage.setItem(process.env.REACT_APP_BASE_NAMETOKEN, JSON.stringify(response.data))
                }
                toast.success(
                    <SuccessToast name={'Success'} description={'Bienvenue'} />, {
                    position: toast.POSITION.TOP_RIGHT,
                    hideProgressBar: true
                })
                /** Bon ce code ci dessous verify si l'utilisateur a reclamer a acceder
                 * un lien puis un fait la requete au cas contraire on
                 * le redirige sur le lien d'ou il c'est deconnecter
                 */
                if (linkredirectData !== null) {
                    history.push(linkredirectData)
                    localStorage.removeItem(process.env.REACT_APP_BASE_NAMELINK_REDIRECT)
                } else {
                    history.push(location)
                    //history.push('/dashboard/')
                }
            }

        } catch (error) { setMessage(error.response.data.message) }
    }
    return (
        <>
            <HelmetSite title={'Connexion'} />
            <div className='auth-wrapper auth-v1 px-2'>
                <div className='auth-inner py-2'>
                    <Card className='mb-0'>
                        <CardBody>
                            <Link className='brand-logo' to='/'>
                                <h2 className='brand-text text-primary ml-1'>{process.env.REACT_APP_NAME}</h2>
                            </Link>
                            <CardTitle tag='h4' className='text-center mb-1'>
                                Connexion
                            </CardTitle>
                            <Form className='auth-login-form mt-2' onSubmit={handleSubmit(onSubmit)}>
                                {message && (
                                    <Alert color='danger'>
                                        <div className='alert-body'>
                                            <AlertCircle size={15} />{' '}
                                            <span className='ml-1'>
                                                {message}
                                            </span>
                                        </div>
                                    </Alert>
                                )}
                                <FormGroup>
                                    <Label className='form-label' for='email'>
                                        <strong>Adresse e-mail</strong>
                                    </Label>
                                    <input className={`form-control ${errors.email ? 'is-invalid' : ''}`}
                                        type="email"
                                        id="email"
                                        placeholder="Email"
                                        autoComplete="off"
                                        {...register('email')} required />
                                    <span className='invalid-feedback'>
                                        <strong>{errors.email?.message}</strong>
                                    </span>
                                </FormGroup>
                                <FormGroup>
                                    <div className='d-flex justify-content-between'>
                                        <Label className='form-label' for='password'>
                                            <strong>Mot de passe</strong>
                                        </Label>
                                        <Link to='/password/reset/'>
                                            <small>Mot de passe oublié?</small>
                                        </Link>
                                    </div>
                                    <input className={`form-control ${errors.password ? 'is-invalid' : ''}`}
                                        type="password"
                                        id="password"
                                        placeholder="Password"
                                        autoComplete="off"
                                        {...register('password')} required />
                                    <span className='invalid-feedback'>
                                        <strong>{errors.password?.message}</strong>
                                    </span>
                                </FormGroup>
                                {/**
                                <FormGroup>
                                    <CustomInput type='checkbox' className='custom-control-Primary' id='remember-me' label='Remember Me' />
                                </FormGroup>
                                 */}
                                <Button.Ripple disabled={isSubmitting} type="submit" color='primary' block>
                                    Se connecter
                                </Button.Ripple>
                            </Form>
                            <p className='text-center mt-2'>
                                <span className='mr-25'>Nouveau sur la plate-forme?</span>
                                <Link to='/register/'>
                                    <span>Créez votre compte</span>
                                </Link>
                            </p>
                        </CardBody>
                    </Card>
                </div>
            </div>
        </>
    )
}

export default LoginUser