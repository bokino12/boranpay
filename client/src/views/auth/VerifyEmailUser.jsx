import React, { useState, useEffect } from 'react'
import HelmetSite from '@components/helmet/HelmetSite'
import {
  Card,
  Alert,
  CardBody,
  FormGroup,
  Row,
  Col,
  Form,
  Button,
  Label
} from 'reactstrap'
import { authuserInfo, authHeader } from '@components/service'
import { useParams, useHistory, Link } from 'react-router-dom'
import { useForm } from 'react-hook-form'
import axios from 'axios'
import { SuccessToast, ErrorToast } from '@components/toastalert'
import { toast } from 'react-toastify'


const VerifyEmailUser = () => {
  const history = useHistory()
  const { user } = useParams()
  const [errormessage, setErrormessage] = useState('')
  const [userSite] = useState(authuserInfo())
  const {
    register,
    handleSubmit,
    errors,
    formState
  } = useForm()
  const { isSubmitting } = formState


  const onSubmit = async (data) => {

    try {
      const response = await axios.post(`${process.env.REACT_APP_SERVER_NODE_URL}/verify_email/${user}`, data, { headers: authHeader() })
      if (response) {
        toast.success(
          <SuccessToast name={'Success'} description={'E-mail vérifié avec succès'} />, {
          position: toast.POSITION.TOP_RIGHT,
          hideProgressBar: true
        })
        if (response.data.accessToken) {
          localStorage.setItem(process.env.REACT_APP_BASE_NAMETOKEN, JSON.stringify(response.data))
        }
        history.push('/dashboard/')
      }

    } catch (error) {
      toast.error(
        <ErrorToast name={'Opp error'} description={'Quelque chose ne va pas ...'} />, {
        position: toast.POSITION.TOP_RIGHT,
        hideProgressBar: true
      })
      setErrormessage(error.response.data.message)
    }
  }

  return (
    <>
      <HelmetSite title={`Vérification e-mail`} />
      <Card>

        <CardBody>
          <div className="col-md-8 mx-auto">
            <div className="tasks-info">
              <h4 className="mb-75">

              </h4>
            </div>

            <Form onSubmit={handleSubmit(onSubmit)}>

              <div className="col-md-12 text-center">

                {(userSite.emailVerifiedStatus) ? (
                  <>
                    <h3>Cliquer sur le button ci dessous pour retourner au tableau de bord</h3>
                    <FormGroup className="form-label-group">
                      <div className='d-inline-block mr-1 mb-1'>
                        <Button.Ripple
                          onClick={() => history.push(`/dashboard/`)}
                          className="btn-block"
                          color="primary"
                          size='lg'
                        >
                          Tableau de bord
                          </Button.Ripple>
                      </div>
                    </FormGroup>
                  </>
                ) : (
                    <>
                      <h3>Cliquer sur le button ci dessous pour valider votre adresse e-mail</h3>
                      <FormGroup className="form-label-group">
                        <div className='d-inline-block mr-1 mb-1'>
                          <Button.Ripple
                            disabled={isSubmitting}
                            type="submit"
                            className="btn-block"
                            color="primary"
                            size='lg'
                          >
                            Confirmer votre adresse e-mail
                          </Button.Ripple>
                        </div>
                      </FormGroup>
                    </>
                  )}

              </div>
            </Form>
          </div>
        </CardBody>
      </Card>
    </>
  )
}

export default VerifyEmailUser