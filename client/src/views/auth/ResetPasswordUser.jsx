import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { AlertCircle } from 'react-feather'
import HelmetSite from '@components/helmet/HelmetSite'
import { Card, CardBody, CardTitle, Form, FormGroup, Label, Alert, Button } from 'reactstrap'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import axios from 'axios'
import '@styles/base/pages/page-auth.scss'
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'
const MySwal = withReactContent(Swal)

const schema = yup.object().shape({
    email: yup.string().email().required().min(3).max(200)
})
const ResetPasswordUser = () => {
    const { register, handleSubmit, formState } = useForm({
        resolver: yupResolver(schema)
    })
    const { errors, isSubmitting } = formState
    const [message, setMessage] = useState('')

    const onSubmit = async (data, e) => {

        try {
            const response = await axios.post(`${process.env.REACT_APP_SERVER_NODE_URL}/password/reset`, data)
            if (response) {
                MySwal.fire({
                    title: 'Good job!',
                    text: 'Nous vous avons envoyé par courriel le lien de réinitialisation du mot de passe',
                    icon: 'success',
                    confirmButtonText: 'Oui, compris',
                    customClass: {
                        confirmButton: 'btn btn-primary'
                    },
                    buttonsStyling: false
                })
                e.target.reset()
            }
        } catch (error) {
            setMessage(error.response.data.message)
        }
    }
    return (
        <>
            <HelmetSite title={'Réinitialiser le mot de passe'} />
            <div className='auth-wrapper auth-v1 px-2'>
                <div className='auth-inner py-2'>
                    <Card className='mb-0'>
                        <CardBody>
                            <Link className='brand-logo' to='/' onClick={e => e.preventDefault()}>
                                <h2 className='brand-text text-primary ml-1'>{process.env.REACT_APP_NAME}</h2>
                            </Link>
                            <CardTitle tag='h4' className='text-center mb-1'>
                                Mot de passe oublié
                            </CardTitle>
                            <Form className='auth-login-form mt-2' onSubmit={handleSubmit(onSubmit)}>
                                {message && (
                                    <Alert color='danger'>
                                        <div className='alert-body'>
                                            <AlertCircle size={15} />{' '}
                                            <span className='ml-1'>
                                                {message}
                                            </span>
                                        </div>
                                    </Alert>
                                )}
                                <FormGroup>
                                    <Label className='form-label' for='email'>
                                        <strong>Adresse e-mail</strong>
                                    </Label>
                                    <input className={`form-control ${errors.email ? 'is-invalid' : ''}`}
                                        type="email"
                                        id="email"
                                        placeholder="E-mail"
                                        autoComplete="off"
                                        {...register('email')} />
                                    <span className='invalid-feedback'>
                                        <strong>{errors.email?.message}</strong>
                                    </span>
                                </FormGroup>
                                <Button.Ripple disabled={isSubmitting} type="submit" color='primary' block>
                                    Envoyer
                                </Button.Ripple>
                            </Form>
                            <p className='text-center mt-2'>
                                <span className='mr-25'>Vous vous souvenez de votre mot de passe ?</span>
                                <Link to='/login/'>
                                    <span>Connectez-vous</span>
                                </Link>
                            </p>
                        </CardBody>
                    </Card>
                </div>
            </div>
        </>
    )
}

export default ResetPasswordUser