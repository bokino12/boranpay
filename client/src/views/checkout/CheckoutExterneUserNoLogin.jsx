import React, { useState } from 'react'
import { Link, useHistory, useParams } from 'react-router-dom'
import HelmetSite from '@components/helmet/HelmetSite'
import {
    Card,
    CardBody,
    CardTitle,
    Form,
    FormGroup,
    Label,
    Alert,
    Col,
    Row,
    Button
} from 'reactstrap'
// ** Utils
import { isUserLoggedIn } from '@utils'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import '@styles/base/pages/page-auth.scss'

const schema = yup.object().shape({
    //email: yup.string().email().required().min(3).max(200)
})
const CheckoutExterneUserNoLogin = () => {
    const { clientid } = useParams()
    const history = useHistory()
    const { register, handleSubmit, formState } = useForm({
        resolver: yupResolver(schema)
    })
    const { errors, isSubmitting } = formState
    const [errormessage, setErrormessage] = useState('')

    const onSubmit = async (data, e) => {
        if (isUserLoggedIn() === null) {
            // important respecter la position pour 

            //First position
            localStorage.setItem(process.env.REACT_APP_BASE_NAMELINK_REDIRECT, `/checkout_externe/${clientid}`)
            //Second position
            history.push('/login/')
        }
    }
    return (
        <>
            <HelmetSite title={'Transaction'} />
            <div className='auth-wrapper auth-v1 px-2'>
                <div className='auth-inner py-2'>
                    <Card className='mb-0'>
                        <CardBody>
                            <Link className='brand-logo' to='/'>
                                <h2 className='brand-text text-primary ml-1'>{process.env.REACT_APP_NAME}</h2>
                            </Link>
                            <Form className='auth-login-form mt-2' onSubmit={handleSubmit(onSubmit)}>

                                <Row>
                                    <Col md="8" sm="8">
                                        <FormGroup>
                                            <Label for="total">Montant transaction</Label>
                                            <input className={`form-control ${errors.total ? 'is-invalid' : ''}`}
                                                type="number"
                                                pattern="[0-9]*"
                                                inputMode="decimal"
                                                min="1" step="1"
                                                id="total"
                                                //defaultValue={8}
                                                placeholder="Montant de la transaction"
                                                {...register('total')}
                                                autoComplete="off"
                                            //required
                                            />
                                            <span className='invalid-feedback'>
                                                <strong>{errors.total?.message}</strong>
                                            </span>
                                        </FormGroup>
                                    </Col>
                                    <Col md="4" sm="4">
                                        <FormGroup>
                                            <Label for='currency'>Device</Label>
                                            <input className={`form-control ${errors.currency ? 'is-invalid' : ''}`}
                                                type='text'
                                                id="currency"
                                                defaultValue={'CAD'}
                                                placeholder='EUR'
                                                {...register('currency')}
                                            //required
                                            />
                                            <span className='invalid-feedback'>
                                                <strong>{errors.currency?.message}</strong>
                                            </span>
                                        </FormGroup>
                                    </Col>
                                    <Col md="12" sm="12">
                                        <FormGroup>
                                            <Label for="title">Description</Label>
                                            <input className={`form-control ${errors.title ? 'is-invalid' : ''}`}
                                                type="text"
                                                id="title"
                                                {...register('title')}
                                                placeholder="Donner un titre à la transaction"
                                            //required
                                            />
                                            <span className='invalid-feedback'>
                                                <strong>{errors.title?.message}</strong>
                                            </span>
                                        </FormGroup>
                                    </Col>
                                    <div className="col-md-12 text-center">
                                        <FormGroup className="form-label-group">
                                            <Button.Ripple
                                                disabled={isSubmitting}
                                                color="primary"
                                                type="submit"
                                                className="mr-1 mb-1"
                                                block>
                                                Terminer
                                        </Button.Ripple>
                                        </FormGroup>
                                    </div>

                                    <br />
                                    <div className="col-md-12 text-center">
                                        <Link to="/dashboard/" ><b>Annuler</b></Link>
                                    </div>
                                </Row>

                            </Form>
                        </CardBody>
                    </Card>
                </div>
            </div>
        </>
    )
}

export default CheckoutExterneUserNoLogin