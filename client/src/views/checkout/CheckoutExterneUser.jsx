import React, { useState } from 'react'
import { Link, useHistory, useParams } from 'react-router-dom'
import { AlertCircle } from 'react-feather'
import HelmetSite from '@components/helmet/HelmetSite'
import {
    Card,
    CardBody,
    CardTitle,
    Form,
    FormGroup,
    Label,
    Row,
    Col,
    Alert,
    Button
} from 'reactstrap'
import { authHeader } from '@components/service'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import axios from 'axios'
import '@styles/base/pages/page-auth.scss'
import { SuccessToast, ErrorToast } from '@components/toastalert'
import { toast } from 'react-toastify'

const schema = yup.object().shape({
    //email: yup.string().email().required().min(3).max(200)
})
const CheckoutExterneUser = () => {
    const { clientId } = useParams()
    const history = useHistory()
    const { register, handleSubmit, formState } = useForm({
        resolver: yupResolver(schema)
    })
    const { errors, isSubmitting } = formState
    const [errormessage, setErrormessage] = useState('')

    const onSubmit = async (data, e) => {

        console.log('Data =>', data)

        try {
            const response = await axios.post(`${process.env.REACT_APP_SERVER_NODE_URL}/checkout_externe/${clientId}`, data, { headers: authHeader() })
            if (response) {
                toast.success(
                    <SuccessToast name={'Success'} description={'Bienvenue sur boranpay'} />, {
                    position: toast.POSITION.TOP_RIGHT,
                    hideProgressBar: true
                })
                // window.location.reload()
            }
        } catch (error) {
            setErrormessage(error.response.data.message)
        }
    }
    return (
        <>
            <HelmetSite title={'Checkout'} />
            <Card>
                <CardBody>
                    <div className="col-md-8 mx-auto">
                        <Form className="mt-2" onSubmit={handleSubmit(onSubmit)}>
                            {errormessage && (
                                <Alert color='danger'>
                                    <div className='alert-body text-center'>
                                        <AlertCircle size={15} />{' '}
                                        <span className='ml-1'>
                                            {errormessage}
                                        </span>
                                    </div>
                                </Alert>
                            )}
                            <Row>
                                <Col md="8" sm="8">
                                    <FormGroup>
                                        <Label for="total">Montant transaction</Label>
                                        <input className={`form-control ${errors.total ? 'is-invalid' : ''}`}
                                            type="number"
                                            pattern="[0-9]*"
                                            inputMode="decimal"
                                            min="1" step="1"
                                            id="total"
                                            //defaultValue={8}
                                            placeholder="Montant de la transaction"
                                            {...register('total')}
                                            autoComplete="off"
                                        //required
                                        />
                                        <span className='invalid-feedback'>
                                            <strong>{errors.total?.message}</strong>
                                        </span>
                                    </FormGroup>
                                </Col>
                                <Col md="4" sm="4">
                                    <FormGroup>
                                        <Label for='currency'>Device</Label>
                                        <input className={`form-control ${errors.currency ? 'is-invalid' : ''}`}
                                            type='text'
                                            id="currency"
                                            defaultValue={'EUR'}
                                            placeholder='EUR'
                                            {...register('currency')}
                                        //required
                                        />
                                        <span className='invalid-feedback'>
                                            <strong>{errors.currency?.message}</strong>
                                        </span>
                                    </FormGroup>
                                </Col>
                                <Col md="12" sm="12">
                                    <FormGroup>
                                        <Label for="title">Description</Label>
                                        <input className={`form-control ${errors.title ? 'is-invalid' : ''}`}
                                            type="text"
                                            id="title"
                                            {...register('title')}
                                            placeholder="Donner un titre à la transaction"
                                        //required
                                        />
                                        <span className='invalid-feedback'>
                                            <strong>{errors.title?.message}</strong>
                                        </span>
                                    </FormGroup>
                                </Col>
                                <div className="col-md-12 text-center">
                                    <FormGroup className="form-label-group">
                                        <Button.Ripple
                                            disabled={isSubmitting}
                                            color="primary"
                                            type="submit"
                                            className="mr-1 mb-1"
                                            block>
                                            Terminer
                                        </Button.Ripple>
                                    </FormGroup>
                                </div>

                                <br />
                                <div className="col-md-12 text-center">
                                    <Link to="/dashboard/" ><b>Annuler</b></Link>
                                </div>
                                <div className="col-md-12 text-center">
                                    Vérifier attentivement <strong>les informations</strong> avant la confirmation de la transaction.
                                </div>
                            </Row>
                        </Form>
                    </div>
                </CardBody>
            </Card>
        </>
    )
}

export default CheckoutExterneUser