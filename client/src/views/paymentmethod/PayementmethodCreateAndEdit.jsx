import React, { useState, useEffect } from 'react'
import Breadcrumbs from '@components/breadcrumbs'
import HelmetSite from '@components/helmet/HelmetSite'
import UndoRedo from '@components/undoredo'
import { authHeader, authuserInfo } from '@components/service'
import {
    Card,
    CardBody,
    Alert,
    FormGroup,
    Row,
    Col,
    Form,
    Button,
    Label
} from 'reactstrap'
import { AlertCircle, Plus } from 'react-feather'
import userServices from '../../services/userServices'
import { useParams, useHistory, Link } from 'react-router-dom'
import { useDispatch, useSelector, shallowEqual } from "react-redux"
import axios from 'axios'
import Swal from 'sweetalert2'
import { loadAllCountriesretrait, loadAllPayementmethodsretrait } from '../../redux/actions/pages/countryAction'
import { SuccessToast, ErrorToast } from '@components/toastalert'
import { toast } from 'react-toastify'

const PayementmethodCreateAndEdit = () => {
    const initialItemState = {
        countryName: '',
        payementmethodId: '',
        contactRecev: '',
        fullName: '',
        password: ''
    }
    const { payementmethod } = useParams()
    const [userSite] = useState(authuserInfo())
    const history = useHistory()
    const isAddMode = !payementmethod
    { /** Get currencies with redux */ }
    const [statusSend, setStatusSend] = useState(false)
    const [errormessage, setErrormessage] = useState('')

    const [currentItem, setCurrentItem] = useState(initialItemState)
    const { countries, payementmethods } = useSelector(state => ({
        countries: state?.countries?.countries,
        payementmethods: state?.countries?.payementmethods
    }), shallowEqual)
    const dispatch = useDispatch()

    const handleInputChange = e => {
        const { name, value } = e.target
        setCurrentItem({ ...currentItem, [name]: value })
    }

    useEffect(() => {
        const loadItems = async () => {
            await dispatch(loadAllCountriesretrait())
            await dispatch(loadAllPayementmethodsretrait())
        }
        loadItems()
    }, [])

    useEffect(() => {
        const loadItems = async () => {
            if (!isAddMode) {
                axios.get(`${process.env.REACT_APP_SERVER_NODE_URL}/mypayementmethods/${payementmethod}`, { headers: authHeader() })
                    .then(response => { setCurrentItem(response.data) })
            }
        }
        loadItems()
    }, [payementmethod])


    const toggleStatus = () => { setStatusSend(!statusSend) }

    const createItem = async (e, data) => {
        try {
            const response = await axios.post(`${process.env.REACT_APP_SERVER_NODE_URL}/mypayementmethods`, data, { headers: authHeader() })
            if (response) {
                toast.success(
                    <SuccessToast name={'Success'} description={'Payement sauvegardé avec succès'} />, {
                    position: toast.POSITION.TOP_RIGHT,
                    hideProgressBar: true
                })
                history.goBack()
            }
        } catch (error) {
            toast.error(
                <ErrorToast name={'Opp error'} description={'Quelque chose ne va pas ...'} />, {
                position: toast.POSITION.TOP_RIGHT,
                hideProgressBar: true
            })
            setErrormessage(error.response.data.message)
        }
    }

    const updateItem = async (payementmethod, e, data) => {
        await axios.put(`${process.env.REACT_APP_SERVER_NODE_URL}/mypayementmethods/${payementmethod}`, data, { headers: authHeader() })
            .then((res) => {
                toast.success(
                    <SuccessToast name={'Success'} description={'Payement sauvegardé avec succès'} />, {
                    position: toast.POSITION.TOP_RIGHT,
                    hideProgressBar: true
                })
                history.push('/mypayementmethods/')
            }).catch((error) => {
                toast.error(
                    <ErrorToast name={'Danger'} description={'Oops! Quelque chose ne va pas.'} />, {
                    position: toast.POSITION.TOP_RIGHT,
                    hideProgressBar: true
                })
                setErrormessage(error.response.data.message)
            })
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        const { countryName, payementmethodId, contactRecev, fullName, password } = currentItem
        const data = { countryName, payementmethodId, contactRecev, fullName, password }

        Swal.fire({
            title: 'Confirmez-vous cette creation ?',
            text: "Êtes-vous sûr de vouloir executer cette action ?",
            confirmButtonText: 'Oui, Confirmer',
            cancelButtonText: 'Non, annuler',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-primary",
            cancelButtonClass: 'btn btn-outline-default',
            showCancelButton: true,
            reverseButtons: false,
            showLoaderOnConfirm: true,
            preConfirm: () => {
                return userServices.userCheckValidate(userSite, data)
                    .then(async () => {

                        return isAddMode ? createItem(e, data) : updateItem(payementmethod, e, data)

                    }).catch(error => {
                        Swal.showValidationMessage(`${error.response.data.message}`)
                    })
            },
            allowOutsideClick: () => !Swal.isLoading()
        })
    }

    return (
        <>
            <HelmetSite title={`Methode de payement`} />
            <Breadcrumbs
                breadCrumbTitle="Methode de payement "
                breadCrumbParent="Sections"
                breadCrumbActive={'Methode de payement'}
            />
            {/** Undo redo */}
            <UndoRedo />
            <Card>

                <CardBody>
                    <div className="col-md-10 mx-auto">
                        <div className="tasks-info">
                            <h4 className="mb-75">
                                <strong>{`Methode de payement`}</strong>
                            </h4>
                        </div>
                        <Form className="mt-2" onSubmit={handleSubmit}>

                            {errormessage && (
                                <Alert color='danger'>
                                    <div className='alert-body text-center'>
                                        <AlertCircle size={15} />{' '}
                                        <span className='ml-1'>
                                            {errormessage}
                                        </span>
                                    </div>
                                </Alert>
                            )}
                            <Row>
                                <Col md={`${currentItem?.countryName}` ? '7' : '12'} sm={`${currentItem?.countryName}` ? '7' : '12'}>
                                    <FormGroup>
                                        <Label htmlFor="countryName"><b>Pays</b></Label>
                                        <select className={`form-control`} id='countryName'
                                            name="countryName"
                                            value={currentItem.countryName}
                                            onChange={handleInputChange}
                                            required>
                                            <option value="" disabled>Choisissez le pays</option>
                                            {countries.map((item) => (
                                                <option key={item.id} value={item.name} >{item.name}</option>
                                            ))}
                                        </select>
                                    </FormGroup>
                                </Col>
                                {currentItem?.countryName && (
                                    <Col md="5" sm="5">
                                        <FormGroup>
                                            <Label htmlFor="payementmethodId"><b>Moyen de retrait</b></Label>
                                            <select className={`form-control`}
                                                name="payementmethodId"
                                                value={currentItem.payementmethodId}
                                                onChange={handleInputChange}
                                                id="payementmethodId" required>
                                                <option value="" disabled>Choisissez le moyen de retrait</option>
                                                {payementmethods.filter(item => !currentItem.countryName || item.country.name.includes(currentItem.countryName))
                                                    .map((item) => (
                                                        <option key={item.id} value={item.id} >{item.name}</option>
                                                    ))}
                                            </select>
                                        </FormGroup>
                                    </Col>
                                )}
                                <Col md="6" sm="6">
                                    <FormGroup>
                                        <Label htmlFor="fullName"><strong>Votre nom complet</strong></Label>
                                        <input className={`form-control`}
                                            type="text"
                                            id="fullName"
                                            name="fullName"
                                            value={currentItem.fullName}
                                            onChange={handleInputChange}
                                            placeholder="Votre nom complet..."
                                            required
                                        />
                                    </FormGroup>
                                </Col>
                                <Col md="6" sm="6">
                                    <FormGroup>
                                        <Label htmlFor="contactRecev"><strong>Identifiant de réception</strong></Label>
                                        <input className={`form-control`}
                                            type="text"
                                            name="contactRecev"
                                            id="contactRecev"
                                            value={currentItem.contactRecev}
                                            onChange={handleInputChange}
                                            placeholder="Numéro de Tél, email, Iban ou Votre nom complet ..."
                                            required
                                        />
                                    </FormGroup>
                                </Col>
                                {statusSend && (
                                    <Col sm={12}>
                                        <FormGroup>
                                            <Label for="password"><strong>Veuillez saisir votre mot de passe</strong></Label>
                                            <input className={`form-control`}
                                                type="password"
                                                id="password"
                                                name="password"
                                                value={currentItem.password}
                                                onChange={handleInputChange}
                                                placeholder="Mot de passe pour valider la sauvegarde"
                                                autoComplete="off"
                                                required />
                                        </FormGroup>
                                    </Col>
                                )}
                                <div className="col-md-12 text-center">
                                    <FormGroup className="form-label-group">
                                        {statusSend ? (
                                            <Button.Ripple
                                                type="submit"
                                                className="btn-block"
                                                color="primary"
                                                block
                                            >
                                                Sauvegarder
                                            </Button.Ripple>
                                        ) : (
                                            <Button.Ripple
                                                className="btn-block"
                                                color="primary"
                                                onClick={toggleStatus}
                                                block
                                            >
                                                Continuer
                                            </Button.Ripple>
                                        )}
                                    </FormGroup>
                                </div>
                                <br />
                                <div className="col-md-12 text-center">
                                    <Link to="/dashboard/" ><b>Annuler</b></Link>
                                </div>
                            </Row>
                        </Form>
                    </div>
                </CardBody>
            </Card>
        </>
    )
}

export default PayementmethodCreateAndEdit