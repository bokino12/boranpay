import { Link } from 'react-router-dom'
import Avatar from '@components/avatar'
import {
    Eye,
    MoreVertical,
    Edit,
    Trash,
    ChevronDown
} from 'react-feather'
import PropTypes from 'prop-types'
import { Card, CardBody, CardHeader, UncontrolledDropdown, DropdownMenu, DropdownToggle, DropdownItem } from 'reactstrap'
import DataTable from 'react-data-table-component'
import { useDispatch } from 'react-redux'
import { deletegroupeItem } from '../../../redux/actions/pages/groupeAction'

const renderClient = item => {
    const stateNum = Math.floor(Math.random() * 6),
        states = ['light-success', 'light-danger', 'light-warning', 'light-info', 'light-primary', 'light-secondary'],
        color = states[stateNum]

    return <Avatar color={color} className='mr-50' content={(item.name)} initials />
}


const GroupeTable = ({ groupes }) => {
    const dispatch = useDispatch()

    const columns = [
        {
            name: <h5>Name</h5>,
            selector: "item_name",
            sortable: true,
            grow: 4
        },
        {
            name: <h5>Description</h5>,
            selector: "full_name",
            sortable: true,
            grow: 3
        },
        {
            name: "",
            selector: "row_button",
            ignoreRowClick: true,
            allowOverflow: true,
            button: true
        }
    ]

    const data = groupes?.length >= 0 ? (
        groupes.map(item => (
            {
                /**avatar_name: <Link to={`/ug/${item.slugin}/`}>
                    <Avatar className='mr-50' img={'https://ivemo-cm.s3.eu-central-1.amazonaws.com/img/locations/de2a5dc5106a1d9d1a250251f78436c16c7923b7.png'} width='60' height='60' /></Link>, */
                item_name: (
                    <>
                        <div className='d-flex justify-content-left align-items-center'>
                            {renderClient(item)}
                            <div className='d-flex flex-column'>
                                <h6 className='user-name text-truncate mb-0'>{item.name}</h6>
                            </div>
                        </div>
                    </>
                ),
                full_name: <Link to={`/ug/${item.slugin}/`} className="text-dark"><b>{item.content}</b></Link>,
                row_button: (
                    <>
                        <div className='column-action d-flex align-items-center'>
                            <Link to={`/ug/${item.slugin}/`}>
                                <Eye size={17} className='mx-1' />
                            </Link>
                            <UncontrolledDropdown>
                                <DropdownToggle tag='span'>
                                    <MoreVertical size={17} className='cursor-pointer' />
                                </DropdownToggle>
                                <DropdownMenu right>
                                    <DropdownItem tag={Link} to={`/groupes/${item.slugin}/edit/`} className='w-100'>
                                        <Edit size={14} className='mr-50' />
                                        <span className='align-middle'>Editer</span>
                                    </DropdownItem>
                                    <DropdownItem tag='a' href={void (0)} className='w-100' onClick={() => dispatch(deletegroupeItem(item))}>
                                        <Trash size={14} className='mr-50' />
                                        <span className='align-middle'>Supprimer</span>
                                    </DropdownItem>
                                </DropdownMenu>
                            </UncontrolledDropdown>
                        </div>
                    </>
                )
            }
        )
        )
    ) : (
        <></>
    )

    return (
        <Card>

            <CardHeader className='flex-md-row flex-column align-md-items-center align-items-start border-bottom'>
                <div className="tasks-info">
                    <h3 className="mb-75">
                        <strong>Groupe </strong>
                    </h3>
                </div>
            </CardHeader>

            <CardBody>
                <br />
                <DataTable
                    className='react-dataTable'
                    sortIcon={<ChevronDown size={10} />}
                    paginationRowsPerPageOptions={[10, 25, 50, 100]}
                    data={data}
                    columns={columns}
                    defaultSortField="createdAt"
                    defaultSortAsc={false}
                    overflowY={true}
                    pagination={true}
                    noHeader />
            </CardBody>
        </Card>
    )
}

export default GroupeTable