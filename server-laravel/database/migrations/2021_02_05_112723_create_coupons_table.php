<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->id();
            $table->string('couponNumber')->nullable();
            $table->string('ip')->nullable();
            $table->boolean('status')->nullable();
            $table->string('currency')->nullable();
            $table->float('couponAmount')->nullable();
            $table->unsignedBigInteger('seriesNumber')->nullable();
            $table->unsignedBigInteger('userId')->nullable();
            $table->date('createdAt')->default(now());
            $table->date('updatedAt')->default(now());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
