<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAmountdonationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amountdonations', function (Blueprint $table) {
            $table->id();
            $table->float('amountDonation')->nullable();
            $table->float('amountDonationNoTaxe')->nullable();
            $table->unsignedBigInteger('userId')->nullable();
            $table->unsignedBigInteger('amountId')->nullable();
            $table->unsignedBigInteger('donationId')->nullable();
            $table->date('createdAt')->default(now());
            $table->date('updatedAt')->default(now());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('amountdonations');
    }
}
