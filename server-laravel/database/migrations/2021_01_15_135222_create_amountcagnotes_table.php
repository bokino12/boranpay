<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAmountcagnotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amountcagnotes', function (Blueprint $table) {
            $table->id();
            $table->float('amountCagnote')->nullable();
            $table->float('amountCagnoteNoTaxe')->nullable();
            $table->unsignedBigInteger('userId')->nullable();
            $table->unsignedBigInteger('amountId')->nullable();
            $table->unsignedBigInteger('cagnoteId')->nullable();
            $table->date('createdAt')->default(now());
            $table->date('updatedAt')->default(now());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('amountcagnotes');
    }
}
