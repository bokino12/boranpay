<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAmountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amounts', function (Blueprint $table) {
            $table->id();
            $table->float('total')->nullable();
            $table->float('totalNoTaxe')->nullable();
            $table->float('taxeTransaction')->nullable();
            $table->string('currency')->nullable();
            $table->unsignedBigInteger('transactionId')->nullable();
            $table->unsignedBigInteger('transactiondonationId')->nullable();
            $table->unsignedBigInteger('transactioncagnoteId')->nullable();
            $table->unsignedBigInteger('transactionserviceId')->nullable();
            $table->date('createdAt')->default(now());
            $table->date('updatedAt')->default(now());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('amounts');
    }
}
