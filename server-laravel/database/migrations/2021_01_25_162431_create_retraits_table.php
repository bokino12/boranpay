<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRetraitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('retraits', function (Blueprint $table) {
            $table->id();
            $table->float('amountRetrait')->nullable();
            $table->string('slugin')->nullable();
            $table->string('ip')->nullable();
            $table->longText('content')->nullable();
            $table->boolean('statusMovesold')->default(false);
            $table->boolean('statusCancelretrait')->default(false);
            $table->unsignedBigInteger('countryId')->nullable();
            $table->unsignedBigInteger('payementmethodId')->nullable();
            $table->unsignedBigInteger('payementmethoduserId')->nullable();
            $table->unsignedBigInteger('transactionId')->nullable();
            $table->unsignedBigInteger('useradminId')->nullable();
            $table->date('createdAt')->default(now());
            $table->date('updatedAt')->default(now());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('retraits');
    }
}
