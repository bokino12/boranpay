<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\category;
use App\Models\transaction;
use App\Models\user;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(transaction::class, function (Faker $faker) {

    $users =  [
        'statusSend' => mt_rand(0, 1),
        'slugin' => sha1(('YmdHis') . str_random(30)),
        'tokenTransaction' => sha1(('YmdHis') . str_random(10)),
        'invoiceNumber' => mt_rand(1000, 100000),
        'title' => $faker->sentence(10),
        'content' => $faker->text,
        'userId' => user::inRandomOrder()->first()->id,
        'usertoId' => user::inRandomOrder()->first()->id,
        'categoryId' => category::inRandomOrder()->first()->id,
    ];
    return $users;
});
