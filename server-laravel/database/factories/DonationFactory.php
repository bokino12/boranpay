<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\amount;
use App\Models\amountuser;
use App\Models\category;
use App\Models\donation;
use App\Models\transaction;
use App\Models\user;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(donation::class, function (Faker $faker) {
    $title = $faker->sentence(20);
    $users =  [
        'title' => $title,
        'slug' => str_slug($title),
        'slugin' => Str::uuid(),
        'content' => "<p>".$faker->realText(rand(1000, 2000))."</p>",
        'userId' => user::inRandomOrder()->first()->id,
    ];
    return $users;
});
