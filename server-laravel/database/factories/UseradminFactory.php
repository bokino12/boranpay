<?php

namespace Database\Factories;

use App\Models\useradmin;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UseradminFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = useradmin::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'firstName' => 'Admin',
            'lastName' => 'BoranPay',
            'email' => 'admin@boranpay.com',
            'slugin' => Str::random(10),
            'role' => 'admin',
            'sex' => 'male',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'createdAt' => now(),
            'updatedAt' => now(),
        ];
    }
}
