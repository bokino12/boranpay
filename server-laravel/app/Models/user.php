<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Laravel\Sanctum\HasApiTokens;

class user extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fistName', 'email', 'password',
    ];

    protected static function boot()
    {
        parent::boot();

        static::created(function ($user){
            $user->profile()->create([
                'siteInternet' => 'https://www.boranpay.com',
                'addresse' => 'Yaounde'.+$user->id,
                'currencyId' => currency::inRandomOrder()->first()->id,
                'countryId' => country::inRandomOrder()->first()->id,
                'slugin' => $user->slugin,
            ]);
        });

        static::created(function ($user){
            $servicemodel = collect([
                ['subject' => 'all', 'action' => 'manage'],
                ['subject' => 'ACL', 'action' => 'read'],
            ]);
            $user->ability()->create([
                'subject' => $servicemodel->shuffle()->first()['subject'],
                'action' => $servicemodel->shuffle()->first()['action'],
            ]);
        });

    }

    public function profile()
    {
        return $this->hasOne(profile::class,'userId');
    }

    public function ability()
    {
        return $this->hasOne(ability::class,'userId');
    }
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
