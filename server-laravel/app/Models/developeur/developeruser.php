<?php

namespace App\Models\developeur;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class developeruser extends Model
{
    use HasFactory;

    public $timestamps = false;
}
